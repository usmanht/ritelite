<?PHP
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/**
 * THIS CLASS IS FOR DEVELOPERS TO MAKE CUSTOMIZATIONS IN
 */
require_once('modules/RL_Sales_Order_Line_Items/RL_Sales_Order_Line_Items_sugar.php');
class RL_Sales_Order_Line_Items extends RL_Sales_Order_Line_Items_sugar {
	
	function RL_Sales_Order_Line_Items(){	
		parent::RL_Sales_Order_Line_Items_sugar();
	}

    function save_lines($post_data, $parent, $groups = array(), $key = ''){

        $line_count = count($post_data[$key.'name']);
        $j = 0;
        for ($i = 0; $i < $line_count; ++$i) {

            if($post_data[$key.'deleted'][$i] == 1){
                $this->mark_deleted($post_data[$key.'id'][$i]);
            } else {
                $product_quote = new RL_Sales_Order_Line_Items();
                foreach($this->field_defs as $field_def) {
                    if(isset($post_data[$key.$field_def['name']][$i])){
                        $product_quote->$field_def['name'] = $post_data[$key.$field_def['name']][$i];
                    }
                }
				if(isset($post_data['currency_id'][$i])) {
                    $product_quote->currency_id = $post_data['currency_id'];
                }
                if(isset($post_data[$key.'group_number'][$i])){
                    $product_quote->group_id = $groups[$post_data[$key.'group_number'][$i]];
                }
                if(trim($product_quote->product_id) != '' && trim($product_quote->name) != '' && trim($product_quote->product_unit_price) != ''){
                    $product_quote->number = ++$j;
                    $product_quote->assigned_user_id = $parent->assigned_user_id;
                    $product_quote->currency_id = $parent->currency_id;
                    $product_quote->parent_id = $parent->id;
                    $product_quote->parent_type = $parent->object_name;
                    $product_quote->sales_order_number = $parent->number;
                    $product_quote->customer_order_number = $parent->customer_order_number_c;
                    $product_quote->sage_sop_number_c = $parent->sage_sop_number_c;
                    $product_quote->invoice_no_c = $parent->invoice_no_c;

                    $product_quote->save();
                    $_POST[$key . 'id'][$i] = $product_quote->id;

                    //add relationships
                    $product_quote->load_relationship('accounts_rl_sales_order_line_items_1');
                    $product_quote->accounts_rl_sales_order_line_items_1->add($parent->billing_account_id);
                    $product_quote->load_relationship('accounts_rl_sales_order_line_items_2');
                    $product_quote->accounts_rl_sales_order_line_items_2->add($parent->accounts_aos_invoices_1accounts_ida);
                    $product_quote->load_relationship('contacts_rl_sales_order_line_items_1');
                    $product_quote->contacts_rl_sales_order_line_items_1->add($parent->billing_contact_id);
                    $product_quote->load_relationship('contacts_rl_sales_order_line_items_2');
                    $product_quote->contacts_rl_sales_order_line_items_2->add($parent->contacts_aos_invoices_1contacts_ida);


                    //relate sales order(invoices) to product
                    $product = new AOS_Products();
                    $product->retrieve($product_quote->product_id);
                    $product->load_relationship('aos_products_aos_invoices_1');
                    $product->aos_products_aos_invoices_1->add($parent->id);
                    //$product->save();

                }
            }
        }
    }


    function mark_lines_deleted($parent){

        require_once('modules/Relationships/Relationship.php');
        //$key = Relationship::retrieve_by_modules($parent->object_name, $this->object_name, $this->db);
        //if (!empty($key)) {
        $product_quotes = $parent->get_linked_beans(' rl_sales_order_line_items_aos_products', $this->object_name);
        foreach($product_quotes as $product_quote){
            $product_quote->mark_deleted($product_quote->id);
        }
        //}
    }
}
?>
