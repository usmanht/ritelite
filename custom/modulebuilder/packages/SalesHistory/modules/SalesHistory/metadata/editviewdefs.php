<?php
$module_name = 'RL_SalesHistory';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'sop_number',
            'label' => 'LBL_SOP_NUMBER',
          ),
          1 => 
          array (
            'name' => 'invoice_number',
            'label' => 'LBL_INVOICE_NUMBER',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'customer_order_number',
            'label' => 'LBL_CUSTOMER_ORDER_NUMBER',
          ),
          1 => 
          array (
            'name' => 'accref',
            'label' => 'LBL_ACCREF',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'carriage_rate_charged',
            'label' => 'LBL_CARRIAGE_RATE_CHARGED',
          ),
          1 => '',
        ),
        3 => 
        array (
          0 => '',
          1 => '',
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'total_value',
            'label' => 'LBL_TOTAL_VALUE',
          ),
          1 => '',
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'order_date',
            'label' => 'LBL_ORDER_DATE',
          ),
          1 => 
          array (
            'name' => 'despatch_date',
            'label' => 'LBL_DESPATCH_DATE',
          ),
        ),
      ),
    ),
  ),
);
?>
