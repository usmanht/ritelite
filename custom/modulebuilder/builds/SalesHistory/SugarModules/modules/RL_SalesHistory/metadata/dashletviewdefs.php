<?php
$dashletData['RL_SalesHistoryDashlet']['searchFields'] = array (
  'date_entered' => 
  array (
    'default' => '',
  ),
  'date_modified' => 
  array (
    'default' => '',
  ),
  'assigned_user_id' => 
  array (
    'type' => 'assigned_user_name',
    'default' => 'Administrator',
  ),
);
$dashletData['RL_SalesHistoryDashlet']['columns'] = array (
  'sop_number' => 
  array (
    'type' => 'int',
    'label' => 'LBL_SOP_NUMBER',
    'width' => '10%',
    'default' => true,
    'name' => 'sop_number',
  ),
  'invoice_number' => 
  array (
    'type' => 'int',
    'label' => 'LBL_INVOICE_NUMBER',
    'width' => '10%',
    'default' => true,
    'name' => 'invoice_number',
  ),
  'customer_order_number' => 
  array (
    'type' => 'int',
    'label' => 'LBL_CUSTOMER_ORDER_NUMBER',
    'width' => '10%',
    'default' => true,
    'name' => 'customer_order_number',
  ),
  'order_date' => 
  array (
    'type' => 'date',
    'label' => 'LBL_ORDER_DATE',
    'width' => '10%',
    'default' => true,
    'name' => 'order_date',
  ),
  'despatch_date' => 
  array (
    'type' => 'date',
    'label' => 'LBL_DESPATCH_DATE',
    'width' => '10%',
    'default' => true,
    'name' => 'despatch_date',
  ),
  'total_value' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_TOTAL_VALUE',
    'currency_format' => true,
    'width' => '10%',
    'default' => true,
  ),
  'name' => 
  array (
    'width' => '40%',
    'label' => 'LBL_LIST_NAME',
    'link' => true,
    'default' => false,
    'name' => 'name',
  ),
  'date_entered' => 
  array (
    'width' => '15%',
    'label' => 'LBL_DATE_ENTERED',
    'default' => false,
    'name' => 'date_entered',
  ),
  'date_modified' => 
  array (
    'width' => '15%',
    'label' => 'LBL_DATE_MODIFIED',
    'name' => 'date_modified',
    'default' => false,
  ),
  'accref' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_ACCREF',
    'width' => '10%',
    'default' => false,
    'name' => 'accref',
  ),
  'created_by' => 
  array (
    'width' => '8%',
    'label' => 'LBL_CREATED',
    'name' => 'created_by',
    'default' => false,
  ),
  'carriage_rate_charged' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_CARRIAGE_RATE_CHARGED',
    'currency_format' => true,
    'width' => '10%',
    'default' => false,
    'name' => 'carriage_rate_charged',
  ),
  'assigned_user_name' => 
  array (
    'width' => '8%',
    'label' => 'LBL_LIST_ASSIGNED_USER',
    'name' => 'assigned_user_name',
    'default' => false,
  ),
);
