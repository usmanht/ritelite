<?php
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

$relationships = array (
  'rl_sales_order_line_items_modified_user' => 
  array (
    'id' => '1481cb30-f597-c3e3-3208-55672bbbbafd',
    'relationship_name' => 'rl_sales_order_line_items_modified_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'RL_Sales_Order_Line_Items',
    'rhs_table' => 'rl_sales_order_line_items',
    'rhs_key' => 'modified_user_id',
    'join_table' => NULL,
    'join_key_lhs' => NULL,
    'join_key_rhs' => NULL,
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => NULL,
    'relationship_role_column_value' => NULL,
    'reverse' => 0,
    'deleted' => 0,
    'readonly' => true,
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
  ),
  'rl_sales_order_line_items_created_by' => 
  array (
    'id' => '150512cf-efb1-115c-9154-55672bff810d',
    'relationship_name' => 'rl_sales_order_line_items_created_by',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'RL_Sales_Order_Line_Items',
    'rhs_table' => 'rl_sales_order_line_items',
    'rhs_key' => 'created_by',
    'join_table' => NULL,
    'join_key_lhs' => NULL,
    'join_key_rhs' => NULL,
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => NULL,
    'relationship_role_column_value' => NULL,
    'reverse' => 0,
    'deleted' => 0,
    'readonly' => true,
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
  ),
  'rl_sales_order_line_items_assigned_user' => 
  array (
    'id' => '157fc87f-0123-4aaa-21e5-55672b6ecdcd',
    'relationship_name' => 'rl_sales_order_line_items_assigned_user',
    'lhs_module' => 'Users',
    'lhs_table' => 'users',
    'lhs_key' => 'id',
    'rhs_module' => 'RL_Sales_Order_Line_Items',
    'rhs_table' => 'rl_sales_order_line_items',
    'rhs_key' => 'assigned_user_id',
    'join_table' => NULL,
    'join_key_lhs' => NULL,
    'join_key_rhs' => NULL,
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => NULL,
    'relationship_role_column_value' => NULL,
    'reverse' => 0,
    'deleted' => 0,
    'readonly' => true,
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
  ),
  'rl_sales_order_line_items_aos_products' => 
  array (
    'id' => '15f9e2c1-809d-4dd0-8aa0-55672b4a882b',
    'relationship_name' => 'rl_sales_order_line_items_aos_products',
    'lhs_module' => 'AOS_Products',
    'lhs_table' => 'aos_products',
    'lhs_key' => 'id',
    'rhs_module' => 'RL_Sales_Order_Line_Items',
    'rhs_table' => 'rl_sales_order_line_items',
    'rhs_key' => 'product_id',
    'join_table' => NULL,
    'join_key_lhs' => NULL,
    'join_key_rhs' => NULL,
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => NULL,
    'relationship_role_column_value' => NULL,
    'reverse' => 0,
    'deleted' => 0,
    'readonly' => true,
    'rhs_subpanel' => NULL,
    'lhs_subpanel' => NULL,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
  ),
  'accounts_rl_sales_order_line_items_1' => 
  array (
    'id' => '6516dd51-c941-b68a-5d4b-55672b220372',
    'relationship_name' => 'accounts_rl_sales_order_line_items_1',
    'lhs_module' => 'Accounts',
    'lhs_table' => 'accounts',
    'lhs_key' => 'id',
    'rhs_module' => 'RL_Sales_Order_Line_Items',
    'rhs_table' => 'rl_sales_order_line_items',
    'rhs_key' => 'id',
    'join_table' => 'accounts_rl_sales_order_line_items_1_c',
    'join_key_lhs' => 'accounts_rl_sales_order_line_items_1accounts_ida',
    'join_key_rhs' => 'accounts_rl_sales_order_line_items_1rl_sales_order_line_items_idb',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => NULL,
    'relationship_role_column_value' => NULL,
    'reverse' => 0,
    'deleted' => 0,
    'readonly' => true,
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'from_studio' => true,
    'is_custom' => true,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'accounts_rl_sales_order_line_items_2' => 
  array (
    'id' => '65973350-608d-f472-30d0-55672b90feb4',
    'relationship_name' => 'accounts_rl_sales_order_line_items_2',
    'lhs_module' => 'Accounts',
    'lhs_table' => 'accounts',
    'lhs_key' => 'id',
    'rhs_module' => 'RL_Sales_Order_Line_Items',
    'rhs_table' => 'rl_sales_order_line_items',
    'rhs_key' => 'id',
    'join_table' => 'accounts_rl_sales_order_line_items_2_c',
    'join_key_lhs' => 'accounts_rl_sales_order_line_items_2accounts_ida',
    'join_key_rhs' => 'accounts_rl_sales_order_line_items_2rl_sales_order_line_items_idb',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => NULL,
    'relationship_role_column_value' => NULL,
    'reverse' => 0,
    'deleted' => 0,
    'readonly' => true,
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'from_studio' => true,
    'is_custom' => true,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'contacts_rl_sales_order_line_items_1' => 
  array (
    'id' => '6d19df30-5623-5543-3860-55672b00f24a',
    'relationship_name' => 'contacts_rl_sales_order_line_items_1',
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'RL_Sales_Order_Line_Items',
    'rhs_table' => 'rl_sales_order_line_items',
    'rhs_key' => 'id',
    'join_table' => 'contacts_rl_sales_order_line_items_1_c',
    'join_key_lhs' => 'contacts_rl_sales_order_line_items_1contacts_ida',
    'join_key_rhs' => 'contacts_rl_sales_order_line_items_1rl_sales_order_line_items_idb',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => NULL,
    'relationship_role_column_value' => NULL,
    'reverse' => 0,
    'deleted' => 0,
    'readonly' => true,
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'from_studio' => true,
    'is_custom' => true,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'contacts_rl_sales_order_line_items_2' => 
  array (
    'id' => '6d9a09d0-9cba-1d77-9057-55672b987d0f',
    'relationship_name' => 'contacts_rl_sales_order_line_items_2',
    'lhs_module' => 'Contacts',
    'lhs_table' => 'contacts',
    'lhs_key' => 'id',
    'rhs_module' => 'RL_Sales_Order_Line_Items',
    'rhs_table' => 'rl_sales_order_line_items',
    'rhs_key' => 'id',
    'join_table' => 'contacts_rl_sales_order_line_items_2_c',
    'join_key_lhs' => 'contacts_rl_sales_order_line_items_2contacts_ida',
    'join_key_rhs' => 'contacts_rl_sales_order_line_items_2rl_sales_order_line_items_idb',
    'relationship_type' => 'one-to-many',
    'relationship_role_column' => NULL,
    'relationship_role_column_value' => NULL,
    'reverse' => 0,
    'deleted' => 0,
    'readonly' => true,
    'rhs_subpanel' => 'default',
    'lhs_subpanel' => NULL,
    'from_studio' => true,
    'is_custom' => true,
    'relationship_only' => false,
    'for_activities' => false,
  ),
  'rl_sales_order_line_items_aos_products_1' => 
  array (
    'rhs_label' => 'Products',
    'lhs_label' => 'Sales Order Line Items',
    'rhs_subpanel' => 'default',
    'lhs_module' => 'RL_Sales_Order_Line_Items',
    'rhs_module' => 'AOS_Products',
    'relationship_type' => 'one-to-many',
    'readonly' => true,
    'deleted' => false,
    'relationship_only' => false,
    'for_activities' => false,
    'is_custom' => false,
    'from_studio' => true,
    'relationship_name' => 'rl_sales_order_line_items_aos_products_1',
  ),
);