<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/


include_once('include/database/MssqlManager.php');

/**
 * SQL Server driver for FreeTDS
 */
class FreeTDSManager1 extends MssqlManager
{
    public $dbName = 'FreeTDS SQL Server';
    public $variant = 'freetds';
    public $label = 'LBL_MSSQL2';

    protected $capabilities = array(
        "affected_rows" => true,
        'fulltext' => true,
        'limit_subquery' => true,
    );

    protected $type_map = array(
        'int'      => 'int',
        'double'   => 'float',
        'float'    => 'float',
        'float'    => 'float(9)',
        'uint'     => 'int',
        'ulong'    => 'int',
        'long'     => 'bigint',
        'short'    => 'smallint',
        'varchar'  => 'nvarchar',
        'text'     => 'nvarchar(max)',
        'longtext' => 'nvarchar(max)',
        'date'     => 'datetime',
        'enum'     => 'nvarchar',
        'relate'   => 'nvarchar',
        'multienum'=> 'nvarchar(max)',
        'html'     => 'nvarchar(max)',
        'longhtml' => 'text',
        'datetime' => 'datetime',
        'datetimecombo' => 'datetime',
        'time'     => 'datetime',
        'bool'     => 'bit',
        'tinyint'  => 'tinyint',
        'char'     => 'char',
        'blob'     => 'nvarchar(max)',
        'longblob' => 'nvarchar(max)',
        'currency' => 'decimal(26,6)',
        'decimal'  => 'decimal',
        'decimal2' => 'decimal',
        'id'       => 'varchar(36)',
        'url'      => 'nvarchar',
        'encrypt'  => 'nvarchar',
        'file'     => 'nvarchar',
        'decimal_tpl' => 'decimal(%d, %d)',
    );

    public function query($sql, $dieOnError = false, $msg = '', $suppress = false, $keepResult = false)
    {
        global $app_strings;
        if(is_array($sql)) {
            return $this->queryArray($sql, $dieOnError, $msg, $suppress);
        }

        $sql = $this->_appendN($sql);
        return parent::query($sql, $dieOnError, $msg, $suppress, $keepResult);
    }

    /**
     * Check if this driver can be used
     * @return bool
     */
    public function valid()
    {
        return function_exists("mssql_connect") && DBManagerFactory::isFreeTDS();
    }

    /**
     * Compares two vardefs
     *
     * @param  array  $fielddef1 This is from the database
     * @param  array  $fielddef2 This is from the vardef
     * @param bool $ignoreName Ignore name-only differences?
     * @return bool   true if they match, false if they don't
     */
    public function compareVarDefs($fielddef1, $fielddef2, $ignoreName = false)
    {
        foreach ( $fielddef1 as $key => $value ) {
            if ($key == 'name' && $ignoreName)
                continue;
            if (isset($fielddef2[$key]))
            {
                if (!is_array($fielddef1[$key]) && !is_array($fielddef2[$key]))
                {
                    if (strtolower($fielddef1[$key]) == strtolower($fielddef2[$key]))
                    {
                        continue;
                    }
                }
                else
                {
                    if (array_map('strtolower', $fielddef1[$key]) == array_map('strtolower',$fielddef2[$key]))
                    {
                        continue;
                    }
                }
            }
            //Ignore len if its not set in the vardef
            if ($key == 'len' && empty($fielddef2[$key]))
                continue;
            // if the length in db is greather than the vardef, ignore it
            if ($key == 'len' && ($fielddef1[$key] >= $fielddef2[$key])) {
                continue;
            }
            if(($key == 'len' || $key == 'type') && $fielddef1['type'] === 'ntext' && $fielddef2['len'] === 'max'){
                continue;
            }
            if(($key === 'type' || $key === 'len') && $fielddef1['type'] === 'real' && $fielddef2['type'] === 'float'  && $fielddef1['len'] == 4 && $fielddef2['len'] == 8){
                continue;
            }
            if($key === 'len' && $fielddef1['type'] === 'bit'){
                continue;
            }
            if(($key === 'type' || $key === 'len') && $fielddef1['type'] === 'real' && $fielddef2['type'] === 'float'  && $fielddef1['len'] == 4 && $fielddef2['len'] == 9){
               continue;
            }

            return false;
        }

        return true;
    }
}
