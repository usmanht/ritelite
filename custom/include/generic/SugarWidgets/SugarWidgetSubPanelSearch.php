<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/**
 * SugarWidgetSubPanelSearch - Adds search to Subpanels
 * ---------------------------------------------------------------
 * This defins the layout and custom javascript to enable search on 
 * SubPanels
 * ---------------------------------------------------------------
 * By SalesAgility
 * September 5th, 2012
 */






class SugarWidgetSubPanelSearch extends SugarWidget
{

//TODO rename defines to layout defs and make it a member variable instead of passing it multiple layers with extra copying.

	/** Take the keys for the strings and look them up.  Module is literal, the rest are label keys
	*/

	/** This default function is used to create the HTML for a simple button */
	function display($defines, $additionalFormFields = null)
	{		
		global $app_list_strings;

		$subpanelName = $defines['subpanel_definition']->name;
		
		$html = '<form id="'.$subpanelName.'.spsform"><div style="white-space:nowrap !important;float:right;">&nbsp;&nbsp;&nbsp;';
		/*
		//Activity Type Dropdown search
		$activity_type_tmp = array('' => '');
		
		$activity_type_tmp['Meetings'] = 'Meeting';
		
		$activity_type_tmp = array_merge($activity_type_tmp, $app_list_strings['Cust_Activity_Call_Type']);
		if($subpanelName == 'history'){
			$activity_type_tmp['Notes'] = 'Note';
		}
		
		$html .= 'Activity Type <select name="'.$subpanelName.'" id="'.$subpanelName.'" >'.get_select_options_with_id($activity_type_tmp, $_REQUEST['activity']).'</select>&nbsp;&nbsp;';
		*/

		// SA added -  Date range search and number range search
		$html .= ' Date:
		<select id="'.$subpanelName.'spsdate_advanced_range_choice" name="'.$subpanelName.'spsdate_advanced_range_choice" style="width:125px !important;" onchange="'.$subpanelName.'spsdate_advanced_range_change(this.value);">
		<option label="Is Between" value="between">Is Between</option>
		<option label="Equals" value="equals">Equals</option>
		<option label="After" value="after">After</option>
		<option label="Before" value="before">Before</option>

		</select>


		<span id="'.$subpanelName.'spsdate_advanced_range_div" style="display:none;">
		<input autocomplete="off" type="text" name="'.$subpanelName.'range_spsdate_advanced" id="'.$subpanelName.'range_spsdate_advanced" value=\''.$_REQUEST['date'].'\' title=\'\'   size="11" style="width:100px !important;">
		<img src="themes/Sugar5/images/jscalendar.gif" style="position:relative;top:-1px;width:12px;"    alt="Enter Date" other_attributes=align="absmiddle" border="0" id="'.$subpanelName.'spsdate_advanced_trigger" alt="" />
		</span>

		<span id="'.$subpanelName.'spsdate_advanced_between_range_div" style="display:\'\';">
		<input autocomplete="off" type="text" name="'.$subpanelName.'start_range_spsdate_advanced" id="'.$subpanelName.'start_range_spsdate_advanced" value=\''.$_REQUEST['date_start'].'\' title=\'\'  tabindex=\'\' size="11" style="width:100px !important;">
		<img src="themes/Sugar5/images/jscalendar.gif" style="position:relative;top:-1px;width:12px;"     alt="Array.LBL_ENTER_DATE other_attributes=align="absmiddle" border="0" id="'.$subpanelName.'start_range_spsdate_advanced_trigger"" />

		And
		<input autocomplete="off" type="text" name="'.$subpanelName.'end_range_spsdate_advanced" id="'.$subpanelName.'end_range_spsdate_advanced" value=\''.$_REQUEST['date_end'].'\' title=\'\'  tabindex=\'\' size="11" style="width:100px !important;" maxlength="10">
		<img src="themes/Sugar5/images/jscalendar.gif" style="position:relative;top:-1px;width:12px;"     alt="Array.LBL_ENTER_DATE other_attributes=align="absmiddle" border="0" id="'.$subpanelName.'end_range_spsdate_advanced_trigger"" />
        &nbsp;&nbsp
		</span>

		Total Value: <select id="'.$subpanelName.'spsdate_advanced_number_choice" name="'.$subpanelName.'spsdate_advanced_number_choice" style="width:125px !important;" onchange="'.$subpanelName.'spsdate_advanced_number_change(this.value);">
		<option label="Is Between" value="between">Is Between</option>
		<option label="Equals" value="equals">Equals</option>
		<option label="Greater Than" value="greater">Greater Than</option>
		<option label="Less Than" value="before">Less Than</option>

		</select>

		<span id="'.$subpanelName.'spsnumber_advanced_number_div" style="display:none;">

		<input autocomplete="off" type="text" name="'.$subpanelName.'range_spsnumber_advanced" id="'.$subpanelName.'range_spsnumber_advanced" value=\''.$_REQUEST['amount'].'\' title=\'\'   size="11" style="width:100px !important;">

		</span>

		<span id="'.$subpanelName.'spsdate_advanced_between_number_div" style="display:\'\';">

		<input autocomplete="off" type="text" name="'.$subpanelName.'start_range_spsnumber_advanced" id="'.$subpanelName.'start_range_spsnumber_advanced" value=\''.$_REQUEST['total_amount1'].'\' title=\'\'  tabindex=\'\' size="11" style="width:100px !important;">

		And
		<input autocomplete="off" type="text" name="'.$subpanelName.'end_range_spsnumber_advanced" id="'.$subpanelName.'end_range_spsnumber_advanced" value=\''.$_REQUEST['total_amount2'].'\' title=\'\'  tabindex=\'\' size="11" style="width:100px !important;" maxlength="10">

        &nbsp;&nbsp
		</span>


		<script type=\'text/javascript\' id=\''.$subpanelName.'sc1\'>

		Calendar.setup ({
            inputField : "'.$subpanelName.'range_spsdate_advanced",
            daFormat : cal_date_format,
            button : "'.$subpanelName.'spsdate_advanced_trigger",
            singleClick : true,
            dateStr : "",
            startWeekday: 0,
            step : 1,
            weekNumbers:false
            }
        );

		Calendar.setup ({
            inputField : "'.$subpanelName.'start_range_spsdate_advanced",
            daFormat : cal_date_format,
            button : "'.$subpanelName.'start_range_spsdate_advanced_trigger",
            singleClick : true,
            dateStr : "",
            step : 1,
            weekNumbers:false
            }
		);

		Calendar.setup ({
            inputField : "'.$subpanelName.'end_range_spsdate_advanced",
            daFormat : cal_date_format,
            button : "'.$subpanelName.'end_range_spsdate_advanced_trigger",
            singleClick : true,
            dateStr : "",
            step : 1,
            weekNumbers:false
            }
		);

		function '.$subpanelName.'spsdate_advanced_range_change(val)
		{
		  if(val == \'between\') {
			 document.getElementById("'.$subpanelName.'range_spsdate_advanced").value = \'\';
			 document.getElementById("'.$subpanelName.'spsdate_advanced_range_div").style.display = \'none\';
			 document.getElementById("'.$subpanelName.'spsdate_advanced_between_range_div").style.display = \'\';
		  } else if (val == \'equals\' || val == \'!=\' || val == \'after\' || val == \'before\') {
			 if((/^\[.*\]$/).test(document.getElementById("'.$subpanelName.'range_spsdate_advanced").value))
			 {
				document.getElementById("'.$subpanelName.'range_spsdate_advanced").value = \'\';
			 }
			 document.getElementById("'.$subpanelName.'start_range_spsdate_advanced").value = \'\';
			 document.getElementById("'.$subpanelName.'end_range_spsdate_advanced").value = \'\';
			 document.getElementById("'.$subpanelName.'spsdate_advanced_range_div").style.display = \'\';
			 document.getElementById("'.$subpanelName.'spsdate_advanced_between_range_div").style.display = \'none\';
		  } else {
			 document.getElementById("'.$subpanelName.'range_spsdate_advanced").value = \'[\' + val + \']\';
			 document.getElementById("'.$subpanelName.'start_range_spsdate_advanced").value = \'\';
			 document.getElementById("'.$subpanelName.'end_range_spsdate_advanced").value = \'\';
			 document.getElementById("'.$subpanelName.'spsdate_advanced_range_div").style.display = \'none\';
			 document.getElementById("'.$subpanelName.'spsdate_advanced_between_range_div").style.display = \'none\';
		  }
		}

		function '.$subpanelName.'spsdate_advanced_number_change(val)
		{
		  if(val == \'between\') {
			 document.getElementById("'.$subpanelName.'range_spsnumber_advanced").value = \'\';
			 document.getElementById("'.$subpanelName.'spsnumber_advanced_number_div").style.display = \'none\';
			 document.getElementById("'.$subpanelName.'spsdate_advanced_between_number_div").style.display = \'\';
		  } else if (val == \'equals\' || val == \'!=\' || val == \'greater\' || val == \'before\') {
			 if((/^\[.*\]$/).test(document.getElementById("'.$subpanelName.'range_spsnumber_advanced").value))
			 {
				document.getElementById("'.$subpanelName.'range_spsnumber_advanced").value = \'\';
			 }
			 document.getElementById("'.$subpanelName.'start_range_spsnumber_advanced").value = \'\';
			 document.getElementById("'.$subpanelName.'end_range_spsnumber_advanced").value = \'\';
			 document.getElementById("'.$subpanelName.'spsnumber_advanced_number_div").style.display = \'\';
			 document.getElementById("'.$subpanelName.'spsdate_advanced_between_number_div").style.display = \'none\';
		  } else {
			 document.getElementById("'.$subpanelName.'range_spsnumber_advanced").value = \'[\' + val + \']\';
			 document.getElementById("'.$subpanelName.'start_range_spsnumber_advanced").value = \'\';
			 document.getElementById("'.$subpanelName.'end_range_spsnumber_advanced").value = \'\';
			 document.getElementById("'.$subpanelName.'spsnumber_advanced_number_div").style.display = \'none\';
			 document.getElementById("'.$subpanelName.'spsdate_advanced_between_number_div").style.display = \'none\';
		  }
		}

		'.$subpanelName.'spsdate_advanced_range_change();
		'.$subpanelName.'spsdate_advanced_number_change();


		var '.$subpanelName.'spsdate_advanced_range_reset = function()
		{
		'.$subpanelName.'spsdate_advanced_range_change(\'=\');
		}

		var '.$subpanelName.'spsdate_advanced_number_reset = function()
		{
		'.$subpanelName.'spsdate_advanced_number_change(\'=\');
		}




		YAHOO.util.Event.onDOMReady(function() {
		if(document.getElementById(\'search_form_clear\'))
		{
		YAHOO.util.Event.addListener(\'search_form_clear\', \'click\', '.$subpanelName.'spsdate_advanced_range_reset);
		}

		});

		'.$subpanelName.'spsdate_advanced_range_change(document.getElementById("'.$subpanelName.'spsdate_advanced_range_choice").value);
		'.$subpanelName.'spsdate_advanced_number_change(document.getElementById("'.$subpanelName.'spsdate_advanced_number_choice").value);


			function '.$subpanelName.'searchSubPanel(child_field,url,force_load,layout_def_key)
			{

				var form = document.getElementById("'.$subpanelName.'.spsform");
				var inline = 1;
				force_load = true;
				if ( typeof(force_load) == \'undefined\' || force_load == null)
				{
					force_load = false;
				}

				if (force_load || typeof( child_field_loaded[child_field] ) == \'undefined\')
				{
					request_map[request_id] = child_field;
					if ( typeof (url) == \'undefined\' || url == null)
					{
						var module = get_module_name();
						var id = get_record_id();
						if ( typeof(layout_def_key) == \'undefined\' || layout_def_key == null ) {
							layout_def_key = get_layout_def_key();
						}

						url = \'index.php?sugar_body_only=1&module=\'+module+\'&subpanel=\'+child_field+\'&action=SubPanelSearch&inline=\' + inline + \'&record=\'+id + \'&layout_def_key=\'+ layout_def_key + \'&offset=0\'
								+ \'&date_range=\' + form.'.$subpanelName.'spsdate_advanced_range_choice.value + \'&date_start=\' + form.'.$subpanelName.'start_range_spsdate_advanced.value + \'&date_end=\' + form.'.$subpanelName.'end_range_spsdate_advanced.value
								+ \'&date=\' + form.'.$subpanelName.'range_spsdate_advanced.value + \' &amount=\' + form.'.$subpanelName.'range_spsnumber_advanced.value + \'&amount_range=\' + form.'.$subpanelName.'spsdate_advanced_number_choice.value + \'&total_amount1=\' + form.'.$subpanelName.'start_range_spsnumber_advanced.value + \'&total_amount2=\' + form.'.$subpanelName.'end_range_spsnumber_advanced.value;

								//+ \'&activity=\' + form.'.$subpanelName.'.value

					}

					if ( url.indexOf(\'http://\') != 0  && url.indexOf(\'https://\') != 0)
					{
						url = \'\'+url ;
					}

					current_subpanel_url = url;
					// http_fetch_async(url,got_data,request_id++);
					var returnstuff = http_fetch_sync(url+ \'&inline=\' + inline + \'&ajaxSubpanel=true\');
					request_id++;
					got_data(returnstuff, inline);
				}
				else
				{
					var subpanel = document.getElementById(\'subpanel_\'+child_field);
					subpanel.style.display = \'\';

					set_div_cookie(subpanel.cookie_name, \'\');

					if (current_child_field != \'\' && child_field != current_child_field)
					{
						hideSubPanel(current_child_field);
					}

					current_child_field = child_field;
				}
				if(typeof(url) != \'undefined\' && url != null && url.indexOf(\'refresh_page=1\') > 0){
					document.location.reload();
				}

                eval(document.getElementById(child_field+"sc1").innerHTML);

                document.getElementById(child_field+"spsdate_advanced_range_choice").value = form.'.$subpanelName.'spsdate_advanced_range_choice.value;
                '.$subpanelName.'spsdate_advanced_range_change(form.'.$subpanelName.'spsdate_advanced_range_choice.value);

                document.getElementById(child_field+"spsdate_advanced_number_choice").value = form.'.$subpanelName.'spsdate_advanced_number_choice.value;
                '.$subpanelName.'spsdate_advanced_number_change(form.'.$subpanelName.'spsdate_advanced_number_choice.value);

                //document.getElementById(child_field+"").value = form.'.$subpanelName.'.value;

                document.getElementById(child_field+"range_spsdate_advanced").value = form.'.$subpanelName.'range_spsdate_advanced.value;
                document.getElementById(child_field+"start_range_spsdate_advanced").value = form.'.$subpanelName.'start_range_spsdate_advanced.value;
                document.getElementById(child_field+"end_range_spsdate_advanced").value = form.'.$subpanelName.'end_range_spsdate_advanced.value;

                document.getElementById(child_field+"range_spsnumber_advanced").value = form.'.$subpanelName.'range_spsnumber_advanced.value;
                document.getElementById(child_field+"start_range_spsnumber_advanced").value = form.'.$subpanelName.'start_range_spsnumber_advanced.value;
                document.getElementById(child_field+"end_range_spsnumber_advanced").value = form.'.$subpanelName.'end_range_spsnumber_advanced.value;

			}

			function clearSearchSubPanel(child_field,url,force_load,layout_def_key)
			{
				var inline = 1;
				force_load = true;
				if ( typeof(force_load) == \'undefined\' || force_load == null)
				{
					force_load = false;
				}

				if (force_load || typeof( child_field_loaded[child_field] ) == \'undefined\')
				{
					request_map[request_id] = child_field;
					if ( typeof (url) == \'undefined\' || url == null)
					{
						var module = get_module_name();
						var id = get_record_id();
						if ( typeof(layout_def_key) == \'undefined\' || layout_def_key == null ) {
							layout_def_key = get_layout_def_key();
						}

						url = \'index.php?sugar_body_only=1&module=\'+module+\'&subpanel=\'+child_field+\'&action=SubPanelViewer&inline=\' + inline + \'&record=\'+id + \'&layout_def_key=\'+ layout_def_key + \'&offset=0\';
					}

					if ( url.indexOf(\'http://\') != 0  && url.indexOf(\'https://\') != 0)
					{
						url = \'\'+url ;
					}

					current_subpanel_url = url;
					// http_fetch_async(url,got_data,request_id++);
					var returnstuff = http_fetch_sync(url+ \'&inline=\' + inline + \'&ajaxSubpanel=true\');
					request_id++;
					got_data(returnstuff, inline);
				}
				else
				{
					var subpanel = document.getElementById(\'subpanel_\'+child_field);
					subpanel.style.display = \'\';

					set_div_cookie(subpanel.cookie_name, \'\');

					if (current_child_field != \'\' && child_field != current_child_field)
					{
						hideSubPanel(current_child_field);
					}

					current_child_field = child_field;
				}
				if(typeof(url) != \'undefined\' && url != null && url.indexOf(\'refresh_page=1\') > 0){
					document.location.reload();
				}
                eval(document.getElementById(child_field+"sc1").innerHTML);
			}

			function showSearchSubPanel(child_field, url, force_load, layout_def_key, activity, date_range, date_start, date_end, date) {
                var inline = 1;
                if (typeof force_load == "undefined" || force_load == null) {
                    force_load = false;
                }
                if (force_load || typeof child_field_loaded[child_field] == "undefined") {
                    request_map[request_id] = child_field;
                    if (typeof url == "undefined" || url == null) {
                        var module = get_module_name();
                        var id = get_record_id();
                        if (typeof layout_def_key == "undefined" || layout_def_key == null) {
                            layout_def_key = get_layout_def_key();
                        }
                        url = "index.php?sugar_body_only=1&module=" + module + "&subpanel=" + child_field + "&action=SubPanelSearch&inline=" + inline + "&record=" + id + "&layout_def_key=" + layout_def_key;
                    }
                    if (url.indexOf("http://") != 0 && url.indexOf("https://") != 0) {
                        url = "" + url + "&action=SubPanelSearch&subpanel=" + child_field;
                    }
                    current_subpanel_url = url;
                    var returnstuff = http_fetch_sync(url + "&inline=" + inline + "&ajaxSubpanel=true");
                    request_id++;
                    got_data(returnstuff, inline);
                } else {
                    var subpanel = document.getElementById("subpanel_" + child_field);
                    subpanel.style.display = "";
                    set_div_cookie(subpanel.cookie_name, "");
                    if (current_child_field != "" && child_field != current_child_field) {
                        hideSubPanel(current_child_field);
                    }
                    current_child_field = child_field;
                }
                if (typeof url != "undefined" && url != null && url.indexOf("refresh_page=1") > 0) {
                    document.location.reload();
                }

                eval(document.getElementById(child_field+"sc1").innerHTML);

                document.getElementById(child_field+"spsdate_advanced_range_choice").value = date_range;
                '.$subpanelName.'spsdate_advanced_range_change(date_range);
                document.getElementById(child_field+"").value = activity;

                document.getElementById(child_field+"range_spsdate_advanced").value = date;
                document.getElementById(child_field+"start_range_spsdate_advanced").value = date_start;
                document.getElementById(child_field+"end_range_spsdate_advanced").value = date_end;
            }




		</script>';

		// search button
		$html .= "&nbsp;&nbsp;<input title='Search' class='button' type='submit' onclick='".$subpanelName."searchSubPanel(\"$subpanelName\");return false;' name='spsearch' id='spsearch' value='Search' />
				&nbsp;<input title='Clear' class='button' type='button' onclick='clearSearchSubPanel(\"$subpanelName\");' name='spclear' id='spclear' value='Clear' />&nbsp;&nbsp;
					</div></form>";
	
        return $html;

	}
}
?>
