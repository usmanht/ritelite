<?php
// created: 2018-03-19 22:58:57
$GLOBALS['tabStructure'] = array (
  'LBL_TABGROUP_SALES' => 
  array (
    'label' => 'LBL_TABGROUP_SALES',
    'modules' => 
    array (
      0 => 'Home',
      1 => 'Accounts',
      2 => 'Contacts',
      3 => 'Opportunities',
      4 => 'AOS_Quotes',
      5 => 'AOS_Invoices',
      6 => 'RL_Sales_Order_Line_Items',
      7 => 'AOS_Contracts',
    ),
  ),
  'LBL_TABGROUP_MARKETING' => 
  array (
    'label' => 'LBL_TABGROUP_MARKETING',
    'modules' => 
    array (
      0 => 'Home',
      1 => 'Accounts',
      2 => 'Contacts',
      3 => 'Leads',
      4 => 'Campaigns',
      5 => 'Prospects',
      6 => 'ProspectLists',
    ),
  ),
  'LBL_TABGROUP_SUPPORT' => 
  array (
    'label' => 'LBL_TABGROUP_SUPPORT',
    'modules' => 
    array (
      0 => 'Home',
      1 => 'Accounts',
      2 => 'Contacts',
      3 => 'Cases',
      4 => 'Bugs',
      5 => 'Documents',
    ),
  ),
  'LBL_TABGROUP_ACTIVITIES' => 
  array (
    'label' => 'LBL_TABGROUP_ACTIVITIES',
    'modules' => 
    array (
      0 => 'Home',
      1 => 'Calendar',
      2 => 'Calls',
      3 => 'Meetings',
      4 => 'Emails',
      5 => 'Tasks',
      6 => 'Notes',
    ),
  ),
  'LBL_TABGROUP_COLLABORATION' => 
  array (
    'label' => 'LBL_TABGROUP_COLLABORATION',
    'modules' => 
    array (
      0 => 'Home',
      1 => 'Emails',
      2 => 'Meetings',
      3 => 'Documents',
      4 => 'Project',
      5 => 'Timesheet',
      6 => 'Cases',
    ),
  ),
  'LBL_GROUPTAB5_1395767308' => 
  array (
    'label' => 'LBL_GROUPTAB5_1395767308',
    'modules' => 
    array (
      0 => 'AOS_Products',
      1 => 'AOS_Product_Categories',
    ),
  ),
  'LBL_GROUPTAB6_1521500336' => 
  array (
    'label' => 'LBL_GROUPTAB6_1521500336',
    'modules' => 
    array (
      0 => 'AOR_Reports',
      1 => 'AOR_Scheduled_Reports',
      2 => 'KReports',
      3 => 'AOW_WorkFlow',
    ),
  ),
);