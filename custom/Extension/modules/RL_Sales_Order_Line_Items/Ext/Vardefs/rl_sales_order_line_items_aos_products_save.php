<?php
/**
 * Created by PhpStorm.
 * User: graeme
 * Date: 06/04/16
 * Time: 13:49
 */

$dictionary['RL_Sales_Order_Line_Items']['fields']['line_items']['function']= array(
    'name' => 'display_lines',
    'returns' => 'html',
    'include' => 'modules/RL_Sales_Order_Line_Items/Line_Items.php'
);


$dictionary['RL_Sales_Order_Line_Items']['fields']['shipping_tax_amt']['function']= array(
    'name' => 'display_lines',
    'returns' => 'html',
    'include' => 'modules/RL_Sales_Order_Line_Items/Line_Items.php'
);
