<?php
// created: 2014-11-12 17:42:29
$dictionary["AOS_Invoices"]["fields"]["documents_aos_invoices_1"] = array (
  'name' => 'documents_aos_invoices_1',
  'type' => 'link',
  'relationship' => 'documents_aos_invoices_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'vname' => 'LBL_DOCUMENTS_AOS_INVOICES_1_FROM_DOCUMENTS_TITLE',
);
