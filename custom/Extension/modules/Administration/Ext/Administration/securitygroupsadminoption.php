<?php

$admin_option_defs=array();
$admin_option_defs['Administration']['securitygroup_management']= array('SecurityGroups','LBL_MANAGE_SECURITYGROUPS_TITLE','LBL_MANAGE_SECURITYGROUPS','./index.php?module=SecurityGroups&action=index');
$admin_option_defs['Administration']['securitygroup_config']= array('SecurityGroups','LBL_CONFIG_SECURITYGROUPS_TITLE','LBL_CONFIG_SECURITYGROUPS','./index.php?module=SecurityGroups&action=config');

  



$admin_option_defs['Administration']['securitygroup_dashletpush']= array('icon_home','LBL_SECURITYGROUPS_DASHLETPUSH_TITLE','LBL_SECURITYGROUPS_DASHLETPUSH','./index.php?module=SecurityGroups&action=DashletPush');
$admin_option_defs['Administration']['securitygroup_hookup']= array('PatchUpgrades','LBL_SECURITYGROUPS_HOOKUP_TITLE','LBL_SECURITYGROUPS_HOOKUP','./index.php?module=SecurityGroups&action=Hookup');

$admin_option_defs['Administration']['securitygroup_info']= array('helpInline','LBL_SECURITYGROUPS_INFO_TITLE','LBL_SECURITYGROUPS_INFO','./index.php?module=SecurityGroups&action=info');


$admin_option_defs['Administration']['securitygroup_sugaroutfitters']= array('helpInline','LBL_SECURITYGROUPS_SUGAROUTFITTERS_TITLE','LBL_SECURITYGROUPS_SUGAROUTFITTERS','https://www.sugaroutfitters.com');
  

$admin_group_header[]= array('LBL_SECURITYGROUPS','',false,$admin_option_defs, '');


?>