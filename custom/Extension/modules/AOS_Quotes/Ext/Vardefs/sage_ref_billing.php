<?php
$dictionary["AOS_Quotes"]["fields"]["sage_ref_billing"] = array(
    'id' => 'sage_ref_billing',
    'class' => 'sqsEnabled',
    'name' => 'sage_ref_billing',
    'vname' => 'LBL_SAGE_REF',
    'comments' => '',
    'help' => '',
    'module' => 'AOS_Quotes',
    'type' => 'varchar',
    'max_size' => '155',
    'reportable' => '0',
    //'source' => 'non-db',
    //'dbtype' => 'non-db',
    'studio' => 'visible'
);

$dictionary["AOS_Quotes"]["fields"]["sage_ref_shipping"] = array(
    'id' => 'sage_ref_shipping',
    'class' => 'sqsEnabled',
    'name' => 'sage_ref_shipping',
    'vname' => 'LBL_SAGE_REF1',
    'comments' => '',
    'help' => '',
    'module' => 'AOS_Quotes',
    'type' => 'varchar',
    'max_size' => '155',
    'reportable' => '0',
    //'source' => 'non-db',
    //'dbtype' => 'non-db',
    'studio' => 'visible'
);