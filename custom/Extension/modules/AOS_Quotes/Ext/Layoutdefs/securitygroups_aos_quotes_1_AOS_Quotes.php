<?php
 // created: 2014-05-02 12:12:37
$layout_defs["AOS_Quotes"]["subpanel_setup"]['securitygroups_aos_quotes_1'] = array (
  'order' => 100,
  'module' => 'SecurityGroups',
  'subpanel_name' => 'admin',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SECURITYGROUPS_AOS_QUOTES_1_FROM_SECURITYGROUPS_TITLE',
  'get_subpanel_data' => 'securitygroups_aos_quotes_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
