<?php
// created: 2014-01-07 11:53:04
$dictionary["User"]["fields"]["users_tasks_1"] = array (
  'name' => 'users_tasks_1',
  'type' => 'link',
  'relationship' => 'users_tasks_1',
  'source' => 'non-db',
  'module' => 'Tasks',
  'bean_name' => 'Task',
  'side' => 'right',
  'vname' => 'LBL_USERS_TASKS_1_FROM_TASKS_TITLE',
);
