<?php
 // created: 2014-10-22 17:01:59
$dictionary['Bug']['fields']['type']['default']='Defect';
$dictionary['Bug']['fields']['type']['len']=100;
$dictionary['Bug']['fields']['type']['required']=true;
$dictionary['Bug']['fields']['type']['audited']=true;
$dictionary['Bug']['fields']['type']['massupdate']='1';
$dictionary['Bug']['fields']['type']['comments']='The type of issue (ex: issue, feature)';
$dictionary['Bug']['fields']['type']['merge_filter']='disabled';

 ?>