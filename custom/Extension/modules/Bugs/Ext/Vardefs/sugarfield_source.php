<?php
 // created: 2014-10-22 17:02:43
$dictionary['Bug']['fields']['source']['len']=100;
$dictionary['Bug']['fields']['source']['required']=true;
$dictionary['Bug']['fields']['source']['audited']=true;
$dictionary['Bug']['fields']['source']['massupdate']='1';
$dictionary['Bug']['fields']['source']['comments']='An indicator of how the bug was entered (ex: via web, email, etc.)';
$dictionary['Bug']['fields']['source']['merge_filter']='disabled';

 ?>