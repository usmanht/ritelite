<?php
 // created: 2014-10-22 17:03:00
$dictionary['Bug']['fields']['product_category']['len']=100;
$dictionary['Bug']['fields']['product_category']['required']=true;
$dictionary['Bug']['fields']['product_category']['audited']=true;
$dictionary['Bug']['fields']['product_category']['massupdate']='1';
$dictionary['Bug']['fields']['product_category']['comments']='Where the bug was discovered (ex: Accounts, Contacts, Leads)';
$dictionary['Bug']['fields']['product_category']['merge_filter']='disabled';

 ?>