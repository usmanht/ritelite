<?php
// created: 2015-02-16 16:56:38
$dictionary["SecurityGroup"]["fields"]["timesheet_securitygroups_1"] = array (
  'name' => 'timesheet_securitygroups_1',
  'type' => 'link',
  'relationship' => 'timesheet_securitygroups_1',
  'source' => 'non-db',
  'module' => 'Timesheet',
  'bean_name' => 'Timesheet',
  'vname' => 'LBL_TIMESHEET_SECURITYGROUPS_1_FROM_TIMESHEET_TITLE',
);
