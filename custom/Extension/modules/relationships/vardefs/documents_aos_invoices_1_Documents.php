<?php
// created: 2014-11-12 17:42:29
$dictionary["Document"]["fields"]["documents_aos_invoices_1"] = array (
  'name' => 'documents_aos_invoices_1',
  'type' => 'link',
  'relationship' => 'documents_aos_invoices_1',
  'source' => 'non-db',
  'module' => 'AOS_Invoices',
  'bean_name' => 'AOS_Invoices',
  'vname' => 'LBL_DOCUMENTS_AOS_INVOICES_1_FROM_AOS_INVOICES_TITLE',
);
