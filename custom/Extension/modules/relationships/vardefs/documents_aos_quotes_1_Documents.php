<?php
// created: 2014-05-13 17:59:33
$dictionary["Document"]["fields"]["documents_aos_quotes_1"] = array (
  'name' => 'documents_aos_quotes_1',
  'type' => 'link',
  'relationship' => 'documents_aos_quotes_1',
  'source' => 'non-db',
  'module' => 'AOS_Quotes',
  'bean_name' => 'AOS_Quotes',
  'vname' => 'LBL_DOCUMENTS_AOS_QUOTES_1_FROM_AOS_QUOTES_TITLE',
);
