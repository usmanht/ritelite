<?php
// created: 2014-05-13 17:59:33
$dictionary["AOS_Quotes"]["fields"]["documents_aos_quotes_1"] = array (
  'name' => 'documents_aos_quotes_1',
  'type' => 'link',
  'relationship' => 'documents_aos_quotes_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'vname' => 'LBL_DOCUMENTS_AOS_QUOTES_1_FROM_DOCUMENTS_TITLE',
);
