<?php
// created: 2014-01-07 15:54:30
$dictionary["RL_SalesHistory"]["fields"]["rl_saleshistory_rl_saleshistoryitems_1"] = array (
  'name' => 'rl_saleshistory_rl_saleshistoryitems_1',
  'type' => 'link',
  'relationship' => 'rl_saleshistory_rl_saleshistoryitems_1',
  'source' => 'non-db',
  'module' => 'RL_SalesHistoryItems',
  'bean_name' => 'RL_SalesHistoryItems',
  'side' => 'right',
  'vname' => 'LBL_RL_SALESHISTORY_RL_SALESHISTORYITEMS_1_FROM_RL_SALESHISTORYITEMS_TITLE',
);
