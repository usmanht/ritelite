<?php
// created: 2017-10-05 08:42:03
$dictionary["AOS_Quotes"]["fields"]["cases_aos_quotes_2"] = array (
  'name' => 'cases_aos_quotes_2',
  'type' => 'link',
  'relationship' => 'cases_aos_quotes_2',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'vname' => 'LBL_CASES_AOS_QUOTES_2_FROM_CASES_TITLE',
  'id_name' => 'cases_aos_quotes_2cases_ida',
);
$dictionary["AOS_Quotes"]["fields"]["cases_aos_quotes_2_name"] = array (
  'name' => 'cases_aos_quotes_2_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CASES_AOS_QUOTES_2_FROM_CASES_TITLE',
  'save' => true,
  'id_name' => 'cases_aos_quotes_2cases_ida',
  'link' => 'cases_aos_quotes_2',
  'table' => 'cases',
  'module' => 'Cases',
  'rname' => 'name',
);
$dictionary["AOS_Quotes"]["fields"]["cases_aos_quotes_2cases_ida"] = array (
  'name' => 'cases_aos_quotes_2cases_ida',
  'type' => 'link',
  'relationship' => 'cases_aos_quotes_2',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_CASES_AOS_QUOTES_2_FROM_AOS_QUOTES_TITLE',
);
