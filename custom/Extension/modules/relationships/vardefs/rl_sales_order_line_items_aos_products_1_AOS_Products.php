<?php
// created: 2015-05-28 17:21:59
$dictionary["AOS_Products"]["fields"]["rl_sales_order_line_items_aos_products_1"] = array (
  'name' => 'rl_sales_order_line_items_aos_products_1',
  'type' => 'link',
  'relationship' => 'rl_sales_order_line_items_aos_products_1',
  'source' => 'non-db',
  'module' => 'RL_Sales_Order_Line_Items',
  'bean_name' => 'RL_Sales_Order_Line_Items',
  'vname' => 'LBL_RL_SALES_ORDER_LINE_ITEMS_AOS_PRODUCTS_1_FROM_RL_SALES_ORDER_LINE_ITEMS_TITLE',
  'id_name' => 'rl_sales_order_line_items_aos_products_1rl_sales_order_line_items_ida',
);
$dictionary["AOS_Products"]["fields"]["rl_sales_order_line_items_aos_products_1_name"] = array (
  'name' => 'rl_sales_order_line_items_aos_products_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_RL_SALES_ORDER_LINE_ITEMS_AOS_PRODUCTS_1_FROM_RL_SALES_ORDER_LINE_ITEMS_TITLE',
  'save' => true,
  'id_name' => 'rl_sales_order_line_items_aos_products_1rl_sales_order_line_items_ida',
  'link' => 'rl_sales_order_line_items_aos_products_1',
  'table' => 'rl_sales_order_line_items',
  'module' => 'RL_Sales_Order_Line_Items',
  'rname' => 'name',
);
$dictionary["AOS_Products"]["fields"]["rl_sales_order_line_items_aos_products_1rl_sales_order_line_items_ida"] = array (
  'name' => 'rl_sales_order_line_items_aos_products_1rl_sales_order_line_items_ida',
  'type' => 'link',
  'relationship' => 'rl_sales_order_line_items_aos_products_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_RL_SALES_ORDER_LINE_ITEMS_AOS_PRODUCTS_1_FROM_AOS_PRODUCTS_TITLE',
);
