<?php
// created: 2015-01-12 15:28:24
$dictionary["AOS_Invoices"]["fields"]["aos_invoices_securitygroups_1"] = array (
  'name' => 'aos_invoices_securitygroups_1',
  'type' => 'link',
  'relationship' => 'aos_invoices_securitygroups_1',
  'source' => 'non-db',
  'module' => 'SecurityGroups',
  'bean_name' => 'SecurityGroup',
  'vname' => 'LBL_AOS_INVOICES_SECURITYGROUPS_1_FROM_SECURITYGROUPS_TITLE',
);
