<?php
// created: 2014-04-05 18:16:31
$dictionary["Project"]["fields"]["documents_project_1"] = array (
  'name' => 'documents_project_1',
  'type' => 'link',
  'relationship' => 'documents_project_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'vname' => 'LBL_DOCUMENTS_PROJECT_1_FROM_DOCUMENTS_TITLE',
);
