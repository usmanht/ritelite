<?php
// created: 2014-05-07 16:21:34
$dictionary["RL_Sales_Order_Line_Items"]["fields"]["contacts_rl_sales_order_line_items_1"] = array (
  'name' => 'contacts_rl_sales_order_line_items_1',
  'type' => 'link',
  'relationship' => 'contacts_rl_sales_order_line_items_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_CONTACTS_RL_SALES_ORDER_LINE_ITEMS_1_FROM_CONTACTS_TITLE',
  'id_name' => 'contacts_rl_sales_order_line_items_1contacts_ida',
);
$dictionary["RL_Sales_Order_Line_Items"]["fields"]["contacts_rl_sales_order_line_items_1_name"] = array (
  'name' => 'contacts_rl_sales_order_line_items_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_RL_SALES_ORDER_LINE_ITEMS_1_FROM_CONTACTS_TITLE',
  'save' => true,
  'id_name' => 'contacts_rl_sales_order_line_items_1contacts_ida',
  'link' => 'contacts_rl_sales_order_line_items_1',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["RL_Sales_Order_Line_Items"]["fields"]["contacts_rl_sales_order_line_items_1contacts_ida"] = array (
  'name' => 'contacts_rl_sales_order_line_items_1contacts_ida',
  'type' => 'link',
  'relationship' => 'contacts_rl_sales_order_line_items_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_RL_SALES_ORDER_LINE_ITEMS_1_FROM_RL_SALES_ORDER_LINE_ITEMS_TITLE',
);
