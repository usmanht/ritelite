<?php
// created: 2015-05-28 17:21:59
$dictionary["RL_Sales_Order_Line_Items"]["fields"]["rl_sales_order_line_items_aos_products_1"] = array (
  'name' => 'rl_sales_order_line_items_aos_products_1',
  'type' => 'link',
  'relationship' => 'rl_sales_order_line_items_aos_products_1',
  'source' => 'non-db',
  'module' => 'AOS_Products',
  'bean_name' => 'AOS_Products',
  'side' => 'right',
  'vname' => 'LBL_RL_SALES_ORDER_LINE_ITEMS_AOS_PRODUCTS_1_FROM_AOS_PRODUCTS_TITLE',
);
