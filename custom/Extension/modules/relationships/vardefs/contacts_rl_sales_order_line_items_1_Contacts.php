<?php
// created: 2014-05-07 16:21:34
$dictionary["Contact"]["fields"]["contacts_rl_sales_order_line_items_1"] = array (
  'name' => 'contacts_rl_sales_order_line_items_1',
  'type' => 'link',
  'relationship' => 'contacts_rl_sales_order_line_items_1',
  'source' => 'non-db',
  'module' => 'RL_Sales_Order_Line_Items',
  'bean_name' => 'RL_Sales_Order_Line_Items',
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_RL_SALES_ORDER_LINE_ITEMS_1_FROM_RL_SALES_ORDER_LINE_ITEMS_TITLE',
);
