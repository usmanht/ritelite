<?php
// created: 2015-02-16 16:56:38
$dictionary["Timesheet"]["fields"]["timesheet_securitygroups_1"] = array (
  'name' => 'timesheet_securitygroups_1',
  'type' => 'link',
  'relationship' => 'timesheet_securitygroups_1',
  'source' => 'non-db',
  'module' => 'SecurityGroups',
  'bean_name' => 'SecurityGroup',
  'vname' => 'LBL_TIMESHEET_SECURITYGROUPS_1_FROM_SECURITYGROUPS_TITLE',
);
