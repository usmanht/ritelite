<?php
 // created: 2015-01-12 15:28:24
$layout_defs["SecurityGroups"]["subpanel_setup"]['aos_invoices_securitygroups_1'] = array (
  'order' => 100,
  'module' => 'AOS_Invoices',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_AOS_INVOICES_SECURITYGROUPS_1_FROM_AOS_INVOICES_TITLE',
  'get_subpanel_data' => 'aos_invoices_securitygroups_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
