<?php
 // created: 2017-10-05 08:42:03
$layout_defs["Cases"]["subpanel_setup"]['cases_aos_quotes_2'] = array (
  'order' => 100,
  'module' => 'AOS_Quotes',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CASES_AOS_QUOTES_2_FROM_AOS_QUOTES_TITLE',
  'get_subpanel_data' => 'cases_aos_quotes_2',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
