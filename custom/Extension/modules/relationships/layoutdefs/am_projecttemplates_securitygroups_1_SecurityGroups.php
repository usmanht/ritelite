<?php
 // created: 2016-04-25 17:24:12
$layout_defs["SecurityGroups"]["subpanel_setup"]['am_projecttemplates_securitygroups_1'] = array (
  'order' => 100,
  'module' => 'AM_ProjectTemplates',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_AM_PROJECTTEMPLATES_SECURITYGROUPS_1_FROM_AM_PROJECTTEMPLATES_TITLE',
  'get_subpanel_data' => 'am_projecttemplates_securitygroups_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
