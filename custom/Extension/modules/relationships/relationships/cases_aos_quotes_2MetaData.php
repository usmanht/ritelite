<?php
// created: 2017-10-05 08:42:03
$dictionary["cases_aos_quotes_2"] = array (
  'true_relationship_type' => 'one-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'cases_aos_quotes_2' => 
    array (
      'lhs_module' => 'Cases',
      'lhs_table' => 'cases',
      'lhs_key' => 'id',
      'rhs_module' => 'AOS_Quotes',
      'rhs_table' => 'aos_quotes',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'cases_aos_quotes_2_c',
      'join_key_lhs' => 'cases_aos_quotes_2cases_ida',
      'join_key_rhs' => 'cases_aos_quotes_2aos_quotes_idb',
    ),
  ),
  'table' => 'cases_aos_quotes_2_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'cases_aos_quotes_2cases_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'cases_aos_quotes_2aos_quotes_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'cases_aos_quotes_2spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'cases_aos_quotes_2_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'cases_aos_quotes_2cases_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'cases_aos_quotes_2_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'cases_aos_quotes_2aos_quotes_idb',
      ),
    ),
  ),
);