<?php
// created: 2018-01-11 10:37:23
$dictionary["AOS_Product_Categories"]["fields"]["aos_product_categories_bugs_1"] = array (
  'name' => 'aos_product_categories_bugs_1',
  'type' => 'link',
  'relationship' => 'aos_product_categories_bugs_1',
  'source' => 'non-db',
  'module' => 'Bugs',
  'bean_name' => 'Bug',
  'side' => 'right',
  'vname' => 'LBL_AOS_PRODUCT_CATEGORIES_BUGS_1_FROM_BUGS_TITLE',
);
