<?php
 // created: 2014-05-07 16:11:42
$layout_defs["Accounts"]["subpanel_setup"]['accounts_rl_sales_order_line_items_2'] = array (
  'order' => 100,
  'module' => 'RL_Sales_Order_Line_Items',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_RL_SALES_ORDER_LINE_ITEMS_2_FROM_RL_SALES_ORDER_LINE_ITEMS_TITLE',
  'get_subpanel_data' => 'accounts_rl_sales_order_line_items_2',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
