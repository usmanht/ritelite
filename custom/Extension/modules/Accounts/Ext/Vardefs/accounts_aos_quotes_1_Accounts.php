<?php
// created: 2014-03-25 16:57:28
$dictionary["Account"]["fields"]["accounts_aos_quotes_1"] = array (
  'name' => 'accounts_aos_quotes_1',
  'type' => 'link',
  'relationship' => 'accounts_aos_quotes_1',
  'source' => 'non-db',
  'module' => 'AOS_Quotes',
  'bean_name' => 'AOS_Quotes',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_AOS_QUOTES_1_FROM_AOS_QUOTES_TITLE',
);
