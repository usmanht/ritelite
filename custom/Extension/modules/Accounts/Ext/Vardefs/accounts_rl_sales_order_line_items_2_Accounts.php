<?php
// created: 2014-05-07 16:11:42
$dictionary["Account"]["fields"]["accounts_rl_sales_order_line_items_2"] = array (
  'name' => 'accounts_rl_sales_order_line_items_2',
  'type' => 'link',
  'relationship' => 'accounts_rl_sales_order_line_items_2',
  'source' => 'non-db',
  'module' => 'RL_Sales_Order_Line_Items',
  'bean_name' => 'RL_Sales_Order_Line_Items',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_RL_SALES_ORDER_LINE_ITEMS_2_FROM_RL_SALES_ORDER_LINE_ITEMS_TITLE',
);
