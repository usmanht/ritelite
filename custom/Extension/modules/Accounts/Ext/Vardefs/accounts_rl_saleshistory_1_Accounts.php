<?php
// created: 2014-01-07 17:22:27
$dictionary["Account"]["fields"]["accounts_rl_saleshistory_1"] = array (
  'name' => 'accounts_rl_saleshistory_1',
  'type' => 'link',
  'relationship' => 'accounts_rl_saleshistory_1',
  'source' => 'non-db',
  'module' => 'RL_SalesHistory',
  'bean_name' => 'RL_SalesHistory',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_RL_SALESHISTORY_1_FROM_RL_SALESHISTORY_TITLE',
);
