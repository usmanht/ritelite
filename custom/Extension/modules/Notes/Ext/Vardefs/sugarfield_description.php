<?php
 // created: 2018-03-26 17:39:41
$dictionary['Note']['fields']['description']['inline_edit']=true;
$dictionary['Note']['fields']['description']['comments']='Full text of the note';
$dictionary['Note']['fields']['description']['merge_filter']='disabled';
$dictionary['Note']['fields']['description']['rows']='100';
$dictionary['Note']['fields']['description']['cols']='150';
$dictionary['Note']['fields']['description']['audited']=true;

 ?>