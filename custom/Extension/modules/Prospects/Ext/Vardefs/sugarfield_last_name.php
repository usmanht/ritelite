<?php
 // created: 2014-03-28 12:36:11
$dictionary['Prospect']['fields']['last_name']['required']=false;
$dictionary['Prospect']['fields']['last_name']['comments']='Last name of the contact';
$dictionary['Prospect']['fields']['last_name']['importable']='true';
$dictionary['Prospect']['fields']['last_name']['merge_filter']='disabled';
$dictionary['Prospect']['fields']['last_name']['unified_search']=false;

 ?>