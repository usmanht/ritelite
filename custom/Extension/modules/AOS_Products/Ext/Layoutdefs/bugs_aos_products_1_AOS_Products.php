<?php
 // created: 2014-10-22 16:58:44
$layout_defs["AOS_Products"]["subpanel_setup"]['bugs_aos_products_1'] = array (
  'order' => 100,
  'module' => 'Bugs',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_BUGS_AOS_PRODUCTS_1_FROM_BUGS_TITLE',
  'get_subpanel_data' => 'bugs_aos_products_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
