<?php
$popupMeta = array (
    'moduleMain' => 'AOS_Invoices',
    'varName' => 'AOS_Invoices',
    'orderBy' => 'aos_invoices.name',
    'whereClauses' => array (
  'name' => 'aos_invoices.name',
  'number' => 'aos_invoices.number',
  'sage_sop_number_c' => 'aos_invoices_cstm.sage_sop_number_c',
  'billing_account' => 'aos_invoices.billing_account',
  'billing_contact' => 'aos_invoices.billing_contact',
  'accounts_aos_invoices_1_name' => 'aos_invoices.accounts_aos_invoices_1_name',
  'contacts_aos_invoices_1_name' => 'aos_invoices.contacts_aos_invoices_1_name',
  'total_amount' => 'aos_invoices.total_amount',
  'date_entered' => 'aos_invoices.date_entered',
  'quote_date' => 'aos_invoices.quote_date',
  'due_date' => 'aos_invoices.due_date',
  'status' => 'aos_invoices.status',
  'invoice_no_c' => 'aos_invoices_cstm.invoice_no_c',
  'invoice_date' => 'aos_invoices.invoice_date',
  'quote_number' => 'aos_invoices.quote_number',
  'assigned_user_name' => 'aos_invoices.assigned_user_name',
  'created_by_name' => 'aos_invoices.created_by_name',
),
    'searchInputs' => array (
  0 => 'name',
  4 => 'number',
  5 => 'sage_sop_number_c',
  6 => 'billing_account',
  7 => 'billing_contact',
  8 => 'accounts_aos_invoices_1_name',
  9 => 'contacts_aos_invoices_1_name',
  10 => 'total_amount',
  11 => 'date_entered',
  12 => 'quote_date',
  13 => 'due_date',
  14 => 'status',
  15 => 'invoice_no_c',
  16 => 'invoice_date',
  17 => 'quote_number',
  18 => 'assigned_user_name',
  19 => 'created_by_name',
),
    'searchdefs' => array (
  'number' => 
  array (
    'name' => 'number',
    'width' => '10%',
  ),
  'name' => 
  array (
    'name' => 'name',
    'width' => '10%',
  ),
  'sage_sop_number_c' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_SAGE_SOP_NUMBER',
    'width' => '10%',
    'name' => 'sage_sop_number_c',
  ),
  'billing_account' => 
  array (
    'name' => 'billing_account',
    'width' => '10%',
  ),
  'billing_contact' => 
  array (
    'name' => 'billing_contact',
    'width' => '10%',
  ),
  'accounts_aos_invoices_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_ACCOUNTS_AOS_INVOICES_1_FROM_ACCOUNTS_TITLE',
    'id' => 'ACCOUNTS_AOS_INVOICES_1ACCOUNTS_IDA',
    'width' => '10%',
    'name' => 'accounts_aos_invoices_1_name',
  ),
  'contacts_aos_invoices_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CONTACTS_AOS_INVOICES_1_FROM_CONTACTS_TITLE',
    'id' => 'CONTACTS_AOS_INVOICES_1CONTACTS_IDA',
    'width' => '10%',
    'name' => 'contacts_aos_invoices_1_name',
  ),
  'total_amount' => 
  array (
    'name' => 'total_amount',
    'width' => '10%',
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'name' => 'date_entered',
  ),
  'quote_date' => 
  array (
    'type' => 'date',
    'label' => 'LBL_QUOTE_DATE',
    'width' => '10%',
    'name' => 'quote_date',
  ),
  'due_date' => 
  array (
    'name' => 'due_date',
    'width' => '10%',
  ),
  'status' => 
  array (
    'name' => 'status',
    'width' => '10%',
  ),
  'invoice_no_c' => 
  array (
    'type' => 'int',
    'label' => 'LBL_INVOICE_NO',
    'width' => '10%',
    'name' => 'invoice_no_c',
  ),
  'invoice_date' => 
  array (
    'type' => 'date',
    'label' => 'LBL_INVOICE_DATE',
    'width' => '10%',
    'name' => 'invoice_date',
  ),
  'quote_number' => 
  array (
    'type' => 'int',
    'label' => 'LBL_QUOTE_NUMBER',
    'width' => '10%',
    'name' => 'quote_number',
  ),
  'created_by_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => '10%',
    'name' => 'created_by_name',
  ),
  'assigned_user_name' => 
  array (
    'link' => true,
    'type' => 'relate',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'id' => 'ASSIGNED_USER_ID',
    'width' => '10%',
    'name' => 'assigned_user_name',
  ),
),
);
