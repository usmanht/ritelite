<?php
$dashletData['AOS_InvoicesDashlet']['searchFields'] = array (
  'number' => 
  array (
    'default' => '',
  ),
  'customer_order_number_c' => 
  array (
    'default' => '',
  ),
  'sage_sop_number_c' => 
  array (
    'default' => '',
  ),
  'invoice_no_c' => 
  array (
    'default' => '',
  ),
  'billing account' => 
  array (
    'default' => '',
  ),
  'accounts_aos_invoices_1_name' => 
  array (
    'default' => '',
  ),
  'created_by_name' => 
  array (
    'default' => '',
  ),
  'assigned_user_name' => 
  array (
    'default' => '',
  ),
  'sales_order_status_c' => 
  array (
    'default' => '',
  ),
);
$dashletData['AOS_InvoicesDashlet']['columns'] = array (
  'number' => 
  array (
    'width' => '5%',
    'label' => 'LBL_LIST_NUM',
    'default' => true,
    'name' => 'number',
  ),
  'sage_sop_number_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_SAGE_SOP_NUMBER',
    'width' => '10%',
    'name' => 'sage_sop_number_c',
  ),
  'customer_order_number_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_CUSTOMER_ORDER_NUMBER',
    'width' => '10%',
    'name' => 'customer_order_number_c',
  ),
  'billing_account' => 
  array (
    'width' => '20%',
    'label' => 'LBL_BILLING_ACCOUNT',
    'name' => 'billing_account',
    'default' => true,
  ),
  'accounts_aos_invoices_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_ACCOUNTS_AOS_INVOICES_1_FROM_ACCOUNTS_TITLE',
    'id' => 'ACCOUNTS_AOS_INVOICES_1ACCOUNTS_IDA',
    'width' => '10%',
    'default' => true,
    'name' => 'accounts_aos_invoices_1_name',
  ),
  'total_amount' => 
  array (
    'width' => '15%',
    'label' => 'LBL_GRAND_TOTAL',
    'currency_format' => true,
    'default' => true,
    'name' => 'total_amount',
  ),
  'billing_contact' => 
  array (
    'width' => '15%',
    'label' => 'LBL_BILLING_CONTACT',
    'name' => 'billing_contact',
    'default' => false,
  ),
  'invoice_date' => 
  array (
    'width' => '15%',
    'label' => 'LBL_INVOICE_DATE',
    'name' => 'invoice_date',
    'default' => false,
  ),
  'date_entered' => 
  array (
    'width' => '15%',
    'label' => 'LBL_DATE_ENTERED',
    'name' => 'date_entered',
    'default' => false,
  ),
  'date_modified' => 
  array (
    'width' => '15%',
    'label' => 'LBL_DATE_MODIFIED',
    'name' => 'date_modified',
    'default' => false,
  ),
  'assigned_user_name' => 
  array (
    'width' => '8%',
    'label' => 'LBL_LIST_ASSIGNED_USER',
    'name' => 'assigned_user_name',
    'default' => false,
  ),
  'created_by' => 
  array (
    'width' => '8%',
    'label' => 'LBL_CREATED',
    'name' => 'created_by',
    'default' => false,
  ),
  'due_date' => 
  array (
    'width' => '15%',
    'label' => 'LBL_DUE_DATE',
    'default' => false,
    'name' => 'due_date',
  ),
  'sales_order_status_c' => 
  array (
    'type' => 'enum',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_SALES_ORDER_STATUS',
    'width' => '10%',
  ),
  'name' => 
  array (
    'width' => '20%',
    'label' => 'LBL_LIST_NAME',
    'link' => true,
    'default' => false,
    'name' => 'name',
  ),
  'status' => 
  array (
    'width' => '15%',
    'label' => 'LBL_STATUS',
    'default' => false,
    'name' => 'status',
  ),
);
