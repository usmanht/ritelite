/**
 *
 * @author SalesAgility (Andrew Mclaughlan) <support@salesagility.com>
 * Date: 30/07/14
 * Used for quick search on Sage account ref in quotes/invoices
 */

YAHOO.util.Event.onDOMReady(function(){ //Call after page has loaded.

    sqs_objects["EditView_sage_ref_billing"] = {
        "form": "EditView",
        "method": "query",
        "modules": ["Accounts"],
        "group": "or",
        "field_list": ["sage_account_ref_c","name","id","billing_address_street","billing_address_city","billing_address_state","billing_address_postalcode","billing_address_country"],
        "populate_list": ["sage_ref_billing", "billing_account","billing_account_id", "billing_address_street","billing_address_city","billing_address_state","billing_address_postalcode","billing_address_country"],
        "required_list": ["billing_account_id"],
        "conditions": [{
            "name": "sage_account_ref_c",
            "op": "like_custom",
            "end": "%",
            "value": ""
        }],
        "order": "sage_account_ref_c",
        "limit": "30",
        "no_match_text": "No Match"};

    sqs_objects["EditView_sage_ref_shipping"] = {
        "form": "EditView",
        "method": "query",
        "modules": ["Accounts"],
        "group": "or",
        "field_list": ["sage_account_ref_c","name","id","shipping_address_street","shipping_address_city","shipping_address_state","shipping_address_postalcode","shipping_address_country"],
        "populate_list": ["sage_ref_shipping", "accounts_aos_invoices_1_name","accounts_aos_invoices_1accounts_ida", "shipping_address_street","shipping_address_city","shipping_address_state","shipping_address_postalcode","shipping_address_country"],
        "required_list": ["billing_account_id"],
        "conditions": [{
            "name": "sage_account_ref_c",
            "op": "like_custom",
            "end": "%",
            "value": ""
        }],
        "order": "sage_account_ref_c",
        "limit": "30",
        "no_match_text": "No Match"};

    enableQS(true);

    //overright default javascript function
    document.getElementById("shipping_checkbox").setAttribute('onclick','get_shipping_address(this);');
    document.getElementById("btn_billing_account").setAttribute('onclick','open_popup( "Accounts",  600,  400,  "",  true,  false,  {"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"billing_account_id","name":"billing_account","billing_address_street":"billing_address_street","billing_address_city":"billing_address_city","billing_address_state":"billing_address_state","billing_address_postalcode":"billing_address_postalcode","billing_address_country":"billing_address_country","sage_account_ref_c":"sage_ref_billing"}},  "single",  true );');
    document.getElementById("btn_accounts_aos_invoices_1_name").setAttribute('onclick','open_popup( "Accounts",  600,  400,  "",  true,  false,  {"call_back_function":"set_return","form_name":"EditView","field_to_name_array":{"id":"accounts_aos_invoices_1accounts_ida","name":"accounts_aos_invoices_1_name","sage_account_ref_c":"sage_ref_shipping","shipping_address_street":"shipping_address_street","shipping_address_city":"shipping_address_city","shipping_address_state":"shipping_address_state","shipping_address_postalcode":"shipping_address_postalcode","shipping_address_country":"shipping_address_country","phone_office":"phone_work"}},  "single",  true );');


});

function get_shipping_address(ele){

    var id = $('#billing_account_id').val();
    var dataString = '&account_id=' + id;

    if(ele.checked){
        $.ajax({
            type: "POST",
            url: "index.php?module=Accounts&action=get_shipping_address",
            data: dataString,
            success: function(data) {
                var fields = data.split("|");
                $('#shipping_address_street').val(fields[0]);
                $('#shipping_address_city').val(fields[1]);
                $('#shipping_address_state').val(fields[2]);
                $('#shipping_address_postalcode').val(fields[3]);
                $('#shipping_address_country').val(fields[4]);
                $('#accounts_aos_invoices_1_name').val(fields[5]);
                $('#accounts_aos_invoices_1accounts_ida').val(fields[6]);
                $('#sage_ref_shipping').val(fields[7]);
                var cont = $('#billing_contact').val();
                var cont_id = $('#billing_account_id').val();
                $('#contacts_aos_invoices_1_name').val(cont);
                $('#contacts_aos_invoices_1contacts_ida').val(cont_id);
            }
        });
    }

}

$(function() {
    //poll the two fields for source changes, checks every 100ms and updates
    setInterval(function() {
        //get the shipping account value
        var inputOne = $('#accounts_aos_invoices_1_name');
        var previous = inputOne.val();
        //get the shipping contact value
        var inputTwo = $('#contacts_aos_invoices_1_name');
        var previous2 = inputTwo.val();

        return function() {
            var current = inputOne.val();
            var current2 = inputTwo.val();
            //update shipping company value if its different from previous
            if (current !== previous) {
                $("#shipping_company_c").val(current);
                previous = current;
            }
            //update shipping contact value if its different from previous
            if (current2 !== previous2) {
                $("#shipping_contact_c").val(current2);
                previous2 = current2;
            }
        }
    }(), 100);

});