<?php 
 //WARNING: The contents of this file are auto-generated


$dictionary["Document"]["fields"]["aos_contracts"] = array (
  'name' => 'aos_contracts',
  'type' => 'link',
  'relationship' => 'aos_contracts_documents',
  'source' => 'non-db',
  'module' => 'AOS_Contracts',
);


// created: 2014-11-12 17:42:29
$dictionary["Document"]["fields"]["documents_aos_invoices_1"] = array (
  'name' => 'documents_aos_invoices_1',
  'type' => 'link',
  'relationship' => 'documents_aos_invoices_1',
  'source' => 'non-db',
  'module' => 'AOS_Invoices',
  'bean_name' => 'AOS_Invoices',
  'vname' => 'LBL_DOCUMENTS_AOS_INVOICES_1_FROM_AOS_INVOICES_TITLE',
);


// created: 2014-04-11 12:48:55
$dictionary["Document"]["fields"]["documents_aos_products_1"] = array (
  'name' => 'documents_aos_products_1',
  'type' => 'link',
  'relationship' => 'documents_aos_products_1',
  'source' => 'non-db',
  'module' => 'AOS_Products',
  'bean_name' => 'AOS_Products',
  'vname' => 'LBL_DOCUMENTS_AOS_PRODUCTS_1_FROM_AOS_PRODUCTS_TITLE',
);


// created: 2014-06-03 10:30:00
$dictionary["Document"]["fields"]["documents_aos_product_categories_1"] = array (
  'name' => 'documents_aos_product_categories_1',
  'type' => 'link',
  'relationship' => 'documents_aos_product_categories_1',
  'source' => 'non-db',
  'module' => 'AOS_Product_Categories',
  'bean_name' => 'AOS_Product_Categories',
  'vname' => 'LBL_DOCUMENTS_AOS_PRODUCT_CATEGORIES_1_FROM_AOS_PRODUCT_CATEGORIES_TITLE',
);


// created: 2014-05-13 17:59:33
$dictionary["Document"]["fields"]["documents_aos_quotes_1"] = array (
  'name' => 'documents_aos_quotes_1',
  'type' => 'link',
  'relationship' => 'documents_aos_quotes_1',
  'source' => 'non-db',
  'module' => 'AOS_Quotes',
  'bean_name' => 'AOS_Quotes',
  'vname' => 'LBL_DOCUMENTS_AOS_QUOTES_1_FROM_AOS_QUOTES_TITLE',
);


// created: 2014-04-05 18:16:31
$dictionary["Document"]["fields"]["documents_project_1"] = array (
  'name' => 'documents_project_1',
  'type' => 'link',
  'relationship' => 'documents_project_1',
  'source' => 'non-db',
  'module' => 'Project',
  'bean_name' => 'Project',
  'vname' => 'LBL_DOCUMENTS_PROJECT_1_FROM_PROJECT_TITLE',
);



$dictionary['Document']['fields']['SecurityGroups'] = array (
  	'name' => 'SecurityGroups',
    'type' => 'link',
	'relationship' => 'securitygroups_documents',
	'module'=>'SecurityGroups',
	'bean_name'=>'SecurityGroup',
    'source'=>'non-db',
	'vname'=>'LBL_SECURITYGROUPS',
);






 // created: 2014-05-09 23:06:57
$dictionary['Document']['fields']['category_id']['required']=true;
$dictionary['Document']['fields']['category_id']['massupdate']='1';
$dictionary['Document']['fields']['category_id']['merge_filter']='disabled';

 

 // created: 2014-05-09 23:07:08
$dictionary['Document']['fields']['subcategory_id']['massupdate']='1';
$dictionary['Document']['fields']['subcategory_id']['merge_filter']='disabled';

 

 // created: 2014-05-09 23:05:53
$dictionary['Document']['fields']['template_type']['merge_filter']='disabled';
$dictionary['Document']['fields']['template_type']['reportable']=true;

 
?>