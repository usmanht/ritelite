<?php
// created: 2018-01-10 16:50:13
$mod_strings = array (
  'LBL_SECURITYGROUPS_SUBPANEL_TITLE' => 'Security Groups',
  'LBL_TEMPLATE_TYPE' => 'Document Template Type',
  'LBL_SF_CATEGORY' => 'Category:',
  'LBL_SF_SUBCATEGORY' => 'Sub Category:',
  'LBL_ASSIGNED_TO_NAME' => 'Owner',
);