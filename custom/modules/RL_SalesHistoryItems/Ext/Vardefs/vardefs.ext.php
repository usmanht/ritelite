<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2014-01-07 15:54:30
$dictionary["RL_SalesHistoryItems"]["fields"]["rl_saleshistory_rl_saleshistoryitems_1"] = array (
  'name' => 'rl_saleshistory_rl_saleshistoryitems_1',
  'type' => 'link',
  'relationship' => 'rl_saleshistory_rl_saleshistoryitems_1',
  'source' => 'non-db',
  'module' => 'RL_SalesHistory',
  'bean_name' => 'RL_SalesHistory',
  'vname' => 'LBL_RL_SALESHISTORY_RL_SALESHISTORYITEMS_1_FROM_RL_SALESHISTORY_TITLE',
  'id_name' => 'rl_saleshistory_rl_saleshistoryitems_1rl_saleshistory_ida',
);
$dictionary["RL_SalesHistoryItems"]["fields"]["rl_saleshistory_rl_saleshistoryitems_1_name"] = array (
  'name' => 'rl_saleshistory_rl_saleshistoryitems_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_RL_SALESHISTORY_RL_SALESHISTORYITEMS_1_FROM_RL_SALESHISTORY_TITLE',
  'save' => true,
  'id_name' => 'rl_saleshistory_rl_saleshistoryitems_1rl_saleshistory_ida',
  'link' => 'rl_saleshistory_rl_saleshistoryitems_1',
  'table' => 'rl_saleshistory',
  'module' => 'RL_SalesHistory',
  'rname' => 'name',
);
$dictionary["RL_SalesHistoryItems"]["fields"]["rl_saleshistory_rl_saleshistoryitems_1rl_saleshistory_ida"] = array (
  'name' => 'rl_saleshistory_rl_saleshistoryitems_1rl_saleshistory_ida',
  'type' => 'link',
  'relationship' => 'rl_saleshistory_rl_saleshistoryitems_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_RL_SALESHISTORY_RL_SALESHISTORYITEMS_1_FROM_RL_SALESHISTORYITEMS_TITLE',
);


 // created: 2014-04-21 13:48:52
$dictionary['RL_SalesHistoryItems']['fields']['name']['duplicate_merge']='disabled';
$dictionary['RL_SalesHistoryItems']['fields']['name']['duplicate_merge_dom_value']='0';
$dictionary['RL_SalesHistoryItems']['fields']['name']['merge_filter']='disabled';
$dictionary['RL_SalesHistoryItems']['fields']['name']['unified_search']=false;

 

 // created: 2015-04-15 17:34:32
$dictionary['RL_SalesHistoryItems']['fields']['polink_c']['labelValue']='PO Link Number';

 
?>