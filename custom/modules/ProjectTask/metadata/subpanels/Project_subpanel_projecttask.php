<?php
// created: 2015-03-17 15:38:55
$subpanel_layout['list_fields'] = array (
  'task_number' => 
  array (
    'type' => 'int',
    'vname' => 'LBL_TASK_NUMBER',
    'width' => '10%',
    'default' => true,
  ),
  'project_task_id' => 
  array (
    'type' => 'int',
    'vname' => 'LBL_PROJECT_TASK_ID',
    'width' => '10%',
    'default' => true,
  ),
  'name' => 
  array (
    'vname' => 'LBL_LIST_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '30%',
    'default' => true,
  ),
  'date_start' => 
  array (
    'vname' => 'LBL_DATE_START',
    'width' => '10%',
    'default' => true,
  ),
  'status' => 
  array (
    'type' => 'enum',
    'vname' => 'LBL_STATUS',
    'width' => '10%',
    'default' => true,
  ),
  'date_due' => 
  array (
    'type' => 'date',
    'vname' => 'LBL_DATE_DUE',
    'width' => '10%',
    'default' => true,
  ),
  'date_finish' => 
  array (
    'vname' => 'LBL_DATE_FINISH',
    'width' => '10%',
    'default' => true,
  ),
  'project_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_PARENT_NAME',
    'id' => 'PROJECT_ID',
    'width' => '10%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Project',
    'target_record_key' => 'project_id',
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'vname' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
  ),
  'assigned_user_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_ASSIGNED_USER_NAME',
    'id' => 'ASSIGNED_USER_ID',
    'width' => '15%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Users',
    'target_record_key' => 'assigned_user_id',
  ),
);