<?php 
 //WARNING: The contents of this file are auto-generated


$dictionary["Contact"]["fields"]["aos_quotes"] = array (
  'name' => 'aos_quotes',
    'type' => 'link',
    'relationship' => 'contact_aos_quotes',
    'module'=>'AOS_Quotes',
    'bean_name'=>'AOS_Quotes',
    'source'=>'non-db',
);
$dictionary["Contact"]["relationships"]["contact_aos_quotes"] = array (
	'lhs_module'=> 'Contacts', 
	'lhs_table'=> 'contacts', 
	'lhs_key' => 'id',
	'rhs_module'=> 'AOS_Quotes', 
	'rhs_table'=> 'aos_quotes', 
	'rhs_key' => 'billing_contact_id',
	'relationship_type'=>'one-to-many',
);

$dictionary["Contact"]["fields"]["aos_invoices"] = array (
  'name' => 'aos_invoices',
    'type' => 'link',
    'relationship' => 'contact_aos_invoices',
    'module'=>'AOS_Invoices',
    'bean_name'=>'AOS_Invoices',
    'source'=>'non-db',
);
$dictionary["Contact"]["relationships"]["contact_aos_invoices"] = array (
	'lhs_module'=> 'Contacts', 
	'lhs_table'=> 'contacts', 
	'lhs_key' => 'id',
	'rhs_module'=> 'AOS_Invoices', 
	'rhs_table'=> 'aos_invoices', 
	'rhs_key' => 'billing_contact_id',
	'relationship_type'=>'one-to-many',
);

$dictionary["Contact"]["fields"]["aos_contracts"] = array (
  'name' => 'aos_contracts',
    'type' => 'link',
    'relationship' => 'contact_aos_contracts',
    'module'=>'AOS_Contracts',
    'bean_name'=>'AOS_Contracts',
    'source'=>'non-db',
);
$dictionary["Contact"]["relationships"]["contact_aos_contracts"] = array (
	'lhs_module'=> 'Contacts', 
	'lhs_table'=> 'contacts', 
	'lhs_key' => 'id',
	'rhs_module'=> 'AOS_Contracts', 
	'rhs_table'=> 'aos_contracts', 
	'rhs_key' => 'contact_id',
	'relationship_type'=>'one-to-many',
);


// created: 2014-05-07 16:27:46
$dictionary["Contact"]["fields"]["contacts_aos_invoices_1"] = array (
  'name' => 'contacts_aos_invoices_1',
  'type' => 'link',
  'relationship' => 'contacts_aos_invoices_1',
  'source' => 'non-db',
  'module' => 'AOS_Invoices',
  'bean_name' => 'AOS_Invoices',
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_AOS_INVOICES_1_FROM_AOS_INVOICES_TITLE',
);


// created: 2014-03-25 17:07:24
$dictionary["Contact"]["fields"]["contacts_aos_quotes_1"] = array (
  'name' => 'contacts_aos_quotes_1',
  'type' => 'link',
  'relationship' => 'contacts_aos_quotes_1',
  'source' => 'non-db',
  'module' => 'AOS_Quotes',
  'bean_name' => 'AOS_Quotes',
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_AOS_QUOTES_1_FROM_AOS_QUOTES_TITLE',
);


// created: 2014-05-07 16:21:34
$dictionary["Contact"]["fields"]["contacts_rl_sales_order_line_items_1"] = array (
  'name' => 'contacts_rl_sales_order_line_items_1',
  'type' => 'link',
  'relationship' => 'contacts_rl_sales_order_line_items_1',
  'source' => 'non-db',
  'module' => 'RL_Sales_Order_Line_Items',
  'bean_name' => 'RL_Sales_Order_Line_Items',
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_RL_SALES_ORDER_LINE_ITEMS_1_FROM_RL_SALES_ORDER_LINE_ITEMS_TITLE',
);


// created: 2014-05-07 16:23:44
$dictionary["Contact"]["fields"]["contacts_rl_sales_order_line_items_2"] = array (
  'name' => 'contacts_rl_sales_order_line_items_2',
  'type' => 'link',
  'relationship' => 'contacts_rl_sales_order_line_items_2',
  'source' => 'non-db',
  'module' => 'RL_Sales_Order_Line_Items',
  'bean_name' => 'RL_Sales_Order_Line_Items',
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_RL_SALES_ORDER_LINE_ITEMS_2_FROM_RL_SALES_ORDER_LINE_ITEMS_TITLE',
);



$dictionary['Contact']['fields']['SecurityGroups'] = array (
  	'name' => 'SecurityGroups',
    'type' => 'link',
	'relationship' => 'securitygroups_contacts',
	'module'=>'SecurityGroups',
	'bean_name'=>'SecurityGroup',
    'source'=>'non-db',
	'vname'=>'LBL_SECURITYGROUPS',
);






 // created: 2014-12-19 16:18:51
$dictionary['Contact']['fields']['accesscontactid_c']['labelValue']='Access Contact ID';
$dictionary['Contact']['fields']['accesscontactid_c']['unified_search']='1';

 

 // created: 2015-03-03 11:44:38
$dictionary['Contact']['fields']['alt_address_city']['comments']='City for alternate address';
$dictionary['Contact']['fields']['alt_address_city']['merge_filter']='disabled';
$dictionary['Contact']['fields']['alt_address_city']['unified_search']=true;

 

 // created: 2015-04-07 17:04:30
$dictionary['Contact']['fields']['alt_address_country']['comments']='Country for alternate address';
$dictionary['Contact']['fields']['alt_address_country']['merge_filter']='disabled';
$dictionary['Contact']['fields']['alt_address_country']['unified_search']=false;

 

 // created: 2015-03-03 11:44:46
$dictionary['Contact']['fields']['alt_address_postalcode']['comments']='Postal code for alternate address';
$dictionary['Contact']['fields']['alt_address_postalcode']['merge_filter']='disabled';
$dictionary['Contact']['fields']['alt_address_postalcode']['unified_search']=true;

 

 // created: 2015-03-03 11:44:50
$dictionary['Contact']['fields']['alt_address_state']['comments']='State for alternate address';
$dictionary['Contact']['fields']['alt_address_state']['merge_filter']='disabled';
$dictionary['Contact']['fields']['alt_address_state']['unified_search']=true;

 

 // created: 2015-03-03 11:44:54
$dictionary['Contact']['fields']['alt_address_street']['comments']='Street address for alternate address';
$dictionary['Contact']['fields']['alt_address_street']['merge_filter']='disabled';
$dictionary['Contact']['fields']['alt_address_street']['unified_search']=true;

 

 // created: 2015-03-06 15:00:04
$dictionary['Contact']['fields']['companyname_c']['labelValue']='Company Name';
$dictionary['Contact']['fields']['companyname_c']['unified_search']='1';

 

 // created: 2014-01-07 11:46:24
$dictionary['Contact']['fields']['contact_status_c']['labelValue']='Status';

 

 // created: 2017-02-24 14:35:30
$dictionary['Contact']['fields']['dealerwebpage_password_c']['inline_edit']='1';
$dictionary['Contact']['fields']['dealerwebpage_password_c']['labelValue']='Dealer Webpage Password';
$dictionary['Contact']['fields']['dealerwebpage_password_c']['unified_search']='1';

 

 // created: 2017-02-24 14:34:16
$dictionary['Contact']['fields']['dealerwebpage_username_c']['inline_edit']='1';
$dictionary['Contact']['fields']['dealerwebpage_username_c']['labelValue']='Dealer Webpage Username';
$dictionary['Contact']['fields']['dealerwebpage_username_c']['unified_search']='1';

 

 // created: 2018-05-09 17:41:21
$dictionary['Contact']['fields']['gdpr_classification_c']['inline_edit']='1';
$dictionary['Contact']['fields']['gdpr_classification_c']['labelValue']='GDPR Classification';

 

 // created: 2014-03-21 17:30:23
$dictionary['Contact']['fields']['group_c']['labelValue']='Group';

 

 // created: 2016-03-04 15:12:49

 

 // created: 2016-03-04 15:12:46

 

 // created: 2016-03-04 15:12:45

 

 // created: 2016-03-04 15:12:42

 

 // created: 2014-03-28 12:17:16
$dictionary['Contact']['fields']['last_name']['required']=false;
$dictionary['Contact']['fields']['last_name']['comments']='Last name of the contact';
$dictionary['Contact']['fields']['last_name']['merge_filter']='disabled';
$dictionary['Contact']['fields']['last_name']['importable']='true';

 

 // created: 2014-07-23 08:59:54
$dictionary['Contact']['fields']['notes_c']['labelValue']='Notes';

 

 // created: 2018-05-23 18:02:56
$dictionary['Contact']['fields']['opt_in_c']['inline_edit']='1';
$dictionary['Contact']['fields']['opt_in_c']['labelValue']='Opt-in';

 

 // created: 2015-03-03 11:44:09
$dictionary['Contact']['fields']['primary_address_city']['comments']='City for primary address';
$dictionary['Contact']['fields']['primary_address_city']['merge_filter']='disabled';
$dictionary['Contact']['fields']['primary_address_city']['unified_search']=true;

 

 // created: 2015-03-03 11:44:16
$dictionary['Contact']['fields']['primary_address_country']['comments']='Country for primary address';
$dictionary['Contact']['fields']['primary_address_country']['merge_filter']='disabled';
$dictionary['Contact']['fields']['primary_address_country']['unified_search']=true;

 

 // created: 2015-03-03 11:44:20
$dictionary['Contact']['fields']['primary_address_postalcode']['comments']='Postal code for primary address';
$dictionary['Contact']['fields']['primary_address_postalcode']['merge_filter']='disabled';
$dictionary['Contact']['fields']['primary_address_postalcode']['unified_search']=true;

 

 // created: 2015-03-03 11:44:24
$dictionary['Contact']['fields']['primary_address_state']['comments']='State for primary address';
$dictionary['Contact']['fields']['primary_address_state']['merge_filter']='disabled';
$dictionary['Contact']['fields']['primary_address_state']['unified_search']=true;

 

 // created: 2015-03-03 11:44:30
$dictionary['Contact']['fields']['primary_address_street']['comments']='Street address for primary address';
$dictionary['Contact']['fields']['primary_address_street']['merge_filter']='disabled';
$dictionary['Contact']['fields']['primary_address_street']['unified_search']=true;

 

 // created: 2014-12-19 16:19:01
$dictionary['Contact']['fields']['searchlabel_c']['labelValue']='Label';
$dictionary['Contact']['fields']['searchlabel_c']['unified_search']='1';

 

 // created: 2014-12-18 14:48:17
$dictionary['Contact']['fields']['testfield_c']['labelValue']='testfield';
$dictionary['Contact']['fields']['testfield_c']['unified_search']='1';

 

 // created: 2018-05-23 18:18:46
$dictionary['Contact']['fields']['workflow_trigge_date_time_c']['inline_edit']='1';
$dictionary['Contact']['fields']['workflow_trigge_date_time_c']['labelValue']='workflow trigger Date/Time';

 
?>