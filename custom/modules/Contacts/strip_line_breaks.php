<?php
/**
 * strip_line_breaks.php
 * @author SalesAgility (Andrew Mclaughlan) <support@salesagility.com>
 * Date: 16/04/15
 * Comments
 */

class strip_line_breaks
{
    function strip($bean, $event, $arguments)
    {
        $bean->primary_address_street = str_replace(array("\r", "\n"), "", $bean->primary_address_street);
        $bean->alt_address_street = str_replace(array("\r", "\n"), "", $bean->alt_address_street);
    }
}