<?php
// created: 2016-03-04 15:10:16
$viewdefs['SecurityGroups']['DetailView'] = array (
  'templateMeta' => 
  array (
    'form' => 
    array (
      'buttons' => 
      array (
        0 => 'EDIT',
        1 => 'DUPLICATE',
        2 => 'DELETE',
      ),
    ),
    'maxColumns' => '2',
    'widths' => 
    array (
      0 => 
      array (
        'label' => '10',
        'field' => '30',
      ),
      1 => 
      array (
        'label' => '10',
        'field' => '30',
      ),
    ),
    'useTabs' => false,
    'tabDefs' => 
    array (
      'DEFAULT' => 
      array (
        'newTab' => false,
        'panelDefault' => 'expanded',
      ),
    ),
    'syncDetailEditViews' => true,
  ),
  'panels' => 
  array (
    'default' => 
    array (
      0 => 
      array (
        0 => 'name',
        1 => 
        array (
          'name' => 'department_number_c',
          'label' => 'LBL_DEPARTMENT_NUMBER',
        ),
      ),
      1 => 
      array (
        0 => 'noninheritable',
        1 => 'assigned_user_name',
      ),
      2 => 
      array (
        0 => 'description',
      ),
    ),
  ),
);