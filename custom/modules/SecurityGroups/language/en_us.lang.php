<?php
// created: 2013-12-10 12:07:28
$mod_strings = array (
  'LNK_NEW_RECORD' => 'Create a Security Group',
  'LNK_LIST' => 'List View',
  'LBL_LIST_FORM_TITLE' => 'Security Groups',
  'LBL_SEARCH_FORM_TITLE' => 'Search Security Group',
  'LBL_DEPARTMENT_NUMBER' => 'Department No',
);