<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2016-04-25 17:24:12
$dictionary["SecurityGroup"]["fields"]["am_projecttemplates_securitygroups_1"] = array (
  'name' => 'am_projecttemplates_securitygroups_1',
  'type' => 'link',
  'relationship' => 'am_projecttemplates_securitygroups_1',
  'source' => 'non-db',
  'module' => 'AM_ProjectTemplates',
  'bean_name' => 'AM_ProjectTemplates',
  'vname' => 'LBL_AM_PROJECTTEMPLATES_SECURITYGROUPS_1_FROM_AM_PROJECTTEMPLATES_TITLE',
);


// created: 2015-01-12 15:28:24
$dictionary["SecurityGroup"]["fields"]["aos_invoices_securitygroups_1"] = array (
  'name' => 'aos_invoices_securitygroups_1',
  'type' => 'link',
  'relationship' => 'aos_invoices_securitygroups_1',
  'source' => 'non-db',
  'module' => 'AOS_Invoices',
  'bean_name' => 'AOS_Invoices',
  'vname' => 'LBL_AOS_INVOICES_SECURITYGROUPS_1_FROM_AOS_INVOICES_TITLE',
);


// created: 2014-05-02 12:12:37
$dictionary["SecurityGroup"]["fields"]["securitygroups_aos_quotes_1"] = array (
  'name' => 'securitygroups_aos_quotes_1',
  'type' => 'link',
  'relationship' => 'securitygroups_aos_quotes_1',
  'source' => 'non-db',
  'module' => 'AOS_Quotes',
  'bean_name' => 'AOS_Quotes',
  'vname' => 'LBL_SECURITYGROUPS_AOS_QUOTES_1_FROM_AOS_QUOTES_TITLE',
);


 // created: 2014-03-21 17:02:18
$dictionary['SecurityGroup']['fields']['department_number_c']['labelValue']='Department No';

 

// created: 2015-02-16 16:56:38
$dictionary["SecurityGroup"]["fields"]["timesheet_securitygroups_1"] = array (
  'name' => 'timesheet_securitygroups_1',
  'type' => 'link',
  'relationship' => 'timesheet_securitygroups_1',
  'source' => 'non-db',
  'module' => 'Timesheet',
  'bean_name' => 'Timesheet',
  'vname' => 'LBL_TIMESHEET_SECURITYGROUPS_1_FROM_TIMESHEET_TITLE',
);

?>