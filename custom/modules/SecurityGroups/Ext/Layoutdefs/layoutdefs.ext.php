<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2016-04-25 17:24:12
$layout_defs["SecurityGroups"]["subpanel_setup"]['am_projecttemplates_securitygroups_1'] = array (
  'order' => 100,
  'module' => 'AM_ProjectTemplates',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_AM_PROJECTTEMPLATES_SECURITYGROUPS_1_FROM_AM_PROJECTTEMPLATES_TITLE',
  'get_subpanel_data' => 'am_projecttemplates_securitygroups_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2015-01-12 15:28:24
$layout_defs["SecurityGroups"]["subpanel_setup"]['aos_invoices_securitygroups_1'] = array (
  'order' => 100,
  'module' => 'AOS_Invoices',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_AOS_INVOICES_SECURITYGROUPS_1_FROM_AOS_INVOICES_TITLE',
  'get_subpanel_data' => 'aos_invoices_securitygroups_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2014-05-02 12:12:37
$layout_defs["SecurityGroups"]["subpanel_setup"]['securitygroups_aos_quotes_1'] = array (
  'order' => 100,
  'module' => 'AOS_Quotes',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_SECURITYGROUPS_AOS_QUOTES_1_FROM_AOS_QUOTES_TITLE',
  'get_subpanel_data' => 'securitygroups_aos_quotes_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2015-02-16 16:56:38
$layout_defs["SecurityGroups"]["subpanel_setup"]['timesheet_securitygroups_1'] = array (
  'order' => 100,
  'module' => 'Timesheet',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_TIMESHEET_SECURITYGROUPS_1_FROM_TIMESHEET_TITLE',
  'get_subpanel_data' => 'timesheet_securitygroups_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>