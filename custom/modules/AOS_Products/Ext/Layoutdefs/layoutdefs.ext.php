<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2014-05-08 10:48:41
$layout_defs["AOS_Products"]["subpanel_setup"]['aos_products_aos_invoices_1'] = array (
  'order' => 100,
  'module' => 'AOS_Invoices',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_AOS_PRODUCTS_AOS_INVOICES_1_FROM_AOS_INVOICES_TITLE',
  'get_subpanel_data' => 'aos_products_aos_invoices_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2017-10-06 11:09:43
$layout_defs["AOS_Products"]["subpanel_setup"]['aos_products_cases_1'] = array (
  'order' => 100,
  'module' => 'Cases',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_AOS_PRODUCTS_CASES_1_FROM_CASES_TITLE',
  'get_subpanel_data' => 'aos_products_cases_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2018-01-12 08:20:30
$layout_defs["AOS_Products"]["subpanel_setup"]['aos_products_opportunities_1'] = array (
  'order' => 100,
  'module' => 'Opportunities',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_AOS_PRODUCTS_OPPORTUNITIES_1_FROM_OPPORTUNITIES_TITLE',
  'get_subpanel_data' => 'aos_products_opportunities_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2014-10-22 16:58:44
$layout_defs["AOS_Products"]["subpanel_setup"]['bugs_aos_products_1'] = array (
  'order' => 100,
  'module' => 'Bugs',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_BUGS_AOS_PRODUCTS_1_FROM_BUGS_TITLE',
  'get_subpanel_data' => 'bugs_aos_products_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2014-04-11 12:48:55
$layout_defs["AOS_Products"]["subpanel_setup"]['documents_aos_products_1'] = array (
  'order' => 100,
  'module' => 'Documents',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_DOCUMENTS_AOS_PRODUCTS_1_FROM_DOCUMENTS_TITLE',
  'get_subpanel_data' => 'documents_aos_products_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


//auto-generated file DO NOT EDIT
$layout_defs['AOS_Products']['subpanel_setup']['aos_products_aos_invoices_1']['override_subpanel_name'] = 'AOS_Products_subpanel_aos_products_aos_invoices_1';


//auto-generated file DO NOT EDIT
$layout_defs['AOS_Products']['subpanel_setup']['aos_products_bugs_1']['override_subpanel_name'] = 'AOS_Products_subpanel_aos_products_bugs_1';


//auto-generated file DO NOT EDIT
$layout_defs['AOS_Products']['subpanel_setup']['aos_products_purchases']['override_subpanel_name'] = 'AOS_Products_subpanel_aos_products_purchases';


//auto-generated file DO NOT EDIT
$layout_defs['AOS_Products']['subpanel_setup']['bugs_aos_products_1']['override_subpanel_name'] = 'AOS_Products_subpanel_bugs_aos_products_1';

?>