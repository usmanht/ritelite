<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2014-05-08 10:48:41
$dictionary["AOS_Products"]["fields"]["aos_products_aos_invoices_1"] = array (
  'name' => 'aos_products_aos_invoices_1',
  'type' => 'link',
  'relationship' => 'aos_products_aos_invoices_1',
  'source' => 'non-db',
  'module' => 'AOS_Invoices',
  'bean_name' => 'AOS_Invoices',
  'vname' => 'LBL_AOS_PRODUCTS_AOS_INVOICES_1_FROM_AOS_INVOICES_TITLE',
);


// created: 2017-10-06 11:09:43
$dictionary["AOS_Products"]["fields"]["aos_products_cases_1"] = array (
  'name' => 'aos_products_cases_1',
  'type' => 'link',
  'relationship' => 'aos_products_cases_1',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'side' => 'right',
  'vname' => 'LBL_AOS_PRODUCTS_CASES_1_FROM_CASES_TITLE',
);


// created: 2018-01-12 08:20:30
$dictionary["AOS_Products"]["fields"]["aos_products_opportunities_1"] = array (
  'name' => 'aos_products_opportunities_1',
  'type' => 'link',
  'relationship' => 'aos_products_opportunities_1',
  'source' => 'non-db',
  'module' => 'Opportunities',
  'bean_name' => 'Opportunity',
  'side' => 'right',
  'vname' => 'LBL_AOS_PRODUCTS_OPPORTUNITIES_1_FROM_OPPORTUNITIES_TITLE',
);


// created: 2014-10-22 16:58:44
$dictionary["AOS_Products"]["fields"]["bugs_aos_products_1"] = array (
  'name' => 'bugs_aos_products_1',
  'type' => 'link',
  'relationship' => 'bugs_aos_products_1',
  'source' => 'non-db',
  'module' => 'Bugs',
  'bean_name' => 'Bug',
  'vname' => 'LBL_BUGS_AOS_PRODUCTS_1_FROM_BUGS_TITLE',
);


// created: 2014-04-11 12:48:55
$dictionary["AOS_Products"]["fields"]["documents_aos_products_1"] = array (
  'name' => 'documents_aos_products_1',
  'type' => 'link',
  'relationship' => 'documents_aos_products_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'vname' => 'LBL_DOCUMENTS_AOS_PRODUCTS_1_FROM_DOCUMENTS_TITLE',
);


// created: 2015-05-28 17:21:59
$dictionary["AOS_Products"]["fields"]["rl_sales_order_line_items_aos_products_1"] = array (
  'name' => 'rl_sales_order_line_items_aos_products_1',
  'type' => 'link',
  'relationship' => 'rl_sales_order_line_items_aos_products_1',
  'source' => 'non-db',
  'module' => 'RL_Sales_Order_Line_Items',
  'bean_name' => 'RL_Sales_Order_Line_Items',
  'vname' => 'LBL_RL_SALES_ORDER_LINE_ITEMS_AOS_PRODUCTS_1_FROM_RL_SALES_ORDER_LINE_ITEMS_TITLE',
  'id_name' => 'rl_sales_order_line_items_aos_products_1rl_sales_order_line_items_ida',
);
$dictionary["AOS_Products"]["fields"]["rl_sales_order_line_items_aos_products_1_name"] = array (
  'name' => 'rl_sales_order_line_items_aos_products_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_RL_SALES_ORDER_LINE_ITEMS_AOS_PRODUCTS_1_FROM_RL_SALES_ORDER_LINE_ITEMS_TITLE',
  'save' => true,
  'id_name' => 'rl_sales_order_line_items_aos_products_1rl_sales_order_line_items_ida',
  'link' => 'rl_sales_order_line_items_aos_products_1',
  'table' => 'rl_sales_order_line_items',
  'module' => 'RL_Sales_Order_Line_Items',
  'rname' => 'name',
);
$dictionary["AOS_Products"]["fields"]["rl_sales_order_line_items_aos_products_1rl_sales_order_line_items_ida"] = array (
  'name' => 'rl_sales_order_line_items_aos_products_1rl_sales_order_line_items_ida',
  'type' => 'link',
  'relationship' => 'rl_sales_order_line_items_aos_products_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_RL_SALES_ORDER_LINE_ITEMS_AOS_PRODUCTS_1_FROM_AOS_PRODUCTS_TITLE',
);


 // created: 2014-03-21 17:02:17
$dictionary['AOS_Products']['fields']['back_order_quantity_c']['labelValue']='Quantity on Back Order ';

 

 // created: 2018-02-23 10:48:58
$dictionary['AOS_Products']['fields']['category']['inline_edit']=true;
$dictionary['AOS_Products']['fields']['category']['massupdate']='1';
$dictionary['AOS_Products']['fields']['category']['merge_filter']='disabled';

 

 // created: 2014-03-21 17:02:17
$dictionary['AOS_Products']['fields']['classification_c']['labelValue']='Classification';

 

 // created: 2014-03-21 17:02:17
$dictionary['AOS_Products']['fields']['commodity_code_c']['labelValue']='Commodity Code';

 

 // created: 2014-03-21 17:02:18
$dictionary['AOS_Products']['fields']['lowest_sale_price_c']['labelValue']='Lowest Sale Price';

 

 // created: 2014-01-07 12:07:19
$dictionary['AOS_Products']['fields']['name']['duplicate_merge']='disabled';
$dictionary['AOS_Products']['fields']['name']['duplicate_merge_dom_value']='0';
$dictionary['AOS_Products']['fields']['name']['merge_filter']='disabled';
$dictionary['AOS_Products']['fields']['name']['unified_search']=false;

 

 // created: 2014-03-21 17:02:18
$dictionary['AOS_Products']['fields']['ninety_days_sales_c']['labelValue']='90 Days Sales Figure';

 

 // created: 2014-05-01 16:30:15
$dictionary['AOS_Products']['fields']['part_number']['len']='30';
$dictionary['AOS_Products']['fields']['part_number']['merge_filter']='disabled';

 

 // created: 2014-01-07 12:07:30
$dictionary['AOS_Products']['fields']['price']['merge_filter']='disabled';

 

 // created: 2014-03-21 17:02:18
$dictionary['AOS_Products']['fields']['quantity_c']['labelValue']='Quantity in Stock';

 
?>