<?php
// created: 2017-11-28 16:34:05
$subpanel_layout['list_fields'] = array (
  'tracker_name' => 
  array (
    'vname' => 'LBL_SUBPANEL_TRACKER_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '30%',
    'default' => true,
  ),
  'tracker_url' => 
  array (
    'vname' => 'LBL_SUBPANEL_TRACKER_URL',
    'width' => '60%',
    'default' => true,
  ),
  'tracker_key' => 
  array (
    'vname' => 'LBL_SUBPANEL_TRACKER_KEY',
    'width' => '10%',
    'default' => true,
  ),
  'edit_button' => 
  array (
    'vname' => 'LBL_EDIT_BUTTON',
    'widget_class' => 'SubPanelEditButton',
    'module' => 'Cases',
    'width' => '5%',
    'default' => true,
  ),
  'remove_button' => 
  array (
    'vname' => 'LBL_REMOVE',
    'widget_class' => 'SubPanelRemoveButton',
    'module' => 'Cases',
    'width' => '5%',
    'default' => true,
  ),
);