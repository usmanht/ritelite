<?php 
 //WARNING: The contents of this file are auto-generated



$dictionary['Task']['fields']['SecurityGroups'] = array (
  	'name' => 'SecurityGroups',
    'type' => 'link',
	'relationship' => 'securitygroups_tasks',
	'module'=>'SecurityGroups',
	'bean_name'=>'SecurityGroup',
    'source'=>'non-db',
	'vname'=>'LBL_SECURITYGROUPS',
);






 // created: 2015-12-10 09:33:51
$dictionary['Task']['fields']['date_due']['display_default']='now&09:30am';
$dictionary['Task']['fields']['date_due']['required']=true;
$dictionary['Task']['fields']['date_due']['massupdate']='1';
$dictionary['Task']['fields']['date_due']['merge_filter']='disabled';

 

 // created: 2015-12-10 09:34:10
$dictionary['Task']['fields']['date_start']['display_default']='now&09:00am';
$dictionary['Task']['fields']['date_start']['required']=true;
$dictionary['Task']['fields']['date_start']['massupdate']='1';
$dictionary['Task']['fields']['date_start']['merge_filter']='disabled';

 

// created: 2014-01-07 11:53:04
$dictionary["Task"]["fields"]["users_tasks_1"] = array (
  'name' => 'users_tasks_1',
  'type' => 'link',
  'relationship' => 'users_tasks_1',
  'source' => 'non-db',
  'module' => 'Users',
  'bean_name' => 'User',
  'vname' => 'LBL_USERS_TASKS_1_FROM_USERS_TITLE',
  'id_name' => 'users_tasks_1users_ida',
);
$dictionary["Task"]["fields"]["users_tasks_1_name"] = array (
  'name' => 'users_tasks_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_USERS_TASKS_1_FROM_USERS_TITLE',
  'save' => true,
  'id_name' => 'users_tasks_1users_ida',
  'link' => 'users_tasks_1',
  'table' => 'users',
  'module' => 'Users',
  'rname' => 'name',
);
$dictionary["Task"]["fields"]["users_tasks_1users_ida"] = array (
  'name' => 'users_tasks_1users_ida',
  'type' => 'link',
  'relationship' => 'users_tasks_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_USERS_TASKS_1_FROM_TASKS_TITLE',
);

?>