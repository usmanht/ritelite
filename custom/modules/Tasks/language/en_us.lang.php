<?php
// created: 2014-08-01 16:39:30
$mod_strings = array (
  'LBL_SECURITYGROUPS_SUBPANEL_TITLE' => 'Security Groups',
  'LBL_CREATED' => 'Owner',
  'LBL_LIST_ASSIGNED_USER' => 'Assigned to',
  'LBL_DUE_DATE' => 'Due Date:',
  'LBL_START_DATE' => 'Start Date:',
  'LBL_QUICKCREATE_PANEL1' => 'Other',
);