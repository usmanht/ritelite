<?php
// created: 2017-11-28 16:27:14
$mod_strings = array (
  'LBL_SECURITYGROUPS_SUBPANEL_TITLE' => 'Security Groups',
  'LBL_LOG_ENTRIES_LINK_TITLE' => 'Click-thru Link',
  'LBL_TRACKED_URLS_SUBPANEL_TITLE' => 'Tracker URLs',
);