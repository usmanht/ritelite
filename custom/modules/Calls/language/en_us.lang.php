<?php
// created: 2015-10-21 10:36:48
$mod_strings = array (
  'LBL_ASSIGNED_TO_NAME' => 'Assigned to',
  'LBL_EDITVIEW_PANEL1' => 'Details',
  'LBL_ACCOUNT_NAME_ACCOUNT_ID' => 'Account Name (related Account ID)',
  'LBL_ACCOUNT_NAME' => 'Account Name',
  'LBL_EDITVIEW_PANEL2' => 'Created',
  'LBL_AOS_QUOTES_CALLS_1_FROM_AOS_QUOTES_TITLE' => 'Related Quote',
  'LBL_CALLS_ACCOUNTS_1_FROM_ACCOUNTS_TITLE' => 'Accounts',
  'LBL_OPPORTUNITIES_CALLS_1_FROM_OPPORTUNITIES_TITLE' => 'Related Opportunity',
  'LBL_CALL_NOTES' => 'call notes',
  'LBL_EDITVIEW_PANEL3' => 'Call Notes',
  'LBL_STATUS' => 'Status:',
);