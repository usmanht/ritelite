<?php 
 //WARNING: The contents of this file are auto-generated


//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_AOS_QUOTES_CALLS_1_FROM_AOS_QUOTES_TITLE'] = 'Quotes';


//THIS FILE IS AUTO GENERATED, DO NOT MODIFY
$mod_strings['LBL_AOS_QUOTES_CALLS_1_FROM_AOS_QUOTES_TITLE'] = 'Quotes';
$mod_strings['LBL_OPPORTUNITIES_CALLS_1_FROM_OPPORTUNITIES_TITLE'] = 'Opportunities';


$mod_strings = array (
    'LBL_RESCHEDULE_COUNT' => 'Call Attempts',
    'LBL_RESCHEDULE_DATE' => 'Date',
    'LBL_RESCHEDULE_REASON' => 'Reason',
    'LBL_RESCHEDULE_ERROR1' => 'Please select a valid date',
    'LBL_RESCHEDULE_ERROR2' => 'Please select a reason',
    'LBL_RESCHEDULE_PANEL' => 'Reschedule',
    'LBL_RESCHEDULE_HISTORY' => 'Call Attempt History'

);



$mod_strings = array_merge($mod_strings,
	array(
		 'LBL_SECURITYGROUPS_SUBPANEL_TITLE' => "Security Groups",
	)
);


?>