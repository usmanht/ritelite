<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2015-05-28 17:21:59
$layout_defs["RL_Sales_Order_Line_Items"]["subpanel_setup"]['rl_sales_order_line_items_aos_products_1'] = array (
  'order' => 100,
  'module' => 'AOS_Products',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_RL_SALES_ORDER_LINE_ITEMS_AOS_PRODUCTS_1_FROM_AOS_PRODUCTS_TITLE',
  'get_subpanel_data' => 'rl_sales_order_line_items_aos_products_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>