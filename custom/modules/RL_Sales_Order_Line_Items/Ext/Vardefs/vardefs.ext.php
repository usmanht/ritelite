<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2014-05-07 16:09:41
$dictionary["RL_Sales_Order_Line_Items"]["fields"]["accounts_rl_sales_order_line_items_1"] = array (
  'name' => 'accounts_rl_sales_order_line_items_1',
  'type' => 'link',
  'relationship' => 'accounts_rl_sales_order_line_items_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_RL_SALES_ORDER_LINE_ITEMS_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_rl_sales_order_line_items_1accounts_ida',
);
$dictionary["RL_Sales_Order_Line_Items"]["fields"]["accounts_rl_sales_order_line_items_1_name"] = array (
  'name' => 'accounts_rl_sales_order_line_items_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_RL_SALES_ORDER_LINE_ITEMS_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_rl_sales_order_line_items_1accounts_ida',
  'link' => 'accounts_rl_sales_order_line_items_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["RL_Sales_Order_Line_Items"]["fields"]["accounts_rl_sales_order_line_items_1accounts_ida"] = array (
  'name' => 'accounts_rl_sales_order_line_items_1accounts_ida',
  'type' => 'link',
  'relationship' => 'accounts_rl_sales_order_line_items_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_RL_SALES_ORDER_LINE_ITEMS_1_FROM_RL_SALES_ORDER_LINE_ITEMS_TITLE',
);


// created: 2014-05-07 16:11:42
$dictionary["RL_Sales_Order_Line_Items"]["fields"]["accounts_rl_sales_order_line_items_2"] = array (
  'name' => 'accounts_rl_sales_order_line_items_2',
  'type' => 'link',
  'relationship' => 'accounts_rl_sales_order_line_items_2',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_RL_SALES_ORDER_LINE_ITEMS_2_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_rl_sales_order_line_items_2accounts_ida',
);
$dictionary["RL_Sales_Order_Line_Items"]["fields"]["accounts_rl_sales_order_line_items_2_name"] = array (
  'name' => 'accounts_rl_sales_order_line_items_2_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_RL_SALES_ORDER_LINE_ITEMS_2_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_rl_sales_order_line_items_2accounts_ida',
  'link' => 'accounts_rl_sales_order_line_items_2',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["RL_Sales_Order_Line_Items"]["fields"]["accounts_rl_sales_order_line_items_2accounts_ida"] = array (
  'name' => 'accounts_rl_sales_order_line_items_2accounts_ida',
  'type' => 'link',
  'relationship' => 'accounts_rl_sales_order_line_items_2',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_RL_SALES_ORDER_LINE_ITEMS_2_FROM_RL_SALES_ORDER_LINE_ITEMS_TITLE',
);


// created: 2014-05-07 16:21:34
$dictionary["RL_Sales_Order_Line_Items"]["fields"]["contacts_rl_sales_order_line_items_1"] = array (
  'name' => 'contacts_rl_sales_order_line_items_1',
  'type' => 'link',
  'relationship' => 'contacts_rl_sales_order_line_items_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_CONTACTS_RL_SALES_ORDER_LINE_ITEMS_1_FROM_CONTACTS_TITLE',
  'id_name' => 'contacts_rl_sales_order_line_items_1contacts_ida',
);
$dictionary["RL_Sales_Order_Line_Items"]["fields"]["contacts_rl_sales_order_line_items_1_name"] = array (
  'name' => 'contacts_rl_sales_order_line_items_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_RL_SALES_ORDER_LINE_ITEMS_1_FROM_CONTACTS_TITLE',
  'save' => true,
  'id_name' => 'contacts_rl_sales_order_line_items_1contacts_ida',
  'link' => 'contacts_rl_sales_order_line_items_1',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["RL_Sales_Order_Line_Items"]["fields"]["contacts_rl_sales_order_line_items_1contacts_ida"] = array (
  'name' => 'contacts_rl_sales_order_line_items_1contacts_ida',
  'type' => 'link',
  'relationship' => 'contacts_rl_sales_order_line_items_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_RL_SALES_ORDER_LINE_ITEMS_1_FROM_RL_SALES_ORDER_LINE_ITEMS_TITLE',
);


// created: 2014-05-07 16:23:44
$dictionary["RL_Sales_Order_Line_Items"]["fields"]["contacts_rl_sales_order_line_items_2"] = array (
  'name' => 'contacts_rl_sales_order_line_items_2',
  'type' => 'link',
  'relationship' => 'contacts_rl_sales_order_line_items_2',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_CONTACTS_RL_SALES_ORDER_LINE_ITEMS_2_FROM_CONTACTS_TITLE',
  'id_name' => 'contacts_rl_sales_order_line_items_2contacts_ida',
);
$dictionary["RL_Sales_Order_Line_Items"]["fields"]["contacts_rl_sales_order_line_items_2_name"] = array (
  'name' => 'contacts_rl_sales_order_line_items_2_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_RL_SALES_ORDER_LINE_ITEMS_2_FROM_CONTACTS_TITLE',
  'save' => true,
  'id_name' => 'contacts_rl_sales_order_line_items_2contacts_ida',
  'link' => 'contacts_rl_sales_order_line_items_2',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["RL_Sales_Order_Line_Items"]["fields"]["contacts_rl_sales_order_line_items_2contacts_ida"] = array (
  'name' => 'contacts_rl_sales_order_line_items_2contacts_ida',
  'type' => 'link',
  'relationship' => 'contacts_rl_sales_order_line_items_2',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_RL_SALES_ORDER_LINE_ITEMS_2_FROM_RL_SALES_ORDER_LINE_ITEMS_TITLE',
);


// created: 2015-05-28 17:21:59
$dictionary["RL_Sales_Order_Line_Items"]["fields"]["rl_sales_order_line_items_aos_products_1"] = array (
  'name' => 'rl_sales_order_line_items_aos_products_1',
  'type' => 'link',
  'relationship' => 'rl_sales_order_line_items_aos_products_1',
  'source' => 'non-db',
  'module' => 'AOS_Products',
  'bean_name' => 'AOS_Products',
  'side' => 'right',
  'vname' => 'LBL_RL_SALES_ORDER_LINE_ITEMS_AOS_PRODUCTS_1_FROM_AOS_PRODUCTS_TITLE',
);


/**
 * Created by PhpStorm.
 * User: graeme
 * Date: 06/04/16
 * Time: 13:49
 */

$dictionary['RL_Sales_Order_Line_Items']['fields']['line_items']['function']= array(
    'name' => 'display_lines',
    'returns' => 'html',
    'include' => 'modules/RL_Sales_Order_Line_Items/Line_Items.php'
);


$dictionary['RL_Sales_Order_Line_Items']['fields']['shipping_tax_amt']['function']= array(
    'name' => 'display_lines',
    'returns' => 'html',
    'include' => 'modules/RL_Sales_Order_Line_Items/Line_Items.php'
);


 // created: 2014-05-05 15:23:44
$dictionary['RL_Sales_Order_Line_Items']['fields']['customer_order_number']['len']='20';

 

 // created: 2014-05-06 11:09:45
$dictionary['RL_Sales_Order_Line_Items']['fields']['date_entered']['comments']='Date record created';
$dictionary['RL_Sales_Order_Line_Items']['fields']['date_entered']['merge_filter']='disabled';

 

 // created: 2014-05-07 17:22:10
$dictionary['RL_Sales_Order_Line_Items']['fields']['invoice_date_c']['labelValue']='Invoice Date ';

 

 // created: 2014-04-17 13:27:20
$dictionary['RL_Sales_Order_Line_Items']['fields']['item_description']['merge_filter']='disabled';

 

 // created: 2014-04-21 13:48:10
$dictionary['RL_Sales_Order_Line_Items']['fields']['name']['merge_filter']='disabled';
$dictionary['RL_Sales_Order_Line_Items']['fields']['name']['unified_search']=false;
$dictionary['RL_Sales_Order_Line_Items']['fields']['name']['rows']='4';
$dictionary['RL_Sales_Order_Line_Items']['fields']['name']['cols']='20';

 

 // created: 2014-04-21 13:47:44

 

 // created: 2015-03-20 15:18:23
$dictionary['RL_Sales_Order_Line_Items']['fields']['polink_c']['labelValue']='PO Link Number';

 

 // created: 2014-04-17 13:32:44
$dictionary['RL_Sales_Order_Line_Items']['fields']['product_list_price']['merge_filter']='disabled';
$dictionary['RL_Sales_Order_Line_Items']['fields']['product_list_price']['enable_range_search']=false;

 

 // created: 2014-04-25 11:16:01
$dictionary['RL_Sales_Order_Line_Items']['fields']['product_list_price_c']['merge_filter']='disabled';
$dictionary['RL_Sales_Order_Line_Items']['fields']['product_list_price_c']['enable_range_search']=false;

 

 // created: 2014-04-21 16:16:20
$dictionary['RL_Sales_Order_Line_Items']['fields']['product_qty']['merge_filter']='disabled';
$dictionary['RL_Sales_Order_Line_Items']['fields']['product_qty']['precision']='0';

 

 // created: 2014-04-25 11:18:23
$dictionary['RL_Sales_Order_Line_Items']['fields']['product_unit_price']['merge_filter']='disabled';
$dictionary['RL_Sales_Order_Line_Items']['fields']['product_unit_price']['enable_range_search']=false;

 

 // created: 2014-05-07 17:16:45
$dictionary['RL_Sales_Order_Line_Items']['fields']['sage_sop_number_c']['labelValue']='Sage SOP Number';

 

 // created: 2015-03-13 15:41:37
$dictionary['RL_Sales_Order_Line_Items']['fields']['salesordersystemnotes_c']['labelValue']='Sales Order Processing System Notes';

 

 // created: 2014-04-17 14:02:42
$dictionary['RL_Sales_Order_Line_Items']['fields']['sales_order_number']['merge_filter']='disabled';
$dictionary['RL_Sales_Order_Line_Items']['fields']['sales_order_number']['unified_search']=false;
$dictionary['RL_Sales_Order_Line_Items']['fields']['sales_order_number']['enable_range_search']=false;
$dictionary['RL_Sales_Order_Line_Items']['fields']['sales_order_number']['min']=false;
$dictionary['RL_Sales_Order_Line_Items']['fields']['sales_order_number']['max']=false;

 
?>