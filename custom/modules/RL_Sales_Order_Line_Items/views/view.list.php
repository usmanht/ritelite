<?php
/**
 * view.list.php
 * @author SalesAgility (Andrew Mclaughlan) <support@salesagility.com>
 * Date: 21/04/14
 * Comments
 */
class RL_Sales_Order_Line_ItemsViewList extends ViewList {
    function RL_Sales_Order_Line_ItemsViewList(){
        parent::ViewList();
    }


    function listViewProcess(){
        $this->processSearchForm();
        $this->lv->searchColumns = $this->searchForm->searchColumns;
        if(!$this->headers)
            return;
        if(empty($_REQUEST['search_form_only']) || $_REQUEST['search_form_only'] == false){
            $this->lv->setup($this->seed, 'custom/modules/RL_Sales_Order_Line_Items/tpls/ListViewGeneric.tpl', $this->where, $this->params);
            $savedSearchName = empty($_REQUEST['saved_search_select_name']) ? '' : (' - ' . $_REQUEST['saved_search_select_name']);
            echo get_form_header($GLOBALS['mod_strings']['LBL_LIST_FORM_TITLE'] . $savedSearchName, '', false);
            $total_qty = 0;
            $total_value = 0;

            foreach($this->lv->data['data'] as $entry) {
                $total_qty += unformat_number($entry['PRODUCT_QTY']);
            }

            $this->lv->ss->assign('TOTAL_QUANTITY', '<div>' . $total_qty . '</div>');
            $currency_settings = array("currency_symbol" => true);
            foreach($this->lv->data['data'] as $entry) {
                $total_value += $entry['PRODUCT_TOTAL_PRICE'];
            }

            $formatted_total = format_number($total_value, 2, 2, $currency_settings);
            $this->lv->ss->assign('TOTAL_AMOUNT', '<div>' . $formatted_total . '</div>');
            echo $this->lv->display();
        }
    }

}
