<?php
$popupMeta = array (
    'moduleMain' => 'RL_Sales_Order_Line_Items',
    'varName' => 'RL_Sales_Order_Line_Items',
    'orderBy' => 'rl_sales_order_line_items.name',
    'whereClauses' => array (
  'name' => 'rl_sales_order_line_items.name',
  'item_description' => 'rl_sales_order_line_items.item_description',
  'parent_name' => 'rl_sales_order_line_items.parent_name',
  'sales_order_number' => 'rl_sales_order_line_items.sales_order_number',
  'sage_sop_number_c' => 'rl_sales_order_line_items_cstm.sage_sop_number_c',
  'customer_order_number' => 'rl_sales_order_line_items.customer_order_number',
  'invoice_number' => 'rl_sales_order_line_items.invoice_number',
  'date_entered' => 'rl_sales_order_line_items.date_entered',
  'date_despatched' => 'rl_sales_order_line_items.date_despatched',
  'accounts_rl_sales_order_line_items_2_name' => 'rl_sales_order_line_items.accounts_rl_sales_order_line_items_2_name',
  'accounts_rl_sales_order_line_items_1_name' => 'rl_sales_order_line_items.accounts_rl_sales_order_line_items_1_name',
  'contacts_rl_sales_order_line_items_2_name' => 'rl_sales_order_line_items.contacts_rl_sales_order_line_items_2_name',
  'contacts_rl_sales_order_line_items_1_name' => 'rl_sales_order_line_items.contacts_rl_sales_order_line_items_1_name',
  'assigned_user_name' => 'rl_sales_order_line_items.assigned_user_name',
  'current_user_only' => 'rl_sales_order_line_items.current_user_only',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'item_description',
  5 => 'parent_name',
  6 => 'sales_order_number',
  7 => 'sage_sop_number_c',
  8 => 'customer_order_number',
  9 => 'invoice_number',
  10 => 'date_entered',
  11 => 'date_despatched',
  12 => 'accounts_rl_sales_order_line_items_2_name',
  13 => 'accounts_rl_sales_order_line_items_1_name',
  14 => 'contacts_rl_sales_order_line_items_2_name',
  15 => 'contacts_rl_sales_order_line_items_1_name',
  16 => 'assigned_user_name',
  17 => 'current_user_only',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => '10%',
  ),
  'item_description' => 
  array (
    'type' => 'text',
    'label' => 'LBL_PRODUCT_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'name' => 'item_description',
  ),
  'parent_name' => 
  array (
    'type' => 'parent',
    'studio' => 'visible',
    'label' => 'LBL_FLEX_RELATE',
    'link' => true,
    'sortable' => false,
    'ACLTag' => 'PARENT',
    'dynamic_module' => 'PARENT_TYPE',
    'id' => 'PARENT_ID',
    'related_fields' => 
    array (
      0 => 'parent_id',
      1 => 'parent_type',
    ),
    'width' => '10%',
    'name' => 'parent_name',
  ),
  'sales_order_number' => 
  array (
    'type' => 'int',
    'label' => 'LBL_SALES_ORDER_NUMBER',
    'width' => '10%',
    'name' => 'sales_order_number',
  ),
  'sage_sop_number_c' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_SAGE_SOP_NUMBER_C',
    'width' => '10%',
    'name' => 'sage_sop_number_c',
  ),
  'customer_order_number' => 
  array (
    'type' => 'int',
    'label' => 'LBL_CUSTOMER_ORDER_NUMBER',
    'width' => '10%',
    'name' => 'customer_order_number',
  ),
  'invoice_number' => 
  array (
    'type' => 'int',
    'label' => 'LBL_INVOICE_NUMBER',
    'width' => '10%',
    'name' => 'invoice_number',
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'name' => 'date_entered',
  ),
  'date_despatched' => 
  array (
    'type' => 'date',
    'label' => 'LBL_DATE_DESPATCHED',
    'width' => '10%',
    'name' => 'date_despatched',
  ),
  'accounts_rl_sales_order_line_items_2_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_ACCOUNTS_RL_SALES_ORDER_LINE_ITEMS_2_FROM_ACCOUNTS_TITLE',
    'id' => 'ACCOUNTS_RL_SALES_ORDER_LINE_ITEMS_2ACCOUNTS_IDA',
    'width' => '10%',
    'name' => 'accounts_rl_sales_order_line_items_2_name',
  ),
  'accounts_rl_sales_order_line_items_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_ACCOUNTS_RL_SALES_ORDER_LINE_ITEMS_1_FROM_ACCOUNTS_TITLE',
    'id' => 'ACCOUNTS_RL_SALES_ORDER_LINE_ITEMS_1ACCOUNTS_IDA',
    'width' => '10%',
    'name' => 'accounts_rl_sales_order_line_items_1_name',
  ),
  'contacts_rl_sales_order_line_items_2_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CONTACTS_RL_SALES_ORDER_LINE_ITEMS_2_FROM_CONTACTS_TITLE',
    'id' => 'CONTACTS_RL_SALES_ORDER_LINE_ITEMS_2CONTACTS_IDA',
    'width' => '10%',
    'name' => 'contacts_rl_sales_order_line_items_2_name',
  ),
  'contacts_rl_sales_order_line_items_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CONTACTS_RL_SALES_ORDER_LINE_ITEMS_1_FROM_CONTACTS_TITLE',
    'id' => 'CONTACTS_RL_SALES_ORDER_LINE_ITEMS_1CONTACTS_IDA',
    'width' => '10%',
    'name' => 'contacts_rl_sales_order_line_items_1_name',
  ),
  'assigned_user_name' => 
  array (
    'link' => true,
    'type' => 'relate',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'id' => 'ASSIGNED_USER_ID',
    'width' => '10%',
    'name' => 'assigned_user_name',
  ),
  'current_user_only' => 
  array (
    'label' => 'LBL_CURRENT_USER_FILTER',
    'type' => 'bool',
    'width' => '10%',
    'name' => 'current_user_only',
  ),
),
);
