<?php
$dashletData['RL_Sales_Order_Line_ItemsDashlet']['searchFields'] = array (
  'sales_order_number' => 
  array (
    'default' => '',
  ),
  'customer_order_number' => 
  array (
    'default' => '',
  ),
  'sage_sop_number_c' => 
  array (
    'default' => '',
  ),
  'accounts_rl_sales_order_line_items_1_name' => 
  array (
    'default' => '',
  ),
  'contacts_rl_sales_order_line_items_1_name' => 
  array (
    'default' => '',
  ),
  'name' => 
  array (
    'default' => '',
  ),
  'invoice_number' => 
  array (
    'default' => '',
  ),
  'assigned_user_name' => 
  array (
    'default' => '',
  ),
);
$dashletData['RL_Sales_Order_Line_ItemsDashlet']['columns'] = array (
  'sales_order_number' => 
  array (
    'type' => 'int',
    'label' => 'LBL_SALES_ORDER_NUMBER',
    'width' => '10%',
    'default' => true,
  ),
  'number' => 
  array (
    'type' => 'int',
    'label' => 'LBL_LIST_NUM',
    'width' => '10%',
    'default' => true,
  ),
  'product_qty' => 
  array (
    'type' => 'decimal',
    'label' => 'LBL_PRODUCT_QTY',
    'width' => '10%',
    'default' => true,
  ),
  'part_number' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'label' => 'LBL_PART_NUMBER',
    'width' => '10%',
  ),
  'item_description' => 
  array (
    'type' => 'text',
    'label' => 'LBL_PRODUCT_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => true,
  ),
  'product_total_price' => 
  array (
    'type' => 'currency',
    'label' => 'LBL_PRODUCT_TOTAL_PRICE',
    'currency_format' => true,
    'width' => '10%',
    'default' => true,
  ),
  'date_modified' => 
  array (
    'width' => '15%',
    'label' => 'LBL_DATE_MODIFIED',
    'name' => 'date_modified',
    'default' => false,
  ),
  'created_by' => 
  array (
    'width' => '8%',
    'label' => 'LBL_CREATED',
    'name' => 'created_by',
    'default' => false,
  ),
  'date_entered' => 
  array (
    'width' => '15%',
    'label' => 'LBL_DATE_ENTERED',
    'default' => false,
    'name' => 'date_entered',
  ),
);
