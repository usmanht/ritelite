<?php
/**
 * PersonalInboundEmail.php
 * @author SalesAgility <support@salesagility.com>
 * Date: 16/12/13
 */

require_once('modules/InboundEmail/InboundEmail.php');

class PersonalInboundEmail extends InboundEmail {

    function getNewMessageIds() {
        $storedOptions = unserialize(base64_decode($this->stored_options));
        $limit = 100;
        $needSave = false;
        //TODO figure out if the since date is UDT
		$GLOBALS['log']->info('Checking stored options');
        if(!empty($storedOptions['only_since'][$this->mailbox])) {// POP3 does not support Unseen flags
            if(empty($storedOptions['only_since_last'][$this->mailbox])) {
                $q = 'SELECT last_run FROM schedulers WHERE job = \'function::pollMonitoredInboxesPersonal\'';
                $r = $this->db->query($q, true);
                $a = $this->db->fetchByAssoc($r);

                $dateString = date('D, d M Y H:i:s O', strtotime($a['last_run']));
				$GLOBALS['log']->debug('Datestring from last_run is '.$dateString);
            } else {
                $dateString = $storedOptions['only_since_last'][$this->mailbox];
				$dateString = trim(preg_replace('/\s*\([^)]*\)/', '', $dateString));
				$GLOBALS['log']->debug('Datestring from stored options is '.$dateString);
            }
			
            $date = DateTime::createFromFormat('D, d M Y H:i:s O',$dateString);
            if(!$date){
                $date = DateTime::createFromFormat('D, d M Y H:i:s O T',$dateString);
            }
            if(!$date){
                $date = DateTime::createFromFormat('d M Y H:i:s O',$dateString);
            }
			
			
            /*$cutoff = new DateTime();
            $cutoff->sub(new DateInterval("P1M"));;
            if($cutoff < $date){
                $date = $cutoff;
                $limit = 0;//Override the limit since these are likely already imported
            }*/


            $date = $date->format('d-M-Y');
            //$date = date('d-M-Y', strtotime('03-Apr-2014'));
            $ret = imap_search($this->conn, 'SINCE "'.$date.'" UNDELETED');
            $check = imap_check($this->conn);
			$GLOBALS['log']->info('Checkdate is '.$check->Date);
            $storedOptions['only_since_last'][$this->mailbox] = $check->Date;
            $this->stored_options = base64_encode(serialize($storedOptions));
            $needSave = true;
        } else {
            $ret = imap_search($this->conn,'UNDELETED');

        }
		$GLOBALS['log']->info('-----> getNewMessageIds() got '.count($ret).' new Messages without limit');
        if(count($ret) > $limit && $limit > 0){
            $lastId = $ret[$limit];
            $ret = array_slice($ret,0,$limit);
            $storedOptions['only_since'][$this->mailbox] = true;
            $headers = imap_fetch_overview($this->conn,$lastId);
            $storedOptions['only_since_last'][$this->mailbox] = $headers[0]->date;
            $this->stored_options = base64_encode(serialize($storedOptions));
            $needSave = true;
        }else{
            $storedOptions['only_since'][$this->mailbox] = true;
            $needSave = true;
        }
        if(!empty($needSave)){
            $this->saveStoredOptions();
        }

        $GLOBALS['log']->info('-----> getNewMessageIds() got '.count($ret).' new Messages');
        return $ret;
    }

    private function saveStoredOptions(){
        global $db;
        return $db->query("UPDATE inbound_email SET stored_options = '{$this->stored_options}' WHERE id = '{$this->id}';");
    }

    function importOneEmail($msgNo, $uid, $forDisplay=false, $clean_email=true, $direction='inbound') {
        $GLOBALS['log']->debug("InboundEmail processing 1 email {$msgNo}-----------------------------------------------------------------------------------------");
        global $timedate;
        global $app_strings;
        global $app_list_strings;
        global $sugar_config;
        global $current_user;

        // Bug # 45477
        // So, on older versions of PHP (PHP VERSION < 5.3),
        // calling imap_headerinfo and imap_fetchheader can cause a buffer overflow for exteremly large headers,
        // This leads to the remaining messages not being read because Sugar crashes everytime it tries to read the headers.
        // The workaround is to mark a message as read before making trying to read the header of the msg in question
        // This forces this message not be read again, and we can continue processing remaining msgs.

        // UNCOMMENT THIS IF YOU HAVE THIS PROBLEM!  See notes on Bug # 45477
        // $this->markEmails($uid, "read");

        $header = imap_headerinfo($this->conn, $msgNo);
        $fullHeader = imap_fetchheader($this->conn, $msgNo); // raw headers

        // reset inline images cache
        $this->inlineImages = array();

        // handle messages deleted on server
        if(empty($header)) {
            if(!isset($this->email) || empty($this->email)) {
                $this->email = new Email();
            }

            $q = "";
            if ($this->isPop3Protocol()) {
                $this->email->name = $app_strings['LBL_EMAIL_ERROR_MESSAGE_DELETED'];
                $q = "DELETE FROM email_cache WHERE message_id = '{$uid}' AND ie_id = '{$this->id}' AND mbox = '{$this->mailbox}'";
            } else {
                $this->email->name = $app_strings['LBL_EMAIL_ERROR_IMAP_MESSAGE_DELETED'];
                $q = "DELETE FROM email_cache WHERE imap_uid = {$uid} AND ie_id = '{$this->id}' AND mbox = '{$this->mailbox}'";
            } // else
            // delete local cache
            $r = $this->db->query($q);

            $this->email->date_sent = $timedate->nowDb();
            return false;
            //return "Message deleted from server.";
        }

        ///////////////////////////////////////////////////////////////////////
        ////	DUPLICATE CHECK
        $dupeCheckResult = $this->importDupeCheck($header->message_id, $header, $fullHeader);
        if($forDisplay || $dupeCheckResult) {
            $GLOBALS['log']->debug('*********** NO duplicate found, continuing with processing.');

            $structure = imap_fetchstructure($this->conn, $msgNo); // map of email

            ///////////////////////////////////////////////////////////////////
            ////	CREATE SEED EMAIL OBJECT
            $email = new Email();
            $email->isDuplicate = ($dupeCheckResult) ? false : true;
            $email->mailbox_id = $this->id;
            $message = array();
            $email->id = create_guid();
            $email->new_with_id = true; //forcing a GUID here to prevent double saves.
            ////	END CREATE SEED EMAIL
            ///////////////////////////////////////////////////////////////////

            ///////////////////////////////////////////////////////////////////
            ////	PREP SYSTEM USER
            if(empty($current_user)) {
                // I-E runs as admin, get admin prefs

                $current_user = new User();
                $current_user->getSystemUser();
            }
            $tPref = $current_user->getUserDateTimePreferences();
            ////	END USER PREP
            ///////////////////////////////////////////////////////////////////
            if(!empty($header->date)) {
                $unixHeaderDate = $timedate->fromString($header->date);
            }
            ///////////////////////////////////////////////////////////////////
            ////	HANDLE EMAIL ATTACHEMENTS OR HTML TEXT
            ////	Inline images require that I-E handle attachments before body text
            // parts defines attachments - be mindful of .html being interpreted as an attachment
            if($structure->type == 1 && !empty($structure->parts)) {
                $GLOBALS['log']->debug('InboundEmail found multipart email - saving attachments if found.');
                $this->saveAttachments($msgNo, $structure->parts, $email->id, 0, $forDisplay);
            } elseif($structure->type == 0) {
                $uuemail = ($this->isUuencode($email->description)) ? true : false;
                /*
                 * UUEncoded attachments - legacy, but still have to deal with it
                 * format:
                 * begin 777 filename.txt
                 * UUENCODE
                 *
                 * end
                 */
                // set body to the filtered one
                if($uuemail) {
                    $email->description = $this->handleUUEncodedEmailBody($email->description, $email->id);
                    $email->retrieve($email->id);
                    $email->save();
                }
            } else {
                if($this->port != 110) {
                    $GLOBALS['log']->debug('InboundEmail found a multi-part email (id:'.$msgNo.') with no child parts to parse.');
                }
            }
            ////	END HANDLE EMAIL ATTACHEMENTS OR HTML TEXT
            ///////////////////////////////////////////////////////////////////

            ///////////////////////////////////////////////////////////////////
            ////	ASSIGN APPROPRIATE ATTRIBUTES TO NEW EMAIL OBJECT
            // handle UTF-8/charset encoding in the ***headers***
            global $db;
            $email->name			= $this->handleMimeHeaderDecode($header->subject);
            $email->type = $direction;
            if(!empty($unixHeaderDate)) {
                $email->date_sent = $timedate->asUser($unixHeaderDate);
                list($email->date_start, $email->time_start) = $timedate->split_date_time($email->date_sent);
            } else {
                $email->date_start = $email->time_start = $email->date_sent = "";
            }

            if($direction != 'inbound'){
                $email->status = 'sent';
            } else if($header->Recent == 'N' || $header->Unseen == 'U') {
                $email->status = 'unread';
            } else {
                $email->status = 'read';
            }

            $email->assigned_user_id = $this->group_id;

            if(!empty($header->toaddress)) {
                $email->to_name	 = $this->handleMimeHeaderDecode($header->toaddress);
                $email->to_addrs_names = $email->to_name;
            }
            if(!empty($header->to)) {
                $email->to_addrs	= $this->convertImapToSugarEmailAddress($header->to);
            }
            $email->from_name		= $this->handleMimeHeaderDecode($header->fromaddress);
            $email->from_addr_name = $email->from_name;
            $email->from_addr		= $this->convertImapToSugarEmailAddress($header->from);
            if(!empty($header->cc)) {
                $email->cc_addrs	= $this->convertImapToSugarEmailAddress($header->cc);
            }
            if(!empty($header->ccaddress)) {
                $email->cc_addrs_names	 = $this->handleMimeHeaderDecode($header->ccaddress);
            } // if
            $email->reply_to_name   = $this->handleMimeHeaderDecode($header->reply_toaddress);
            $email->reply_to_email  = $this->convertImapToSugarEmailAddress($header->reply_to);
            if (!empty($email->reply_to_email)) {
                $email->reply_to_addr   = $email->reply_to_name;
            }
            $email->intent			= $this->mailbox_type;

            $email->message_id		= $this->compoundMessageId; // filled by importDupeCheck();

            $oldPrefix = $this->imagePrefix;
            if(!$forDisplay) {
                // Store CIDs in imported messages, convert on display
                $this->imagePrefix = "cid:";
            }
            // handle multi-part email bodies
            $email->description_html= $this->getMessageText($msgNo, 'HTML', $structure, $fullHeader,$clean_email); // runs through handleTranserEncoding() already
            $email->description	= $this->getMessageText($msgNo, 'PLAIN', $structure, $fullHeader,$clean_email); // runs through handleTranserEncoding() already
            $this->imagePrefix = $oldPrefix;

            // empty() check for body content
            if(empty($email->description)) {
                $GLOBALS['log']->debug('InboundEmail Message (id:'.$email->message_id.') has no body');
            }

            // assign_to group
            if (!empty($_REQUEST['user_id'])) {
                $email->assigned_user_id = $_REQUEST['user_id'];
            } else {
                // Samir Gandhi : Commented out this code as its not needed
                //$email->assigned_user_id = $this->group_id;
            }

            //Assign Parent Values if set
            if (!empty($_REQUEST['parent_id']) && !empty($_REQUEST['parent_type'])) {
                $email->parent_id = $_REQUEST['parent_id'];
                $email->parent_type = $_REQUEST['parent_type'];

                $mod = strtolower($email->parent_type);
                $rel = array_key_exists($mod, $email->field_defs) ? $mod : $mod . "_activities_emails"; //Custom modules rel name

                if(! $email->load_relationship($rel) )
                    return FALSE;
                $email->$rel->add($email->parent_id);
            }

            // override $forDisplay w/user pref
            if($forDisplay) {
                if($this->isAutoImport()) {
                    $forDisplay = false; // triggers save of imported email
                }
            }

            if(!$forDisplay) {
                $email->save();

                $email->new_with_id = false; // to allow future saves by UPDATE, instead of INSERT
                ////	ASSIGN APPROPRIATE ATTRIBUTES TO NEW EMAIL OBJECT
                ///////////////////////////////////////////////////////////////////

                ///////////////////////////////////////////////////////////////////
                ////	LINK APPROPRIATE BEANS TO NEWLY SAVED EMAIL
                //$contactAddr = $this->handleLinking($email);
                ////	END LINK APPROPRIATE BEANS TO NEWLY SAVED EMAIL
                ///////////////////////////////////////////////////////////////////

                ///////////////////////////////////////////////////////////////////
                ////	MAILBOX TYPE HANDLING
                $this->handleMailboxType($email, $header);
                ////	END MAILBOX TYPE HANDLING
                ///////////////////////////////////////////////////////////////////

                ///////////////////////////////////////////////////////////////////
                ////	SEND AUTORESPONSE
                if(!empty($email->reply_to_email)) {
                    $contactAddr = $email->reply_to_email;
                } else {
                    $contactAddr = $email->from_addr;
                }
                if (!$this->isMailBoxTypeCreateCase()) {
                    $this->handleAutoresponse($email, $contactAddr);
                }
                ////	END SEND AUTORESPONSE
                ///////////////////////////////////////////////////////////////////
                ////	END IMPORT ONE EMAIL
                ///////////////////////////////////////////////////////////////////
            }
        } else {
            // only log if not POP3; pop3 iterates through ALL mail
            if($this->protocol != 'pop3') {
                $GLOBALS['log']->info("InboundEmail found a duplicate email: ".$header->message_id);
                //echo "This email has already been imported";
            }
            return false;
        }
        ////	END DUPLICATE CHECK
        ///////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////
        ////	DEAL WITH THE MAILBOX
        if(!$forDisplay) {
            /*$r = imap_setflag_full($this->conn, $msgNo, '\\SEEN');

            // if delete_seen, mark msg as deleted
            if($this->delete_seen == 1  && !$forDisplay) {
                $GLOBALS['log']->info("INBOUNDEMAIL: delete_seen == 1 - deleting email");
                imap_setflag_full($this->conn, $msgNo, '\\DELETED');
            }*/
        } else {
            // for display - don't touch server files?
            //imap_setflag_full($this->conn, $msgNo, '\\UNSEEN');
        }

        $GLOBALS['log']->debug('********************************* InboundEmail finished import of 1 email: '.$email->name);
        ////	END DEAL WITH THE MAILBOX
        ///////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////
        ////	TO SUPPORT EMAIL 2.0
        $this->email = $email;

        if(empty($this->email->et)) {
            $this->email->email2init();
        }
		if($header->Recent == 'N' || $header->Unseen == 'U') {
            $this->markEmails($uid, "unread");
        } else {
            $this->markEmails($uid, "read");
        }

        //$this->markEmails($uid, "unread");
        return true;
    }

} 