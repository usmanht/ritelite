<?php
// created: 2015-08-15 12:23:41
$unified_search_modules_display = array (
  'Accounts' => 
  array (
    'visible' => true,
  ),
  'Contacts' => 
  array (
    'visible' => true,
  ),
  'Prospects' => 
  array (
    'visible' => true,
  ),
  'Leads' => 
  array (
    'visible' => true,
  ),
  'Opportunities' => 
  array (
    'visible' => true,
  ),
  'AOS_Quotes' => 
  array (
    'visible' => true,
  ),
  'AOS_Invoices' => 
  array (
    'visible' => true,
  ),
  'Calls' => 
  array (
    'visible' => true,
  ),
  'Campaigns' => 
  array (
    'visible' => true,
  ),
  'Cases' => 
  array (
    'visible' => true,
  ),
  'Meetings' => 
  array (
    'visible' => true,
  ),
  'Notes' => 
  array (
    'visible' => true,
  ),
  'Tasks' => 
  array (
    'visible' => true,
  ),
  'AOS_Contracts' => 
  array (
    'visible' => true,
  ),
  'Project' => 
  array (
    'visible' => true,
  ),
  'ProjectTask' => 
  array (
    'visible' => true,
  ),
  'Documents' => 
  array (
    'visible' => true,
  ),
  'AOS_PDF_Templates' => 
  array (
    'visible' => false,
  ),
  'AOS_Product_Categories' => 
  array (
    'visible' => false,
  ),
  'AOW_Processed' => 
  array (
    'visible' => false,
  ),
  'AOW_WorkFlow' => 
  array (
    'visible' => false,
  ),
  'Bugs' => 
  array (
    'visible' => false,
  ),
  'Calls_Reschedule' => 
  array (
    'visible' => false,
  ),
  'ProspectLists' => 
  array (
    'visible' => false,
  ),
);