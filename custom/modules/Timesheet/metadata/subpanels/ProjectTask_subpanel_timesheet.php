<?php
// created: 2015-02-11 10:19:25
$subpanel_layout['list_fields'] = array (
  'assigned_user_name' => 
  array (
    'name' => 'assigned_user_name',
    'vname' => 'LBL_LIST_EMPLOYEE',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '10%',
    'default' => true,
  ),
  'description' => 
  array (
    'type' => 'text',
    'vname' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => true,
  ),
  'actual' => 
  array (
    'vname' => 'LBL_LIST_ACTUAL',
    'width' => '15%',
    'default' => true,
  ),
  'date_booked' => 
  array (
    'vname' => 'LBL_LIST_DATE_BOOKED',
    'width' => '15%',
    'default' => true,
  ),
  'status' => 
  array (
    'vname' => 'LBL_LIST_STATUS',
    'width' => '15%',
    'default' => true,
  ),
  'billable' => 
  array (
    'vname' => 'LBL_LIST_BILLABLE',
    'width' => '15%',
    'default' => true,
  ),
  'edit_button' => 
  array (
    'widget_class' => 'SubPanelEditButton',
    'width' => '4%',
    'default' => true,
  ),
);