<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2014-01-07 15:54:30
$layout_defs["RL_SalesHistory"]["subpanel_setup"]['rl_saleshistory_rl_saleshistoryitems_1'] = array (
  'order' => 100,
  'module' => 'RL_SalesHistoryItems',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_RL_SALESHISTORY_RL_SALESHISTORYITEMS_1_FROM_RL_SALESHISTORYITEMS_TITLE',
  'get_subpanel_data' => 'rl_saleshistory_rl_saleshistoryitems_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>