<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2014-01-07 15:01:07
$dictionary["RL_SalesHistory"]["fields"]["securitygroups_rl_saleshistory_1"] = array (
  'name' => 'securitygroups_rl_saleshistory_1',
  'type' => 'link',
  'relationship' => 'securitygroups_rl_saleshistory_1',
  'source' => 'non-db',
  'module' => 'SecurityGroups',
  'bean_name' => 'SecurityGroup',
  'vname' => 'LBL_SECURITYGROUPS_RL_SALESHISTORY_1_FROM_SECURITYGROUPS_TITLE',
  'id_name' => 'securitygroups_rl_saleshistory_1securitygroups_ida',
);
$dictionary["RL_SalesHistory"]["fields"]["securitygroups_rl_saleshistory_1_name"] = array (
  'name' => 'securitygroups_rl_saleshistory_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_SECURITYGROUPS_RL_SALESHISTORY_1_FROM_SECURITYGROUPS_TITLE',
  'save' => true,
  'id_name' => 'securitygroups_rl_saleshistory_1securitygroups_ida',
  'link' => 'securitygroups_rl_saleshistory_1',
  'table' => 'securitygroups',
  'module' => 'SecurityGroups',
  'rname' => 'name',
);
$dictionary["RL_SalesHistory"]["fields"]["securitygroups_rl_saleshistory_1securitygroups_ida"] = array (
  'name' => 'securitygroups_rl_saleshistory_1securitygroups_ida',
  'type' => 'link',
  'relationship' => 'securitygroups_rl_saleshistory_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_SECURITYGROUPS_RL_SALESHISTORY_1_FROM_RL_SALESHISTORY_TITLE',
);


// created: 2014-01-07 17:22:27
$dictionary["RL_SalesHistory"]["fields"]["accounts_rl_saleshistory_1"] = array (
  'name' => 'accounts_rl_saleshistory_1',
  'type' => 'link',
  'relationship' => 'accounts_rl_saleshistory_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_RL_SALESHISTORY_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_rl_saleshistory_1accounts_ida',
);
$dictionary["RL_SalesHistory"]["fields"]["accounts_rl_saleshistory_1_name"] = array (
  'name' => 'accounts_rl_saleshistory_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_RL_SALESHISTORY_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_rl_saleshistory_1accounts_ida',
  'link' => 'accounts_rl_saleshistory_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["RL_SalesHistory"]["fields"]["accounts_rl_saleshistory_1accounts_ida"] = array (
  'name' => 'accounts_rl_saleshistory_1accounts_ida',
  'type' => 'link',
  'relationship' => 'accounts_rl_saleshistory_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_RL_SALESHISTORY_1_FROM_RL_SALESHISTORY_TITLE',
);


// created: 2014-01-07 14:55:50
$dictionary["RL_SalesHistory"]["fields"]["users_rl_saleshistory_1"] = array (
  'name' => 'users_rl_saleshistory_1',
  'type' => 'link',
  'relationship' => 'users_rl_saleshistory_1',
  'source' => 'non-db',
  'module' => 'Users',
  'bean_name' => 'User',
  'vname' => 'LBL_USERS_RL_SALESHISTORY_1_FROM_USERS_TITLE',
  'id_name' => 'users_rl_saleshistory_1users_ida',
);
$dictionary["RL_SalesHistory"]["fields"]["users_rl_saleshistory_1_name"] = array (
  'name' => 'users_rl_saleshistory_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_USERS_RL_SALESHISTORY_1_FROM_USERS_TITLE',
  'save' => true,
  'id_name' => 'users_rl_saleshistory_1users_ida',
  'link' => 'users_rl_saleshistory_1',
  'table' => 'users',
  'module' => 'Users',
  'rname' => 'name',
);
$dictionary["RL_SalesHistory"]["fields"]["users_rl_saleshistory_1users_ida"] = array (
  'name' => 'users_rl_saleshistory_1users_ida',
  'type' => 'link',
  'relationship' => 'users_rl_saleshistory_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_USERS_RL_SALESHISTORY_1_FROM_RL_SALESHISTORY_TITLE',
);


// created: 2014-01-07 15:54:30
$dictionary["RL_SalesHistory"]["fields"]["rl_saleshistory_rl_saleshistoryitems_1"] = array (
  'name' => 'rl_saleshistory_rl_saleshistoryitems_1',
  'type' => 'link',
  'relationship' => 'rl_saleshistory_rl_saleshistoryitems_1',
  'source' => 'non-db',
  'module' => 'RL_SalesHistoryItems',
  'bean_name' => 'RL_SalesHistoryItems',
  'side' => 'right',
  'vname' => 'LBL_RL_SALESHISTORY_RL_SALESHISTORYITEMS_1_FROM_RL_SALESHISTORYITEMS_TITLE',
);

?>