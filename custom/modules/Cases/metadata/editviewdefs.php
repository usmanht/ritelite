<?php
$viewdefs ['Cases'] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'LBL_CASE_INFORMATION' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_PANEL_ASSIGNMENT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'lbl_case_information' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'case_number',
            'type' => 'readonly',
          ),
          1 => '',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'date_entered',
            'comment' => 'Date record created',
            'label' => 'LBL_DATE_ENTERED',
          ),
          1 => 
          array (
            'name' => 'created_by_name',
            'label' => 'LBL_CREATED',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'required_closure_date_c',
            'label' => 'LBL_REQUIRED_CLOSURE_DATE',
          ),
          1 => 'assigned_user_name',
        ),
        3 => 
        array (
          0 => 'priority',
          1 => 
          array (
            'name' => 'state',
            'comment' => 'The state of the case (i.e. open/closed)',
            'label' => 'LBL_STATE',
          ),
        ),
        4 => 
        array (
          0 => 'status',
          1 => 'account_name',
        ),
        5 => 
        array (
          0 => 'type',
          1 => 
          array (
            'name' => 'quantity_c',
            'label' => 'LBL_QUANTITY',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'product_category_c',
            'studio' => 'visible',
            'label' => 'LBL_PRODUCT_CATEGORY',
          ),
          1 => 
          array (
            'name' => 'aos_products_cases_1_name',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'name',
            'displayParams' => 
            array (
            ),
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'description',
            'nl2br' => true,
          ),
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'resolution',
            'nl2br' => true,
          ),
        ),
        10 => 
        array (
          0 => 
          array (
            'name' => 'suggestion_box',
            'label' => 'LBL_SUGGESTION_BOX',
          ),
        ),
        11 => 
        array (
          0 => 
          array (
            'name' => 'generate_access_return_c',
            'label' => 'LBL_GENERATE_ACCESS_RETURN',
          ),
          1 => 
          array (
            'name' => 'access_returns_number_c',
            'label' => 'LBL_ACCESS_RETURNS_NUMBER',
          ),
        ),
        12 => 
        array (
          0 => 
          array (
            'name' => 'create_workshop_repair_quote_c',
            'label' => 'LBL_CREATE_WORKSHOP_REPAIR_QUOTE',
          ),
        ),
        13 => 
        array (
          0 => 
          array (
            'name' => 'update_text',
            'studio' => 'visible',
            'label' => 'LBL_UPDATE_TEXT',
          ),
          1 => 
          array (
            'name' => 'internal',
            'studio' => 'visible',
            'label' => 'LBL_INTERNAL',
          ),
        ),
        14 => 
        array (
          0 => 
          array (
            'name' => 'aop_case_updates_threaded',
            'studio' => 'visible',
            'label' => 'LBL_AOP_CASE_UPDATES_THREADED',
          ),
        ),
        15 => 
        array (
          0 => 
          array (
            'name' => 'received_date_time_c',
            'label' => 'LBL_RECEIVED_DATE_TIME',
          ),
          1 => 
          array (
            'name' => 'arrival_method_c',
            'studio' => 'visible',
            'label' => 'LBL_ARRIVAL_METHOD',
          ),
        ),
      ),
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'parcel_pallet_quantity_c',
            'label' => 'LBL_PARCEL_PALLET_QUANTITY',
          ),
          1 => 
          array (
            'name' => 'freight_company_c',
            'studio' => 'visible',
            'label' => 'LBL_FREIGHT_COMPANY',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'return_freight_type_c',
            'studio' => 'visible',
            'label' => 'LBL_RETURN_FREIGHT_TYPE',
          ),
        ),
      ),
      'LBL_PANEL_ASSIGNMENT' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'case_update_form',
            'studio' => 'visible',
          ),
          1 => 
          array (
            'name' => 'case_attachments_display',
            'studio' => 'visible',
            'label' => 'LBL_CASE_ATTACHMENTS_DISPLAY',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'date_modified',
            'comment' => 'Date record last modified',
            'label' => 'LBL_DATE_MODIFIED',
          ),
          1 => 
          array (
            'name' => 'modified_by_name',
            'label' => 'LBL_MODIFIED_NAME',
          ),
        ),
      ),
    ),
  ),
);
?>
