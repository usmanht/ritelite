<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2014-05-02 08:59:17
$dictionary['AOS_Products_Quotes']['fields']['parent_id']['importable']='true';
$dictionary['AOS_Products_Quotes']['fields']['parent_id']['merge_filter']='disabled';
$dictionary['AOS_Products_Quotes']['fields']['parent_id']['reportable']=true;
$dictionary['AOS_Products_Quotes']['fields']['parent_id']['len']=255;

 

 // created: 2014-05-02 08:59:17
$dictionary['AOS_Products_Quotes']['fields']['parent_name']['options']='parent_type_display';
$dictionary['AOS_Products_Quotes']['fields']['parent_name']['importable']='true';
$dictionary['AOS_Products_Quotes']['fields']['parent_name']['merge_filter']='disabled';
$dictionary['AOS_Products_Quotes']['fields']['parent_name']['reportable']=true;

 

 // created: 2014-05-02 08:59:17
$dictionary['AOS_Products_Quotes']['fields']['parent_type']['importable']='true';
$dictionary['AOS_Products_Quotes']['fields']['parent_type']['merge_filter']='disabled';
$dictionary['AOS_Products_Quotes']['fields']['parent_type']['reportable']=true;
$dictionary['AOS_Products_Quotes']['fields']['parent_type']['len']=255;

 

// created: 2014-04-18 11:03:26
$dictionary["AOS_Products_Quotes"]["fields"]["product_list_price_c"] = array (
        'required' => false,
        'name' => 'product_list_price_c',
        'vname' => 'LBL_PRODUCT_LIST_PRICE_C',
        'type' => 'currency',
        'massupdate' => 0,
        'comments' => '',
        'help' => '',
        'importable' => 'true',
        'duplicate_merge' => 'disabled',
        'duplicate_merge_dom_value' => '0',
        'audited' => 1,
        'reportable' => true,
        'len' => '26,6',
);

 $dictionary["AOS_Products_Quotes"]["fields"]["polink_c"] = array ( 
			'required' => false, 
			'name' => 'polink_c', 
			'vname' => 'LBL_POLINK', 
			'type' => 'varchar', 
			'massupdate' => '0', 
			'default' => '', '
			no_default' => false, 
			'comments' => '', 
			'help' => '', 
			'importable' => 'true', 
			'duplicate_merge' => 'disabled', 
			'duplicate_merge_dom_value' => '0', 
			'audited' => true, 
			'reportable' => true, 
			'unified_search' => false, 
			'merge_filter' => 'disabled', 
			'len' => '255', 
			'size' => '20', 
	);
?>