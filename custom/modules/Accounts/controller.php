<?php

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class CustomAccountsController extends SugarController {

    function action_get_shipping_address(){

        $id = $_REQUEST['account_id'];

        $account = new Account();
        $account = $account->retrieve($id);
        $address = $account->shipping_address_street.'|'.$account->shipping_address_city.'|'.$account->shipping_address_state.'|'.$account->shipping_address_postalcode.'|'.$account->shipping_address_country.'|'.$account->name.'|'.$account->id.'|'.$account->sage_account_ref_c;
        echo $address;
        die();
    }

}