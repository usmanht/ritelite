<?php
/**
 * strip_line_breaks.php
 * @author SalesAgility (Andrew Mclaughlan) <support@salesagility.com>
 * Date: 16/04/15
 * Comments
 */

class strip_line_breaks1
{
    function strip($bean, $event, $arguments)
    {
        $bean->billing_address_street = str_replace(array("\r", "\n"), "", $bean->billing_address_street);
        $bean->shipping_address_street = str_replace(array("\r", "\n"), "", $bean->shipping_address_street);
    }
}