<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2014-05-07 16:14:02
$dictionary["Account"]["fields"]["accounts_aos_invoices_1"] = array (
  'name' => 'accounts_aos_invoices_1',
  'type' => 'link',
  'relationship' => 'accounts_aos_invoices_1',
  'source' => 'non-db',
  'module' => 'AOS_Invoices',
  'bean_name' => 'AOS_Invoices',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_AOS_INVOICES_1_FROM_AOS_INVOICES_TITLE',
);


// created: 2014-03-25 16:57:28
$dictionary["Account"]["fields"]["accounts_aos_quotes_1"] = array (
  'name' => 'accounts_aos_quotes_1',
  'type' => 'link',
  'relationship' => 'accounts_aos_quotes_1',
  'source' => 'non-db',
  'module' => 'AOS_Quotes',
  'bean_name' => 'AOS_Quotes',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_AOS_QUOTES_1_FROM_AOS_QUOTES_TITLE',
);


// created: 2014-01-07 17:22:27
$dictionary["Account"]["fields"]["accounts_rl_saleshistory_1"] = array (
  'name' => 'accounts_rl_saleshistory_1',
  'type' => 'link',
  'relationship' => 'accounts_rl_saleshistory_1',
  'source' => 'non-db',
  'module' => 'RL_SalesHistory',
  'bean_name' => 'RL_SalesHistory',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_RL_SALESHISTORY_1_FROM_RL_SALESHISTORY_TITLE',
);


// created: 2014-05-07 16:09:41
$dictionary["Account"]["fields"]["accounts_rl_sales_order_line_items_1"] = array (
  'name' => 'accounts_rl_sales_order_line_items_1',
  'type' => 'link',
  'relationship' => 'accounts_rl_sales_order_line_items_1',
  'source' => 'non-db',
  'module' => 'RL_Sales_Order_Line_Items',
  'bean_name' => 'RL_Sales_Order_Line_Items',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_RL_SALES_ORDER_LINE_ITEMS_1_FROM_RL_SALES_ORDER_LINE_ITEMS_TITLE',
);


// created: 2014-05-07 16:11:42
$dictionary["Account"]["fields"]["accounts_rl_sales_order_line_items_2"] = array (
  'name' => 'accounts_rl_sales_order_line_items_2',
  'type' => 'link',
  'relationship' => 'accounts_rl_sales_order_line_items_2',
  'source' => 'non-db',
  'module' => 'RL_Sales_Order_Line_Items',
  'bean_name' => 'RL_Sales_Order_Line_Items',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_RL_SALES_ORDER_LINE_ITEMS_2_FROM_RL_SALES_ORDER_LINE_ITEMS_TITLE',
);


$dictionary["Account"]["fields"]["aos_quotes"] = array (
  'name' => 'aos_quotes',
    'type' => 'link',
    'relationship' => 'account_aos_quotes',
    'module'=>'AOS_Quotes',
    'bean_name'=>'AOS_Quotes',
    'source'=>'non-db',
);

$dictionary["Account"]["relationships"]["account_aos_quotes"] = array (
	'lhs_module'=> 'Accounts', 
	'lhs_table'=> 'accounts', 
	'lhs_key' => 'id',
	'rhs_module'=> 'AOS_Quotes', 
	'rhs_table'=> 'aos_quotes', 
	'rhs_key' => 'billing_account_id',
	'relationship_type'=>'one-to-many',
);

$dictionary["Account"]["fields"]["aos_invoices"] = array (
  'name' => 'aos_invoices',
    'type' => 'link',
    'relationship' => 'account_aos_invoices',
    'module'=>'AOS_Invoices',
    'bean_name'=>'AOS_Invoices',
    'source'=>'non-db',
);

$dictionary["Account"]["relationships"]["account_aos_invoices"] = array (
	'lhs_module'=> 'Accounts', 
	'lhs_table'=> 'accounts', 
	'lhs_key' => 'id',
	'rhs_module'=> 'AOS_Invoices', 
	'rhs_table'=> 'aos_invoices', 
	'rhs_key' => 'billing_account_id',
	'relationship_type'=>'one-to-many',
);

$dictionary["Account"]["fields"]["aos_contracts"] = array (
  'name' => 'aos_contracts',
    'type' => 'link',
    'relationship' => 'account_aos_contracts',
    'module'=>'AOS_Contracts',
    'bean_name'=>'AOS_Contracts',
    'source'=>'non-db',
);

$dictionary["Account"]["relationships"]["account_aos_contracts"] = array (
	'lhs_module'=> 'Accounts', 
	'lhs_table'=> 'accounts', 
	'lhs_key' => 'id',
	'rhs_module'=> 'AOS_Contracts', 
	'rhs_table'=> 'aos_contracts', 
	'rhs_key' => 'contract_account_id',
	'relationship_type'=>'one-to-many',
);



$dictionary['Account']['fields']['SecurityGroups'] = array (
  	'name' => 'SecurityGroups',
    'type' => 'link',
	'relationship' => 'securitygroups_accounts',
	'module'=>'SecurityGroups',
	'bean_name'=>'SecurityGroup',
    'source'=>'non-db',
	'vname'=>'LBL_SECURITYGROUPS',
);






 // created: 2014-01-07 11:39:15
$dictionary['Account']['fields']['account_status_c']['labelValue']='Status';

 

 // created: 2015-03-03 11:47:33
$dictionary['Account']['fields']['billing_address_city']['comments']='The city used for billing address';
$dictionary['Account']['fields']['billing_address_city']['merge_filter']='disabled';
$dictionary['Account']['fields']['billing_address_city']['unified_search']=true;

 

 // created: 2015-03-03 11:47:40
$dictionary['Account']['fields']['billing_address_country']['comments']='The country used for the billing address';
$dictionary['Account']['fields']['billing_address_country']['merge_filter']='disabled';
$dictionary['Account']['fields']['billing_address_country']['unified_search']=true;

 

 // created: 2015-03-03 11:47:45
$dictionary['Account']['fields']['billing_address_postalcode']['comments']='The postal code used for billing address';
$dictionary['Account']['fields']['billing_address_postalcode']['merge_filter']='disabled';
$dictionary['Account']['fields']['billing_address_postalcode']['unified_search']=true;

 

 // created: 2015-03-03 11:47:49
$dictionary['Account']['fields']['billing_address_state']['comments']='The state used for billing address';
$dictionary['Account']['fields']['billing_address_state']['merge_filter']='disabled';
$dictionary['Account']['fields']['billing_address_state']['unified_search']=true;

 

 // created: 2015-03-03 11:47:55
$dictionary['Account']['fields']['billing_address_street']['comments']='The street address used for billing address';
$dictionary['Account']['fields']['billing_address_street']['merge_filter']='disabled';
$dictionary['Account']['fields']['billing_address_street']['unified_search']=true;

 

 // created: 2014-03-21 17:27:07
$dictionary['Account']['fields']['company_type_c']['labelValue']='Company Type';

 

 // created: 2014-03-21 17:28:22
$dictionary['Account']['fields']['group_c']['labelValue']='Group ';

 

 // created: 2016-03-04 15:12:29

 

 // created: 2016-03-04 15:12:27

 

 // created: 2016-03-04 15:12:23

 

 // created: 2016-03-04 15:12:19

 

 // created: 2015-07-06 15:24:40
$dictionary['Account']['fields']['orderconfirmation_c']['labelValue']='Order Confirmation';

 

 // created: 2014-12-10 14:44:42
$dictionary['Account']['fields']['ownership']['merge_filter']='disabled';
$dictionary['Account']['fields']['ownership']['unified_search']=true;

 

 // created: 2014-12-10 11:20:02
$dictionary['Account']['fields']['phone_office']['comments']='The office phone number';
$dictionary['Account']['fields']['phone_office']['merge_filter']='disabled';

 

 // created: 2014-12-19 16:18:24
$dictionary['Account']['fields']['sage_account_ref_c']['labelValue']='Sage Account Ref';
$dictionary['Account']['fields']['sage_account_ref_c']['unified_search']='1';

 

 // created: 2014-12-19 16:18:07
$dictionary['Account']['fields']['searchlabel_c']['labelValue']='Search Label';
$dictionary['Account']['fields']['searchlabel_c']['unified_search']='1';

 

 // created: 2015-03-03 11:47:59
$dictionary['Account']['fields']['shipping_address_city']['comments']='The city used for the shipping address';
$dictionary['Account']['fields']['shipping_address_city']['merge_filter']='disabled';
$dictionary['Account']['fields']['shipping_address_city']['unified_search']=true;

 

 // created: 2015-03-03 11:48:05
$dictionary['Account']['fields']['shipping_address_country']['comments']='The country used for the shipping address';
$dictionary['Account']['fields']['shipping_address_country']['merge_filter']='disabled';
$dictionary['Account']['fields']['shipping_address_country']['unified_search']=true;

 

 // created: 2015-03-03 11:48:08
$dictionary['Account']['fields']['shipping_address_postalcode']['comments']='The zip code used for the shipping address';
$dictionary['Account']['fields']['shipping_address_postalcode']['merge_filter']='disabled';
$dictionary['Account']['fields']['shipping_address_postalcode']['unified_search']=true;

 

 // created: 2015-03-03 11:48:18
$dictionary['Account']['fields']['shipping_address_state']['comments']='The state used for the shipping address';
$dictionary['Account']['fields']['shipping_address_state']['merge_filter']='disabled';
$dictionary['Account']['fields']['shipping_address_state']['unified_search']=true;

 

 // created: 2015-03-03 11:48:22
$dictionary['Account']['fields']['shipping_address_street']['comments']='The street address used for for shipping purposes';
$dictionary['Account']['fields']['shipping_address_street']['merge_filter']='disabled';
$dictionary['Account']['fields']['shipping_address_street']['unified_search']=true;

 

 // created: 2014-12-10 14:54:34
$dictionary['Account']['fields']['sic_code']['comments']='SIC code of the account';
$dictionary['Account']['fields']['sic_code']['merge_filter']='disabled';
$dictionary['Account']['fields']['sic_code']['unified_search']=false;

 

 // created: 2014-12-10 16:44:40
$dictionary['Account']['fields']['test_field_c']['labelValue']='LBL_TEST_FIELD';
$dictionary['Account']['fields']['test_field_c']['unified_search']='1';

 

 // created: 2014-12-10 15:50:17
$dictionary['Account']['fields']['test_int_c']['labelValue']='test int';
$dictionary['Account']['fields']['test_int_c']['unified_search']='1';

 
?>