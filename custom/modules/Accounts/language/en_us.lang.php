<?php
// created: 2015-07-06 15:24:40
$mod_strings = array (
  'LBL_SECURITYGROUPS_SUBPANEL_TITLE' => 'Security Groups',
  'LBL_ACCOUNT_STATUS' => 'Status',
  'LBL_ACCOUNTS_RL_SALESHISTORY_1_FROM_RL_SALESHISTORY_TITLE' => 'Sales history',
  'LBL_SAGE_ACCOUNT_REF' => 'Sage Account Ref',
  'LBL_COMPANY_TYPE' => 'Company Type',
  'LBL_GROUP_C' => 'Group ',
  'AOS_Invoices' => 'Sales Orders (Billing Account)',
  'LBL_ACCOUNTS_RL_SALES_ORDER_LINE_ITEMS_2_FROM_RL_SALES_ORDER_LINE_ITEMS_TITLE' => 'Sales Order Line Items (Shipping)',
  'LBL_ACCOUNTS_RL_SALES_ORDER_LINE_ITEMS_1_FROM_RL_SALES_ORDER_LINE_ITEMS_TITLE' => 'Sales Order Line Items (Billing)',
  'LBL_ACCOUNTS_AOS_INVOICES_1_FROM_AOS_INVOICES_TITLE' => 'Sales Orders Shipping',
  'LBL_ACCOUNTS_AOS_QUOTES_1_FROM_AOS_QUOTES_TITLE' => 'Quotes Shipping ',
  'AOS_Quotes' => 'Quotes (Billing Acount)',
  'LBL_DOCUMENTS_SUBPANEL_TITLE' => 'Documents',
  'LBL_CONTACTS_SUBPANEL_TITLE' => 'Contacts',
  'LBL_OPPORTUNITIES_SUBPANEL_TITLE' => 'Opportunities',
  'LBL_CAMPAIGNS' => 'Campaigns',
  'AOS_Contracts' => 'Contracts',
  'LBL_SEARCHLABEL' => 'Search Label',
  'LBL_ASSIGNED_TO' => 'Salesperson',
  'LBL_LIST_ASSIGNED_USER' => 'Salesperson',
  'LBL_ASSIGNED_TO_NAME' => 'Salesperson',
  'LBL_LEADS_SUBPANEL_TITLE' => 'Leads',
  'LBL_BILLING_ADDRESS_CITY' => 'Billing City:',
  'LBL_BILLING_ADDRESS_COUNTRY' => 'Billing Country:',
  'LBL_BILLING_ADDRESS_POSTALCODE' => 'Postal Code',
  'LBL_BILLING_ADDRESS_STATE' => 'Billing State:',
  'LBL_BILLING_ADDRESS_STREET' => 'Billing Street:',
  'LBL_SHIPPING_ADDRESS_CITY' => 'Shipping City:',
  'LBL_SHIPPING_ADDRESS_COUNTRY' => 'Shipping Country:',
  'LBL_SHIPPING_ADDRESS_POSTALCODE' => 'Shipping Postal Code:',
  'LBL_SHIPPING_ADDRESS_STATE' => 'Shipping State:',
  'LBL_SHIPPING_ADDRESS_STREET' => 'Shipping Street:',
  'LBL_CREATE_SALES_ORDER' => 'Create Sales Order',
  'LBL_CREATE_QUOTE' => 'Create Quote',
  'LBL_STATE' => 'County',
  'LBL_EDITVIEW_PANEL1' => 'Google Maps',
  'LBL_ORDERCONFIRMATION' => 'Order Confirmation',
);