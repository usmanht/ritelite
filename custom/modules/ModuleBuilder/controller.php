<?php
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

require_once ('modules/ModuleBuilder/controller.php') ;
//require_once ('modules/ModuleBuilder/parsers/ParserFactory.php') ;
//require_once ('modules/ModuleBuilder/Module/StudioModuleFactory.php');
//require_once 'modules/ModuleBuilder/parsers/constants.php' ;

class CustomModuleBuilderController extends ModuleBuilderController
{
    function action_SaveField ()
    {
        require_once ('custom/modules/DynamicFields/FieldCases.php') ;
        $field = get_widget_custom ( $_REQUEST [ 'type' ] ) ;
        $_REQUEST [ 'name' ] = trim ( $_REQUEST [ 'name' ] ) ;

        $field->populateFromPost () ;

        if (!isset ( $_REQUEST [ 'view_package' ] ))
        {
            require_once ('modules/DynamicFields/DynamicField.php') ;
            if (! empty ( $_REQUEST [ 'view_module' ] ))
            {
                $module = $_REQUEST [ 'view_module' ] ;
                if ( $module == 'Employees' ) {
                    $module = 'Users';
                }

                $bean = BeanFactory::getBean($module);
                if(!empty($bean))
                {
	                $field_defs = $bean->field_defs;
	                if(isset($field_defs[$field->name. '_c']))
	                {
						$GLOBALS['log']->error($GLOBALS['mod_strings']['ERROR_ALREADY_EXISTS'] . '[' . $field->name . ']');
						sugar_die($GLOBALS['mod_strings']['ERROR_ALREADY_EXISTS']);
	                }
                }

                $df = new DynamicField ( $module ) ;
                $class_name = $GLOBALS [ 'beanList' ] [ $module ] ;
                require_once ($GLOBALS [ 'beanFiles' ] [ $class_name ]) ;
                $mod = new $class_name ( ) ;
                $df->setup ( $mod ) ;

                $field->save ( $df ) ;
                $this->action_SaveLabel () ;
                include_once ('modules/Administration/QuickRepairAndRebuild.php') ;
        		global $mod_strings;
                $mod_strings['LBL_ALL_MODULES'] = 'all_modules';
                require_once('ModuleInstall/ModuleInstaller.php');
                $mi = new ModuleInstaller();
                $mi->silent = true;
                $mi->rebuild_extensions();
                $repair = new RepairAndClear();

		        $repair->repairAndClearAll(array('rebuildExtensions','clearSearchCache', 'clearVardefs', 'clearTpls'), array($class_name), true, false);
                if ( $module == 'Users' ) {
                    $repair->repairAndClearAll(array('rebuildExtensions','clearSearchCache', 'clearVardefs', 'clearTpls'), array('Employee'), true, false);
                    
                }

                //#28707 ,clear all the js files in cache
		        $repair->module_list = array();
		        $repair->clearJsFiles();
            }
        } else
        {
            $mb = new ModuleBuilder ( ) ;
            $module = & $mb->getPackageModule ( $_REQUEST [ 'view_package' ], $_REQUEST [ 'view_module' ] ) ;
            $field->save ( $module ) ;
            $module->mbvardefs->save () ;
            // get the module again to refresh the labels we might have saved with the $field->save (e.g., for address fields)
            $module = & $mb->getPackageModule ( $_REQUEST [ 'view_package' ], $_REQUEST [ 'view_module' ] ) ;
            if (isset ( $_REQUEST [ 'label' ] ) && isset ( $_REQUEST [ 'labelValue' ] ))
                $module->setLabel ( $GLOBALS [ 'current_language' ], $_REQUEST [ 'label' ], $_REQUEST [ 'labelValue' ] ) ;
            $module->save();
        }
        $this->view = 'modulefields' ;
    }

    function action_saveSugarField ()
    {
    	global $mod_strings;
    	require_once ('custom/modules/DynamicFields/FieldCases.php') ;

        $field = get_widget_custom ( $_REQUEST [ 'type' ] ) ;
        $_REQUEST [ 'name' ] = trim ( $_POST [ 'name' ] ) ;

        $field->populateFromPost () ;
        require_once ('custom/modules/ModuleBuilder/parsers/StandardField.php') ;
        $module = $_REQUEST [ 'view_module' ] ;

        // Need to map Employees -> Users
        if ( $module=='Employees') {
            $module = 'Users';
        }

        $df = new CustomStandardField ( $module ) ;
        $mod = BeanFactory::getBean($module);
        $class_name = $GLOBALS [ 'beanList' ] [ $module ] ;
        $df->setup ( $mod ) ;

        $field->module = $mod;
        $field->save ( $df ) ;
        $this->action_SaveLabel () ;

        $MBmodStrings = $mod_strings;
        $GLOBALS [ 'mod_strings' ] = return_module_language ( '', 'Administration' ) ;

       	include_once ('modules/Administration/QuickRepairAndRebuild.php') ;
        $GLOBALS [ 'mod_strings' ]['LBL_ALL_MODULES'] = 'all_modules';
        $_REQUEST['execute_sql'] = true;

        require_once('ModuleInstall/ModuleInstaller.php');
		$mi = new ModuleInstaller();
        $mi->silent = true;
		$mi->rebuild_extensions();

        $repair = new RepairAndClear();
        $repair->repairAndClearAll(array('clearSearchCache','clearVardefs', 'clearTpls'), array($class_name), true, false);
        //#28707 ,clear all the js files in cache
        $repair->module_list = array();
        $repair->clearJsFiles();

        // now clear the cache so that the results are immediately visible
        include_once ('include/TemplateHandler/TemplateHandler.php') ;
        TemplateHandler::clearCache ( $module ) ;
        if ( $module == 'Users' ) {
            TemplateHandler::clearCache('Employees');
        }

        $GLOBALS [ 'mod_strings' ] = $MBmodStrings;
    }
}
?>
