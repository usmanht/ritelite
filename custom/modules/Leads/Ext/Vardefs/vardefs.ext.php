<?php 
 //WARNING: The contents of this file are auto-generated



$dictionary['Lead']['fields']['SecurityGroups'] = array (
  	'name' => 'SecurityGroups',
    'type' => 'link',
	'relationship' => 'securitygroups_leads',
	'module'=>'SecurityGroups',
	'bean_name'=>'SecurityGroup',
    'source'=>'non-db',
	'vname'=>'LBL_SECURITYGROUPS',
);






 // created: 2014-12-19 16:19:41
$dictionary['Lead']['fields']['accesscontactid_c']['labelValue']='Access Contact ID';
$dictionary['Lead']['fields']['accesscontactid_c']['unified_search']='1';

 

 // created: 2018-05-09 17:46:16
$dictionary['Lead']['fields']['account_type_dom_c']['inline_edit']='1';
$dictionary['Lead']['fields']['account_type_dom_c']['labelValue']='Type';

 

 // created: 2015-03-03 11:48:30
$dictionary['Lead']['fields']['alt_address_city']['comments']='City for alternate address';
$dictionary['Lead']['fields']['alt_address_city']['merge_filter']='disabled';
$dictionary['Lead']['fields']['alt_address_city']['unified_search']=true;

 

 // created: 2015-03-03 11:48:37
$dictionary['Lead']['fields']['alt_address_country']['comments']='Country for alternate address';
$dictionary['Lead']['fields']['alt_address_country']['merge_filter']='disabled';
$dictionary['Lead']['fields']['alt_address_country']['unified_search']=true;

 

 // created: 2015-03-03 11:48:41
$dictionary['Lead']['fields']['alt_address_postalcode']['comments']='Postal code for alternate address';
$dictionary['Lead']['fields']['alt_address_postalcode']['merge_filter']='disabled';
$dictionary['Lead']['fields']['alt_address_postalcode']['unified_search']=true;

 

 // created: 2015-03-03 11:48:45
$dictionary['Lead']['fields']['alt_address_state']['comments']='State for alternate address';
$dictionary['Lead']['fields']['alt_address_state']['merge_filter']='disabled';
$dictionary['Lead']['fields']['alt_address_state']['unified_search']=true;

 

 // created: 2015-03-03 11:48:49
$dictionary['Lead']['fields']['alt_address_street']['comments']='Street address for alternate address';
$dictionary['Lead']['fields']['alt_address_street']['merge_filter']='disabled';
$dictionary['Lead']['fields']['alt_address_street']['unified_search']=true;

 

 // created: 2014-03-28 13:13:49
$dictionary['Lead']['fields']['companyname_c']['labelValue']='Company Name';

 

 // created: 2018-05-09 17:44:22
$dictionary['Lead']['fields']['gdpr_classification_c']['inline_edit']='1';
$dictionary['Lead']['fields']['gdpr_classification_c']['labelValue']='GDPR Classification';

 

 // created: 2014-03-21 17:48:09
$dictionary['Lead']['fields']['group_c']['labelValue']='Group ';

 

 // created: 2014-06-19 15:11:21
$dictionary['Lead']['fields']['industry_c']['labelValue']='Industry';

 

 // created: 2016-03-04 15:12:57

 

 // created: 2016-03-04 15:12:56

 

 // created: 2016-03-04 15:12:53

 

 // created: 2016-03-04 15:12:51

 

 // created: 2014-03-28 12:36:26
$dictionary['Lead']['fields']['last_name']['comments']='Last name of the contact';
$dictionary['Lead']['fields']['last_name']['importable']='true';
$dictionary['Lead']['fields']['last_name']['merge_filter']='disabled';
$dictionary['Lead']['fields']['last_name']['required']=false;

 

 // created: 2015-08-10 16:46:36
$dictionary['Lead']['fields']['lead_source']['required']=true;
$dictionary['Lead']['fields']['lead_source']['help']='The lead source must be selected to enable Ritelite to use this data for reporting';
$dictionary['Lead']['fields']['lead_source']['comments']='Lead source (ex: Web, print)';
$dictionary['Lead']['fields']['lead_source']['merge_filter']='disabled';

 

 // created: 2017-04-27 08:17:53
$dictionary['Lead']['fields']['notes_c']['inline_edit']='1';
$dictionary['Lead']['fields']['notes_c']['labelValue']='Notes';

 

 // created: 2018-05-07 10:26:38
$dictionary['Lead']['fields']['opt_in_c']['inline_edit']='1';
$dictionary['Lead']['fields']['opt_in_c']['labelValue']='Opt-in';

 

 // created: 2015-03-03 11:48:55
$dictionary['Lead']['fields']['primary_address_city']['comments']='City for primary address';
$dictionary['Lead']['fields']['primary_address_city']['merge_filter']='disabled';
$dictionary['Lead']['fields']['primary_address_city']['unified_search']=true;

 

 // created: 2015-03-03 11:49:00
$dictionary['Lead']['fields']['primary_address_country']['comments']='Country for primary address';
$dictionary['Lead']['fields']['primary_address_country']['merge_filter']='disabled';
$dictionary['Lead']['fields']['primary_address_country']['unified_search']=true;

 

 // created: 2015-03-03 11:49:05
$dictionary['Lead']['fields']['primary_address_postalcode']['comments']='Postal code for primary address';
$dictionary['Lead']['fields']['primary_address_postalcode']['merge_filter']='disabled';
$dictionary['Lead']['fields']['primary_address_postalcode']['unified_search']=true;

 

 // created: 2015-03-03 11:56:08
$dictionary['Lead']['fields']['primary_address_state']['comments']='State for primary address';
$dictionary['Lead']['fields']['primary_address_state']['merge_filter']='disabled';
$dictionary['Lead']['fields']['primary_address_state']['unified_search']=true;

 

 // created: 2014-12-19 16:19:30
$dictionary['Lead']['fields']['searchlabel_c']['labelValue']='Label';
$dictionary['Lead']['fields']['searchlabel_c']['unified_search']='1';

 

 // created: 2015-03-11 14:28:35
$dictionary['Lead']['fields']['type_c']['labelValue']='Type';

 

 // created: 2015-05-15 09:06:31
$dictionary['Lead']['fields']['webref_c']['labelValue']='WebRef';

 

 // created: 2018-05-23 18:16:39
$dictionary['Lead']['fields']['workflow_trigger_date_time_c']['inline_edit']='1';
$dictionary['Lead']['fields']['workflow_trigger_date_time_c']['labelValue']='Workflow Trigger Date/Time';

 
?>