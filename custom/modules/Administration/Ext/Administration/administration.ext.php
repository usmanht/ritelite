<?php 
 //WARNING: The contents of this file are auto-generated


$admin_option_defs = array();
$admin_option_defs['Administration']['aos_settings_link1'] = array(
    'edit',
    'AOS Settings',
    'Change settings for Advanced OpenSales',
    './index.php?module=Administration&action=AOSAdmin'
);
$admin_group_header[] = array(
    'Advanced OpenSales',
    '',
    false,
    $admin_option_defs,
    'Change settings for Advanced OpenSales'
);


/*********************************************************************************
 * This file is part of QuickCRM Mobile Pro.
 * QuickCRM Mobile Pro is a mobile client for SugarCRM
 * 
 * Author : NS-Team (http://www.quickcrm.fr/mobile)
 * All rights (c) 2011 by NS-Team
 *
 * This Version of the QuickCRM Mobile Pro is licensed software and may only be used in 
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * witten consent of NS-Team
 * 
 * You can contact NS-Team at NS-Team - 55 Chemin de Mervilla - 31320 Auzeville - France
 * or via email at quickcrm@ns-team.fr
 * 
 ********************************************************************************/

$admin_option_defs=array();
$admin_option_defs['Administration']['quickcrm_update']= array('Administration','LBL_UPDATE_QUICKCRM_TITLE','LBL_UPDATE_QUICKCRM','./index.php?module=Administration&action=updatequickcrm');


$admin_option_defs['Administration'] = array_merge((array)$admin_group_header[1][3]['Administration'],(array)$admin_option_defs['Administration']);

$admin_group_header[1]= array('LBL_ADMINISTRATION_HOME_TITLE','',false,$admin_option_defs, 'LBL_ADMINISTRATION_HOME_DESC');




/*********************************************************************************
 * This file is part of QuickCRM Mobile CE.
 * QuickCRM Mobile CE is a mobile client for SugarCRM
 *
 * Author : NS-Team (http://www.ns-team.fr)
 *
 * QuickCRM Mobile CE is licensed under GNU General Public License v3 (GPLv3)
 *
 * You can contact NS-Team at NS-Team - 55 Chemin de Mervilla - 31320 Auzeville - France
 * or via email at infos@ns-team.fr
 *
 ********************************************************************************/

$admin_option_defs=array();
$admin_option_defs['Administration']['quickcrm_update']= array('Administration','LBL_UPDATE_QUICKCRM_TITLE','LBL_UPDATE_QUICKCRM','./index.php?module=Administration&action=updatequickcrm');


$admin_option_defs['Administration'] = array_merge((array)$admin_group_header[1][3]['Administration'],(array)$admin_option_defs['Administration']);

$admin_group_header[1]= array('LBL_ADMINISTRATION_HOME_TITLE','',false,$admin_option_defs, 'LBL_ADMINISTRATION_HOME_DESC');






$admin_option_defs=array();
$admin_option_defs['Administration']['salesagility']= array('Calls_Reschedule','LBL_RESCHEDULE_ADMIN','LBL_RESCHEDULE_ADMIN_DESC','./index.php?module=Administration&action=Reschedule_admin');
$admin_group_header[]= array('LBL_SALESAGILITY_ADMIN','',false, $admin_option_defs, '');






$admin_option_defs=array();
$admin_option_defs['Administration']['securitygroup_management']= array('SecurityGroups','LBL_MANAGE_SECURITYGROUPS_TITLE','LBL_MANAGE_SECURITYGROUPS','./index.php?module=SecurityGroups&action=index');
$admin_option_defs['Administration']['securitygroup_config']= array('SecurityGroups','LBL_CONFIG_SECURITYGROUPS_TITLE','LBL_CONFIG_SECURITYGROUPS','./index.php?module=SecurityGroups&action=config');

  



$admin_option_defs['Administration']['securitygroup_dashletpush']= array('icon_home','LBL_SECURITYGROUPS_DASHLETPUSH_TITLE','LBL_SECURITYGROUPS_DASHLETPUSH','./index.php?module=SecurityGroups&action=DashletPush');
$admin_option_defs['Administration']['securitygroup_hookup']= array('PatchUpgrades','LBL_SECURITYGROUPS_HOOKUP_TITLE','LBL_SECURITYGROUPS_HOOKUP','./index.php?module=SecurityGroups&action=Hookup');

$admin_option_defs['Administration']['securitygroup_info']= array('helpInline','LBL_SECURITYGROUPS_INFO_TITLE','LBL_SECURITYGROUPS_INFO','./index.php?module=SecurityGroups&action=info');


$admin_option_defs['Administration']['securitygroup_sugaroutfitters']= array('helpInline','LBL_SECURITYGROUPS_SUGAROUTFITTERS_TITLE','LBL_SECURITYGROUPS_SUGAROUTFITTERS','https://www.sugaroutfitters.com');
  

$admin_group_header[]= array('LBL_SECURITYGROUPS','',false,$admin_option_defs, '');



?>