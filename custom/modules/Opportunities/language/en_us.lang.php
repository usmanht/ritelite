<?php
// created: 2018-01-12 08:42:13
$mod_strings = array (
  'LBL_SECURITYGROUPS_SUBPANEL_TITLE' => 'Security Groups',
  'LBL_ASSIGNED_TO_NAME' => 'Salesperson',
  'LBL_LIST_ASSIGNED_USER' => 'Salesperson',
  'LBL_ASSIGNED_TO' => 'Salesperson',
  'AMOUNT' => 'Opportunity Amount',
  'LBL_COMPETITION' => 'Competition',
  'LBL_REASON_WON_LOST' => 'Reason won or lost',
  'LBL_LEAD_SOURCE_DESCRIPTION' => 'Lead Source Description',
  'LBL_EDITVIEW_PANEL1' => 'Sales / Production Forecast',
  'LBL_FORECAST_QUANTITY' => 'Forecast Quantity',
  'LBL_INCLUDE_IN_FORECAST' => 'Include in Forecast',
  'LBL_TOTAL_FORECAST_VALUE_GBP' => 'Total Forecast Value (GBP)',
  'LBL_EXPECTED_DESPATCH_DATE' => 'Expected Despatch Date',
);