<?php
$searchdefs ['Opportunities'] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      0 => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      1 => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_ACCOUNT_NAME',
        'id' => 'ACCOUNT_ID',
        'name' => 'account_name',
        'default' => true,
        'width' => '10%',
      ),
      2 => 
      array (
        'type' => 'currency',
        'label' => 'LBL_AMOUNT',
        'currency_format' => true,
        'name' => 'amount',
        'default' => true,
        'width' => '10%',
      ),
      3 => 
      array (
        'type' => 'enum',
        'label' => 'LBL_SALES_STAGE',
        'name' => 'sales_stage',
        'default' => true,
        'width' => '10%',
      ),
      4 => 
      array (
        'name' => 'current_user_only',
        'label' => 'LBL_CURRENT_USER_FILTER',
        'type' => 'bool',
        'default' => true,
        'width' => '10%',
      ),
      5 => 
      array (
        'name' => 'open_only',
        'label' => 'LBL_OPEN_ITEMS',
        'type' => 'bool',
        'default' => true,
        'width' => '10%',
      ),
      6 => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_CAMPAIGN',
        'id' => 'CAMPAIGN_ID',
        'width' => '10%',
        'default' => true,
        'name' => 'campaign_name',
      ),
      7 => 
      array (
        'type' => 'enum',
        'label' => 'LBL_ASSIGNED_TO',
        'id' => 'ASSIGNED_USER_ID',
        'link' => true,
        'name' => 'assigned_user_id',
        'function' => 
        array (
          'name' => 'get_user_array',
          'params' => 
          array (
            0 => false,
          ),
        ),
        'default' => true,
        'width' => '10%',
      ),
    ),
    'advanced_search' => 
    array (
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'account_name' => 
      array (
        'name' => 'account_name',
        'default' => true,
        'width' => '10%',
      ),
      'amount' => 
      array (
        'name' => 'amount',
        'default' => true,
        'width' => '10%',
      ),
      'aos_product_categories_opportunities_1_name' => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_AOS_PRODUCT_CATEGORIES_OPPORTUNITIES_1_FROM_AOS_PRODUCT_CATEGORIES_TITLE',
        'id' => 'AOS_PRODUCT_CATEGORIES_OPPORTUNITIES_1AOS_PRODUCT_CATEGORIES_IDA',
        'width' => '10%',
        'default' => true,
        'name' => 'aos_product_categories_opportunities_1_name',
      ),
      'aos_products_opportunities_1_name' => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_AOS_PRODUCTS_OPPORTUNITIES_1_FROM_AOS_PRODUCTS_TITLE',
        'id' => 'AOS_PRODUCTS_OPPORTUNITIES_1AOS_PRODUCTS_IDA',
        'width' => '10%',
        'default' => true,
        'name' => 'aos_products_opportunities_1_name',
      ),
      'forecast_quantity_c' => 
      array (
        'type' => 'int',
        'default' => true,
        'label' => 'LBL_FORECAST_QUANTITY',
        'width' => '10%',
        'name' => 'forecast_quantity_c',
      ),
      'sales_stage' => 
      array (
        'name' => 'sales_stage',
        'default' => true,
        'width' => '10%',
      ),
      'probability' => 
      array (
        'type' => 'int',
        'label' => 'LBL_PROBABILITY',
        'width' => '10%',
        'default' => true,
        'name' => 'probability',
      ),
      'next_step' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_NEXT_STEP',
        'width' => '10%',
        'default' => true,
        'name' => 'next_step',
      ),
      'lead_source' => 
      array (
        'name' => 'lead_source',
        'default' => true,
        'width' => '10%',
      ),
      'lead_source_description_c' => 
      array (
        'type' => 'text',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_LEAD_SOURCE_DESCRIPTION',
        'sortable' => false,
        'width' => '10%',
        'name' => 'lead_source_description_c',
      ),
      'campaign_name' => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_CAMPAIGN',
        'id' => 'CAMPAIGN_ID',
        'width' => '10%',
        'default' => true,
        'name' => 'campaign_name',
      ),
      'opportunity_type' => 
      array (
        'type' => 'enum',
        'label' => 'LBL_TYPE',
        'width' => '10%',
        'default' => true,
        'name' => 'opportunity_type',
      ),
      'assigned_user_id' => 
      array (
        'name' => 'assigned_user_id',
        'type' => 'enum',
        'label' => 'LBL_ASSIGNED_TO',
        'function' => 
        array (
          'name' => 'get_user_array',
          'params' => 
          array (
            0 => false,
          ),
        ),
        'default' => true,
        'width' => '10%',
      ),
      'date_closed' => 
      array (
        'name' => 'date_closed',
        'default' => true,
        'width' => '10%',
      ),
      'include_in_forecast_c' => 
      array (
        'type' => 'bool',
        'default' => true,
        'label' => 'LBL_INCLUDE_IN_FORECAST',
        'width' => '10%',
        'name' => 'include_in_forecast_c',
      ),
      'current_user_only' => 
      array (
        'label' => 'LBL_CURRENT_USER_FILTER',
        'type' => 'bool',
        'default' => true,
        'width' => '10%',
        'name' => 'current_user_only',
      ),
      'open_only' => 
      array (
        'label' => 'LBL_OPEN_ITEMS',
        'type' => 'bool',
        'default' => true,
        'width' => '10%',
        'name' => 'open_only',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
?>
