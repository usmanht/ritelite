<?php 
 //WARNING: The contents of this file are auto-generated


$dictionary["Opportunity"]["fields"]["aos_quotes"] = array (
  'name' => 'aos_quotes',
    'type' => 'link',
    'relationship' => 'opportunity_aos_quotes',
    'module'=>'AOS_Quotes',
    'bean_name'=>'AOS_Quotes',
    'source'=>'non-db',
);

$dictionary["Opportunity"]["relationships"]["opportunity_aos_quotes"] = array (
	'lhs_module'=> 'Opportunities', 
	'lhs_table'=> 'opportunities', 
	'lhs_key' => 'id',
	'rhs_module'=> 'AOS_Quotes', 
	'rhs_table'=> 'aos_quotes', 
	'rhs_key' => 'opportunity_id',
	'relationship_type'=>'one-to-many',
);

$dictionary["Opportunity"]["fields"]["aos_contracts"] = array (
  'name' => 'aos_contracts',
    'type' => 'link',
    'relationship' => 'opportunity_aos_contracts',
    'module'=>'AOS_Contracts',
    'bean_name'=>'AOS_Contracts',
    'source'=>'non-db',
);

$dictionary["Opportunity"]["relationships"]["opportunity_aos_contracts"] = array (
	'lhs_module'=> 'Opportunities', 
	'lhs_table'=> 'opportunities', 
	'lhs_key' => 'id',
	'rhs_module'=> 'AOS_Contracts', 
	'rhs_table'=> 'aos_contracts', 
	'rhs_key' => 'opportunity_id',
	'relationship_type'=>'one-to-many',
);


// created: 2018-01-12 08:20:30
$dictionary["Opportunity"]["fields"]["aos_products_opportunities_1"] = array (
  'name' => 'aos_products_opportunities_1',
  'type' => 'link',
  'relationship' => 'aos_products_opportunities_1',
  'source' => 'non-db',
  'module' => 'AOS_Products',
  'bean_name' => 'AOS_Products',
  'vname' => 'LBL_AOS_PRODUCTS_OPPORTUNITIES_1_FROM_AOS_PRODUCTS_TITLE',
  'id_name' => 'aos_products_opportunities_1aos_products_ida',
);
$dictionary["Opportunity"]["fields"]["aos_products_opportunities_1_name"] = array (
  'name' => 'aos_products_opportunities_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_AOS_PRODUCTS_OPPORTUNITIES_1_FROM_AOS_PRODUCTS_TITLE',
  'save' => true,
  'id_name' => 'aos_products_opportunities_1aos_products_ida',
  'link' => 'aos_products_opportunities_1',
  'table' => 'aos_products',
  'module' => 'AOS_Products',
  'rname' => 'name',
);
$dictionary["Opportunity"]["fields"]["aos_products_opportunities_1aos_products_ida"] = array (
  'name' => 'aos_products_opportunities_1aos_products_ida',
  'type' => 'link',
  'relationship' => 'aos_products_opportunities_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_AOS_PRODUCTS_OPPORTUNITIES_1_FROM_OPPORTUNITIES_TITLE',
);


// created: 2018-01-12 08:18:24
$dictionary["Opportunity"]["fields"]["aos_product_categories_opportunities_1"] = array (
  'name' => 'aos_product_categories_opportunities_1',
  'type' => 'link',
  'relationship' => 'aos_product_categories_opportunities_1',
  'source' => 'non-db',
  'module' => 'AOS_Product_Categories',
  'bean_name' => 'AOS_Product_Categories',
  'vname' => 'LBL_AOS_PRODUCT_CATEGORIES_OPPORTUNITIES_1_FROM_AOS_PRODUCT_CATEGORIES_TITLE',
  'id_name' => 'aos_product_categories_opportunities_1aos_product_categories_ida',
);
$dictionary["Opportunity"]["fields"]["aos_product_categories_opportunities_1_name"] = array (
  'name' => 'aos_product_categories_opportunities_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_AOS_PRODUCT_CATEGORIES_OPPORTUNITIES_1_FROM_AOS_PRODUCT_CATEGORIES_TITLE',
  'save' => true,
  'id_name' => 'aos_product_categories_opportunities_1aos_product_categories_ida',
  'link' => 'aos_product_categories_opportunities_1',
  'table' => 'aos_product_categories',
  'module' => 'AOS_Product_Categories',
  'rname' => 'name',
);
$dictionary["Opportunity"]["fields"]["aos_product_categories_opportunities_1aos_product_categories_ida"] = array (
  'name' => 'aos_product_categories_opportunities_1aos_product_categories_ida',
  'type' => 'link',
  'relationship' => 'aos_product_categories_opportunities_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_AOS_PRODUCT_CATEGORIES_OPPORTUNITIES_1_FROM_OPPORTUNITIES_TITLE',
);


// created: 2014-08-19 18:08:45
$dictionary["Opportunity"]["fields"]["opportunities_aos_invoices_1"] = array (
  'name' => 'opportunities_aos_invoices_1',
  'type' => 'link',
  'relationship' => 'opportunities_aos_invoices_1',
  'source' => 'non-db',
  'module' => 'AOS_Invoices',
  'bean_name' => 'AOS_Invoices',
  'side' => 'right',
  'vname' => 'LBL_OPPORTUNITIES_AOS_INVOICES_1_FROM_AOS_INVOICES_TITLE',
);


// created: 2014-08-26 12:19:38
$dictionary["Opportunity"]["fields"]["opportunities_calls_1"] = array (
  'name' => 'opportunities_calls_1',
  'type' => 'link',
  'relationship' => 'opportunities_calls_1',
  'source' => 'non-db',
  'module' => 'Calls',
  'bean_name' => 'Call',
  'side' => 'right',
  'vname' => 'LBL_OPPORTUNITIES_CALLS_1_FROM_CALLS_TITLE',
);



$dictionary['Opportunity']['fields']['SecurityGroups'] = array (
  	'name' => 'SecurityGroups',
    'type' => 'link',
	'relationship' => 'securitygroups_opportunities',
	'module'=>'SecurityGroups',
	'bean_name'=>'SecurityGroup',
    'source'=>'non-db',
	'vname'=>'LBL_SECURITYGROUPS',
);






 // created: 2014-08-19 10:38:09
$dictionary['Opportunity']['fields']['competition_c']['labelValue']='Competition';

 

 // created: 2018-01-12 08:44:05
$dictionary['Opportunity']['fields']['expected_despatch_date_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['expected_despatch_date_c']['options']='date_range_search_dom';
$dictionary['Opportunity']['fields']['expected_despatch_date_c']['labelValue']='Expected Despatch Date';
$dictionary['Opportunity']['fields']['expected_despatch_date_c']['enable_range_search']='1';

 

 // created: 2018-01-12 08:28:17
$dictionary['Opportunity']['fields']['forecast_quantity_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['forecast_quantity_c']['labelValue']='Forecast Quantity';

 

 // created: 2018-01-12 08:30:11
$dictionary['Opportunity']['fields']['include_in_forecast_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['include_in_forecast_c']['labelValue']='Include in Forecast';

 

 // created: 2016-03-04 15:13:15

 

 // created: 2016-03-04 15:13:14

 

 // created: 2016-03-04 15:13:12

 

 // created: 2016-03-04 15:13:09

 

 // created: 2015-08-10 17:19:02
$dictionary['Opportunity']['fields']['lead_source_description_c']['labelValue']='lead source description';

 

 // created: 2014-08-19 15:05:28
$dictionary['Opportunity']['fields']['reason_won_lost_c']['labelValue']='Reason won or lost';

 

 // created: 2018-01-12 08:32:28
$dictionary['Opportunity']['fields']['total_forecast_value_gbp_c']['inline_edit']='1';
$dictionary['Opportunity']['fields']['total_forecast_value_gbp_c']['labelValue']='Total Forecast Value (GBP)';

 
?>