<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2014-03-25 16:57:28
$dictionary["AOS_Quotes"]["fields"]["accounts_aos_quotes_1"] = array (
  'name' => 'accounts_aos_quotes_1',
  'type' => 'link',
  'relationship' => 'accounts_aos_quotes_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_AOS_QUOTES_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_aos_quotes_1accounts_ida',
);
$dictionary["AOS_Quotes"]["fields"]["accounts_aos_quotes_1_name"] = array (
  'name' => 'accounts_aos_quotes_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_AOS_QUOTES_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_aos_quotes_1accounts_ida',
  'link' => 'accounts_aos_quotes_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["AOS_Quotes"]["fields"]["accounts_aos_quotes_1accounts_ida"] = array (
  'name' => 'accounts_aos_quotes_1accounts_ida',
  'type' => 'link',
  'relationship' => 'accounts_aos_quotes_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_AOS_QUOTES_1_FROM_AOS_QUOTES_TITLE',
);


// created: 2014-08-26 09:34:04
$dictionary["AOS_Quotes"]["fields"]["aos_quotes_calls_1"] = array (
  'name' => 'aos_quotes_calls_1',
  'type' => 'link',
  'relationship' => 'aos_quotes_calls_1',
  'source' => 'non-db',
  'module' => 'Calls',
  'bean_name' => 'Call',
  'side' => 'right',
  'vname' => 'LBL_AOS_QUOTES_CALLS_1_FROM_CALLS_TITLE',
);


// created: 2017-10-05 08:41:36
$dictionary["AOS_Quotes"]["fields"]["cases_aos_quotes_1"] = array (
  'name' => 'cases_aos_quotes_1',
  'type' => 'link',
  'relationship' => 'cases_aos_quotes_1',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'vname' => 'LBL_CASES_AOS_QUOTES_1_FROM_CASES_TITLE',
  'id_name' => 'cases_aos_quotes_1cases_ida',
);
$dictionary["AOS_Quotes"]["fields"]["cases_aos_quotes_1_name"] = array (
  'name' => 'cases_aos_quotes_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CASES_AOS_QUOTES_1_FROM_CASES_TITLE',
  'save' => true,
  'id_name' => 'cases_aos_quotes_1cases_ida',
  'link' => 'cases_aos_quotes_1',
  'table' => 'cases',
  'module' => 'Cases',
  'rname' => 'name',
);
$dictionary["AOS_Quotes"]["fields"]["cases_aos_quotes_1cases_ida"] = array (
  'name' => 'cases_aos_quotes_1cases_ida',
  'type' => 'link',
  'relationship' => 'cases_aos_quotes_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_CASES_AOS_QUOTES_1_FROM_AOS_QUOTES_TITLE',
);


// created: 2017-10-05 08:42:03
$dictionary["AOS_Quotes"]["fields"]["cases_aos_quotes_2"] = array (
  'name' => 'cases_aos_quotes_2',
  'type' => 'link',
  'relationship' => 'cases_aos_quotes_2',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'vname' => 'LBL_CASES_AOS_QUOTES_2_FROM_CASES_TITLE',
  'id_name' => 'cases_aos_quotes_2cases_ida',
);
$dictionary["AOS_Quotes"]["fields"]["cases_aos_quotes_2_name"] = array (
  'name' => 'cases_aos_quotes_2_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CASES_AOS_QUOTES_2_FROM_CASES_TITLE',
  'save' => true,
  'id_name' => 'cases_aos_quotes_2cases_ida',
  'link' => 'cases_aos_quotes_2',
  'table' => 'cases',
  'module' => 'Cases',
  'rname' => 'name',
);
$dictionary["AOS_Quotes"]["fields"]["cases_aos_quotes_2cases_ida"] = array (
  'name' => 'cases_aos_quotes_2cases_ida',
  'type' => 'link',
  'relationship' => 'cases_aos_quotes_2',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_CASES_AOS_QUOTES_2_FROM_AOS_QUOTES_TITLE',
);


// created: 2014-03-25 17:07:24
$dictionary["AOS_Quotes"]["fields"]["contacts_aos_quotes_1"] = array (
  'name' => 'contacts_aos_quotes_1',
  'type' => 'link',
  'relationship' => 'contacts_aos_quotes_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_CONTACTS_AOS_QUOTES_1_FROM_CONTACTS_TITLE',
  'id_name' => 'contacts_aos_quotes_1contacts_ida',
);
$dictionary["AOS_Quotes"]["fields"]["contacts_aos_quotes_1_name"] = array (
  'name' => 'contacts_aos_quotes_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_CONTACTS_AOS_QUOTES_1_FROM_CONTACTS_TITLE',
  'save' => true,
  'id_name' => 'contacts_aos_quotes_1contacts_ida',
  'link' => 'contacts_aos_quotes_1',
  'table' => 'contacts',
  'module' => 'Contacts',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["AOS_Quotes"]["fields"]["contacts_aos_quotes_1contacts_ida"] = array (
  'name' => 'contacts_aos_quotes_1contacts_ida',
  'type' => 'link',
  'relationship' => 'contacts_aos_quotes_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_CONTACTS_AOS_QUOTES_1_FROM_AOS_QUOTES_TITLE',
);


// created: 2014-05-13 17:59:33
$dictionary["AOS_Quotes"]["fields"]["documents_aos_quotes_1"] = array (
  'name' => 'documents_aos_quotes_1',
  'type' => 'link',
  'relationship' => 'documents_aos_quotes_1',
  'source' => 'non-db',
  'module' => 'Documents',
  'bean_name' => 'Document',
  'vname' => 'LBL_DOCUMENTS_AOS_QUOTES_1_FROM_DOCUMENTS_TITLE',
);


$dictionary["AOS_Quotes"]["fields"]["sage_ref_billing"] = array(
    'id' => 'sage_ref_billing',
    'class' => 'sqsEnabled',
    'name' => 'sage_ref_billing',
    'vname' => 'LBL_SAGE_REF',
    'comments' => '',
    'help' => '',
    'module' => 'AOS_Quotes',
    'type' => 'varchar',
    'max_size' => '155',
    'reportable' => '0',
    //'source' => 'non-db',
    //'dbtype' => 'non-db',
    'studio' => 'visible'
);

$dictionary["AOS_Quotes"]["fields"]["sage_ref_shipping"] = array(
    'id' => 'sage_ref_shipping',
    'class' => 'sqsEnabled',
    'name' => 'sage_ref_shipping',
    'vname' => 'LBL_SAGE_REF1',
    'comments' => '',
    'help' => '',
    'module' => 'AOS_Quotes',
    'type' => 'varchar',
    'max_size' => '155',
    'reportable' => '0',
    //'source' => 'non-db',
    //'dbtype' => 'non-db',
    'studio' => 'visible'
);

// created: 2014-05-02 12:12:37
$dictionary["AOS_Quotes"]["fields"]["securitygroups_aos_quotes_1"] = array (
  'name' => 'securitygroups_aos_quotes_1',
  'type' => 'link',
  'relationship' => 'securitygroups_aos_quotes_1',
  'source' => 'non-db',
  'module' => 'SecurityGroups',
  'bean_name' => 'SecurityGroup',
  'vname' => 'LBL_SECURITYGROUPS_AOS_QUOTES_1_FROM_SECURITYGROUPS_TITLE',
);


 // created: 2014-05-20 13:35:33
$dictionary['AOS_Quotes']['fields']['accessquoteid_c']['labelValue']='Access Quote ID';

 

 // created: 2014-12-19 16:20:59
$dictionary['AOS_Quotes']['fields']['accessquoteref_c']['labelValue']='Access Quote Ref';
$dictionary['AOS_Quotes']['fields']['accessquoteref_c']['unified_search']='1';

 

 // created: 2014-09-18 10:10:16
$dictionary['AOS_Quotes']['fields']['additiona_text_for_quote_c']['labelValue']='Additional Text for Quote';

 

 // created: 2014-05-05 16:17:01
$dictionary['AOS_Quotes']['fields']['approval_status']['default']='Not Approved';
$dictionary['AOS_Quotes']['fields']['approval_status']['merge_filter']='disabled';

 

 // created: 2015-07-27 16:05:15
$dictionary['AOS_Quotes']['fields']['notes_currency_exchange_rate_c']['labelValue']='notes currency exchange rate';

 

 // created: 2014-08-01 11:32:10
$dictionary['AOS_Quotes']['fields']['sage_ref_billing']['merge_filter']='disabled';
$dictionary['AOS_Quotes']['fields']['sage_ref_billing']['reportable']=true;
$dictionary['AOS_Quotes']['fields']['sage_ref_billing']['rows']='4';
$dictionary['AOS_Quotes']['fields']['sage_ref_billing']['cols']='20';
$dictionary['AOS_Quotes']['fields']['sage_ref_billing']['len']='155';

 

 // created: 2014-08-01 11:32:49
$dictionary['AOS_Quotes']['fields']['sage_ref_shipping']['merge_filter']='disabled';
$dictionary['AOS_Quotes']['fields']['sage_ref_shipping']['reportable']=true;
$dictionary['AOS_Quotes']['fields']['sage_ref_shipping']['rows']='4';
$dictionary['AOS_Quotes']['fields']['sage_ref_shipping']['cols']='20';
$dictionary['AOS_Quotes']['fields']['sage_ref_shipping']['len']='155';

 

 // created: 2014-12-19 16:21:10
$dictionary['AOS_Quotes']['fields']['searchlabel_c']['labelValue']='Search Label';
$dictionary['AOS_Quotes']['fields']['searchlabel_c']['unified_search']='1';

 

 // created: 2015-01-09 10:00:35
$dictionary['AOS_Quotes']['fields']['shipping_company_c']['labelValue']='Shipping Company';

 

 // created: 2015-01-09 10:01:11
$dictionary['AOS_Quotes']['fields']['shipping_contact_c']['labelValue']='Shipping Contact';

 

 // created: 2014-05-07 10:13:17
$dictionary['AOS_Quotes']['fields']['term']['options']='payment_terms';
$dictionary['AOS_Quotes']['fields']['term']['merge_filter']='disabled';

 
?>