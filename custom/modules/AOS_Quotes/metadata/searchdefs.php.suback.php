<?php
// created: 2016-03-04 15:10:16
$searchdefs['AOS_Quotes'] = array (
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
    'maxColumnsBasic' => '3',
  ),
  'layout' => 
  array (
    'basic_search' => 
    array (
      0 => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      1 => 
      array (
        'name' => 'current_user_only',
        'label' => 'LBL_CURRENT_USER_FILTER',
        'type' => 'bool',
        'default' => true,
        'width' => '10%',
      ),
      2 => 
      array (
        'type' => 'int',
        'label' => 'LBL_QUOTE_NUMBER',
        'default' => true,
        'width' => '10%',
        'name' => 'number',
      ),
      3 => 
      array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_STAGE',
        'width' => '10%',
        'name' => 'stage',
      ),
      4 => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_ACCESSQUOTEREF',
        'width' => '10%',
        'name' => 'accessquoteref_c',
      ),
    ),
    'advanced_search' => 
    array (
      0 => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      1 => 
      array (
        'name' => 'billing_account',
        'default' => true,
        'width' => '10%',
      ),
      2 => 
      array (
        'name' => 'billing_contact',
        'default' => true,
        'width' => '10%',
      ),
      3 => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_ACCOUNTS_AOS_QUOTES_1_FROM_ACCOUNTS_TITLE',
        'id' => 'ACCOUNTS_AOS_QUOTES_1ACCOUNTS_IDA',
        'width' => '10%',
        'default' => true,
        'name' => 'accounts_aos_quotes_1_name',
      ),
      4 => 
      array (
        'type' => 'relate',
        'link' => true,
        'label' => 'LBL_CONTACTS_AOS_QUOTES_1_FROM_CONTACTS_TITLE',
        'id' => 'CONTACTS_AOS_QUOTES_1CONTACTS_IDA',
        'width' => '10%',
        'default' => true,
        'name' => 'contacts_aos_quotes_1_name',
      ),
      5 => 
      array (
        'name' => 'number',
        'default' => true,
        'width' => '10%',
      ),
      6 => 
      array (
        'type' => 'relate',
        'studio' => 'visible',
        'label' => 'LBL_OPPORTUNITY',
        'id' => 'OPPORTUNITY_ID',
        'link' => true,
        'width' => '10%',
        'default' => true,
        'name' => 'opportunity',
      ),
      7 => 
      array (
        'name' => 'total_amount',
        'default' => true,
        'width' => '10%',
      ),
      8 => 
      array (
        'name' => 'expiration',
        'default' => true,
        'width' => '10%',
      ),
      9 => 
      array (
        'name' => 'stage',
        'default' => true,
        'width' => '10%',
      ),
      10 => 
      array (
        'name' => 'assigned_user_id',
        'type' => 'enum',
        'label' => 'LBL_ASSIGNED_TO',
        'function' => 
        array (
          'name' => 'get_user_array',
          'params' => 
          array (
            0 => false,
          ),
        ),
        'default' => true,
        'width' => '10%',
      ),
      11 => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_ACCESSQUOTEREF',
        'width' => '10%',
        'name' => 'accessquoteref_c',
      ),
      12 => 
      array (
        'type' => 'varchar',
        'default' => true,
        'label' => 'LBL_SEARCHLABEL',
        'width' => '10%',
        'name' => 'searchlabel_c',
      ),
      13 => 
      array (
        'type' => 'datetime',
        'label' => 'LBL_DATE_ENTERED',
        'width' => '10%',
        'default' => true,
        'name' => 'date_entered',
      ),
      14 => 
      array (
        'label' => 'LBL_CURRENT_USER_FILTER',
        'type' => 'bool',
        'default' => true,
        'width' => '10%',
        'name' => 'current_user_only',
      ),
    ),
  ),
);