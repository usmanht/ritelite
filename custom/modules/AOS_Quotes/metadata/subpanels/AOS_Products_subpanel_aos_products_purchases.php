<?php
// created: 2017-03-29 13:47:01
$subpanel_layout['list_fields'] = array (
  'number' => 
  array (
    'width' => '5%',
    'vname' => 'LBL_LIST_NUM',
    'default' => true,
  ),
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '25%',
    'default' => true,
  ),
  'billing_account' => 
  array (
    'width' => '20%',
    'vname' => 'LBL_BILLING_ACCOUNT',
    'default' => true,
  ),
  'billing_contact' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'vname' => 'LBL_BILLING_CONTACT',
    'id' => 'BILLING_CONTACT_ID',
    'link' => true,
    'width' => '10%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Contacts',
    'target_record_key' => 'billing_contact_id',
  ),
  'total_amount' => 
  array (
    'type' => 'currency',
    'width' => '15%',
    'currency_format' => true,
    'vname' => 'LBL_GRAND_TOTAL',
    'default' => true,
  ),
  'date_entered' => 
  array (
    'type' => 'datetime',
    'vname' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
  ),
  'expiration' => 
  array (
    'width' => '15%',
    'vname' => 'LBL_EXPIRATION',
    'default' => true,
  ),
  'assigned_user_name' => 
  array (
    'link' => 'assigned_user_link',
    'type' => 'relate',
    'vname' => 'LBL_ASSIGNED_USER',
    'width' => '15%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Users',
    'target_record_key' => 'assigned_user_id',
  ),
  'edit_button' => 
  array (
    'widget_class' => 'SubPanelEditButton',
    'module' => 'AOS_Quotes',
    'width' => '4%',
    'default' => true,
  ),
  'currency_id' => 
  array (
    'usage' => 'query_only',
  ),
);