<?php
// created: 2016-03-04 17:25:43
$searchdefs['AOS_PDF_Templates'] = array (
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
    'maxColumnsBasic' => '3',
  ),
  'layout' => 
  array (
    'basic_search' => 
    array (
      0 => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      1 => 
      array (
        'name' => 'current_user_only',
        'label' => 'LBL_CURRENT_USER_FILTER',
        'type' => 'bool',
        'default' => true,
        'width' => '10%',
      ),
      2 => 
      array (
        'type' => 'bool',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_ACTIVE',
        'width' => '10%',
        'name' => 'active',
      ),
    ),
    'advanced_search' => 
    array (
      0 => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      1 => 
      array (
        'name' => 'date_entered',
        'default' => true,
        'width' => '10%',
      ),
      2 => 
      array (
        'name' => 'date_modified',
        'default' => true,
        'width' => '10%',
      ),
      3 => 
      array (
        'name' => 'type',
        'default' => true,
        'width' => '10%',
      ),
      4 => 
      array (
        'name' => 'created_by',
        'label' => 'LBL_CREATED',
        'type' => 'enum',
        'function' => 
        array (
          'name' => 'get_user_array',
          'params' => 
          array (
            0 => false,
          ),
        ),
        'default' => true,
        'width' => '10%',
      ),
      5 => 
      array (
        'label' => 'LBL_CURRENT_USER_FILTER',
        'type' => 'bool',
        'default' => true,
        'width' => '10%',
        'name' => 'current_user_only',
      ),
      6 => 
      array (
        'type' => 'bool',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_ACTIVE',
        'width' => '10%',
        'name' => 'active',
      ),
    ),
  ),
);