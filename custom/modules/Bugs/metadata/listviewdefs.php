<?php
$listViewDefs ['Bugs'] = 
array (
  'BUG_NUMBER' => 
  array (
    'width' => '5%',
    'label' => 'LBL_LIST_NUMBER',
    'link' => true,
    'default' => true,
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '10%',
    'default' => true,
  ),
  'BUG_REQUIRED_CLOSE_DATE_C' => 
  array (
    'type' => 'date',
    'default' => true,
    'label' => 'LBL_BUG_REQUIRED_CLOSE_DATE',
    'width' => '10%',
  ),
  'PRODUCT_CATEGORY' => 
  array (
    'type' => 'enum',
    'label' => 'LBL_PRODUCT_CATEGORY',
    'width' => '10%',
    'default' => true,
  ),
  'AOS_PRODUCT_CATEGORIES_BUGS_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_AOS_PRODUCT_CATEGORIES_BUGS_1_FROM_AOS_PRODUCT_CATEGORIES_TITLE',
    'id' => 'AOS_PRODUCT_CATEGORIES_BUGS_1AOS_PRODUCT_CATEGORIES_IDA',
    'width' => '10%',
    'default' => true,
  ),
  'NAME' => 
  array (
    'width' => '20%',
    'label' => 'LBL_LIST_SUBJECT',
    'default' => true,
    'link' => true,
  ),
  'STATUS' => 
  array (
    'width' => '10%',
    'label' => 'LBL_LIST_STATUS',
    'default' => true,
  ),
  'TYPE' => 
  array (
    'width' => '10%',
    'label' => 'LBL_LIST_TYPE',
    'default' => true,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_LIST_ASSIGNED_USER',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
  ),
  'RELEASE_NAME' => 
  array (
    'width' => '10%',
    'label' => 'LBL_FOUND_IN_RELEASE',
    'default' => false,
    'related_fields' => 
    array (
      0 => 'found_in_release',
    ),
    'module' => 'Releases',
    'id' => 'FOUND_IN_RELEASE',
  ),
  'RESOLUTION' => 
  array (
    'width' => '10%',
    'label' => 'LBL_LIST_RESOLUTION',
    'default' => false,
  ),
  'FIXED_IN_RELEASE_NAME' => 
  array (
    'width' => '10%',
    'label' => 'LBL_LIST_FIXED_IN_RELEASE',
    'default' => false,
    'related_fields' => 
    array (
      0 => 'fixed_in_release',
    ),
    'module' => 'Releases',
    'id' => 'FIXED_IN_RELEASE',
  ),
  'PRIORITY' => 
  array (
    'width' => '10%',
    'label' => 'LBL_LIST_PRIORITY',
    'default' => false,
  ),
  'RESOLUTION_DESCRIPTION_C' => 
  array (
    'type' => 'text',
    'default' => false,
    'studio' => 'visible',
    'label' => 'LBL_RESOLUTION_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
  ),
);
?>
