<?php 
 //WARNING: The contents of this file are auto-generated



$dictionary['Prospect']['fields']['SecurityGroups'] = array (
  	'name' => 'SecurityGroups',
    'type' => 'link',
	'relationship' => 'securitygroups_prospects',
	'module'=>'SecurityGroups',
	'bean_name'=>'SecurityGroup',
    'source'=>'non-db',
	'vname'=>'LBL_SECURITYGROUPS',
);






 // created: 2014-12-19 16:20:33
$dictionary['Prospect']['fields']['accesscontactid_c']['labelValue']='Access Contact ID';
$dictionary['Prospect']['fields']['accesscontactid_c']['unified_search']='1';

 

 // created: 2015-03-03 12:03:41
$dictionary['Prospect']['fields']['alt_address_city']['comments']='City for alternate address';
$dictionary['Prospect']['fields']['alt_address_city']['merge_filter']='disabled';
$dictionary['Prospect']['fields']['alt_address_city']['unified_search']=true;

 

 // created: 2015-03-03 12:04:12
$dictionary['Prospect']['fields']['alt_address_country']['comments']='Country for alternate address';
$dictionary['Prospect']['fields']['alt_address_country']['merge_filter']='disabled';
$dictionary['Prospect']['fields']['alt_address_country']['unified_search']=true;

 

 // created: 2015-03-03 12:03:49
$dictionary['Prospect']['fields']['alt_address_postalcode']['comments']='Postal code for alternate address';
$dictionary['Prospect']['fields']['alt_address_postalcode']['merge_filter']='disabled';
$dictionary['Prospect']['fields']['alt_address_postalcode']['unified_search']=true;

 

 // created: 2015-03-03 12:03:53
$dictionary['Prospect']['fields']['alt_address_state']['comments']='State for alternate address';
$dictionary['Prospect']['fields']['alt_address_state']['merge_filter']='disabled';
$dictionary['Prospect']['fields']['alt_address_state']['unified_search']=true;

 

 // created: 2015-03-03 12:03:56
$dictionary['Prospect']['fields']['alt_address_street']['comments']='Street address for alternate address';
$dictionary['Prospect']['fields']['alt_address_street']['merge_filter']='disabled';
$dictionary['Prospect']['fields']['alt_address_street']['unified_search']=true;

 

 // created: 2014-03-28 13:14:24
$dictionary['Prospect']['fields']['companyname_c']['labelValue']='Company Name';

 

 // created: 2018-05-09 17:49:34
$dictionary['Prospect']['fields']['gdpr_classification_c']['inline_edit']='1';
$dictionary['Prospect']['fields']['gdpr_classification_c']['labelValue']='GDPR Classification';

 

 // created: 2014-03-21 17:48:10
$dictionary['Prospect']['fields']['group_c']['labelValue']='Group ';

 

 // created: 2016-03-04 15:13:32

 

 // created: 2016-03-04 15:13:29

 

 // created: 2016-03-04 15:13:27

 

 // created: 2016-03-04 15:13:25

 

 // created: 2014-03-28 12:36:11
$dictionary['Prospect']['fields']['last_name']['required']=false;
$dictionary['Prospect']['fields']['last_name']['comments']='Last name of the contact';
$dictionary['Prospect']['fields']['last_name']['importable']='true';
$dictionary['Prospect']['fields']['last_name']['merge_filter']='disabled';
$dictionary['Prospect']['fields']['last_name']['unified_search']=false;

 

 // created: 2014-03-21 17:48:10
$dictionary['Prospect']['fields']['notes_c']['labelValue']='Notes';

 

 // created: 2018-05-07 10:29:40
$dictionary['Prospect']['fields']['opt_in_c']['inline_edit']='1';
$dictionary['Prospect']['fields']['opt_in_c']['labelValue']='Opt-in';

 

 // created: 2015-03-03 12:04:16
$dictionary['Prospect']['fields']['primary_address_city']['comments']='City for primary address';
$dictionary['Prospect']['fields']['primary_address_city']['merge_filter']='disabled';
$dictionary['Prospect']['fields']['primary_address_city']['unified_search']=true;

 

 // created: 2015-03-03 12:04:19
$dictionary['Prospect']['fields']['primary_address_country']['comments']='Country for primary address';
$dictionary['Prospect']['fields']['primary_address_country']['merge_filter']='disabled';
$dictionary['Prospect']['fields']['primary_address_country']['unified_search']=true;

 

 // created: 2015-03-03 12:04:24
$dictionary['Prospect']['fields']['primary_address_postalcode']['comments']='Postal code for primary address';
$dictionary['Prospect']['fields']['primary_address_postalcode']['merge_filter']='disabled';
$dictionary['Prospect']['fields']['primary_address_postalcode']['unified_search']=true;

 

 // created: 2015-03-03 12:04:28
$dictionary['Prospect']['fields']['primary_address_state']['comments']='State for primary address';
$dictionary['Prospect']['fields']['primary_address_state']['merge_filter']='disabled';
$dictionary['Prospect']['fields']['primary_address_state']['unified_search']=true;

 

 // created: 2015-03-03 12:04:32
$dictionary['Prospect']['fields']['primary_address_street']['comments']='Street address for primary address';
$dictionary['Prospect']['fields']['primary_address_street']['merge_filter']='disabled';
$dictionary['Prospect']['fields']['primary_address_street']['unified_search']=true;

 

 // created: 2014-03-28 14:03:40
$dictionary['Prospect']['fields']['searchlabel_c']['labelValue']='Label';

 

 // created: 2014-12-19 16:20:20
$dictionary['Prospect']['fields']['search_tags_c']['labelValue']='Search tags';
$dictionary['Prospect']['fields']['search_tags_c']['unified_search']='1';

 

 // created: 2014-03-28 13:41:52
$dictionary['Prospect']['fields']['website_c']['labelValue']='website';

 

 // created: 2018-05-08 08:52:47
$dictionary['Prospect']['fields']['workflow_trigger_date_time_c']['inline_edit']='1';
$dictionary['Prospect']['fields']['workflow_trigger_date_time_c']['labelValue']='Workflow Trigger Date/Time';

 
?>